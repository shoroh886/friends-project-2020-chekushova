package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.time.LocalDate.parse;

@Entity
@NamedEntityGraphs({
        @NamedEntityGraph(name = "fullUser", attributeNodes = {
                @NamedAttributeNode("phones"),
                @NamedAttributeNode("avatar")
        }),
        @NamedEntityGraph(name = "withPhones", attributeNodes = {
                @NamedAttributeNode("phones"),
        }),
        @NamedEntityGraph(name = "withAvatar", attributeNodes = {
                @NamedAttributeNode("avatar")
        })
})

public class User implements Serializable {
    private static final long serialVersionUID = -7441705478778383444L;

    @Id
    private Long id;
    private String name;
    private String surname;
    private String patronymic;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    private List<Phone> phones = new ArrayList<>();
    private LocalDate birth;
    private String home;
    private String work;
    private String email;
    private String icq;
    private String skype;
    private String additionally;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
    @JoinColumn(name = "id")
    private Avatar avatar;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String firstName) {
        this.name = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String secondName) {
        this.surname = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String birthToString() {
        return birth == null ? "" : birth.toString();
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth.equals("") ? null : parse(birth);
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getHome() {
        return home;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String workAddress) {
        this.work = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionally() {
        return additionally;
    }

    public void setAdditionally(String additionally) {
        this.additionally = additionally;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(patronymic, user.patronymic) &&
                Objects.equals(phones, user.phones) &&
                Objects.equals(birth, user.birth) &&
                Objects.equals(home, user.home) &&
                Objects.equals(work, user.work) &&
                Objects.equals(email, user.email) &&
                Objects.equals(icq, user.icq) &&
                Objects.equals(skype, user.skype) &&
                Objects.equals(additionally, user.additionally) &&
                Objects.equals(avatar, user.avatar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, patronymic, phones, birth, home, work, email, icq,
                skype, additionally, avatar);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", phones=" + phones +
                ", birth=" + birth +
                ", home='" + home + '\'' +
                ", work='" + work + '\'' +
                ", email='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", additionally='" + additionally + '\'' +
                ", avatar=" + avatar +
                '}';
    }

    public static class UserBuilder {
        private final User newUser;

        public UserBuilder() {
            newUser = new User();
        }

        public UserBuilder id(long id) {
            newUser.id = id;
            return this;
        }

        public UserBuilder name(String name) {
            newUser.name = name;
            return this;
        }

        public UserBuilder surname(String surname) {
            newUser.surname = surname;
            return this;
        }

        public UserBuilder patronymic(String patronymic) {
            newUser.patronymic = patronymic;
            return this;
        }

        public UserBuilder phones(List<Phone> phones) {
            newUser.phones = phones;
            return this;
        }

        public UserBuilder birth(LocalDate birth) {
            newUser.birth = birth;
            return this;
        }

        public UserBuilder birth(String birth) {
            newUser.birth = birth.isEmpty() ? null : parse(birth);
            return this;
        }

        public UserBuilder homeAddress(String homeAddress) {
            newUser.home = homeAddress;
            return this;
        }

        public UserBuilder workAddress(String workAddress) {
            newUser.work = workAddress;
            return this;
        }

        public UserBuilder email(String email) {
            newUser.email = email;
            return this;
        }

        public UserBuilder icq(String icq) {
            newUser.icq = icq;
            return this;
        }

        public UserBuilder skype(String skype) {
            newUser.skype = skype;
            return this;
        }

        public UserBuilder additionally(String additionally) {
            newUser.additionally = additionally;
            return this;
        }

        public UserBuilder avatar(Avatar avatar) {
            newUser.avatar = avatar;
            return this;
        }

        public User build() {
            return newUser;
        }
    }
}