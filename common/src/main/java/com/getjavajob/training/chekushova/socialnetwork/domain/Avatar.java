package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

@Entity
public class Avatar implements Serializable {
    private static final long serialVersionUID = -103237766474753408L;

    @Id
    private Long id;
    @Lob
    @Column(columnDefinition = "BLOB")
    private byte[] image;

    public Avatar() {
    }

    public Avatar(Long id, byte[] image) {
        this.id = id;
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getBase64Avatar() {
        return image == null ? null : Base64.getEncoder().encodeToString(image);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Avatar avatar = (Avatar) o;
        return Objects.equals(id, avatar.id) &&
                Arrays.equals(image, avatar.image);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }

    @Override
    public String toString() {
        return "Avatar{" +
                "id=" + id +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}