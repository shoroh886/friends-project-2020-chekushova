package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToOne;
import java.util.Objects;

@Entity
@NamedEntityGraph(name = "friendsFetchAvatar",
        attributeNodes = {
                @NamedAttributeNode(value = "initiator", subgraph = "withAvatar"),
                @NamedAttributeNode(value = "accepting", subgraph = "withAvatar")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "withAvatar",
                        attributeNodes = {
                                @NamedAttributeNode("avatar")
                        }
                )
        })
public class Friendship {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "initiator")
    private User initiator;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "accepting")
    private User accepting;
    private int status;

    public Friendship() {
    }

    public Friendship(User initiator, User friend, int status) {
        this.initiator = initiator;
        this.accepting = friend;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getInitiator() {
        return initiator;
    }

    public void setInitiator(User initiator) {
        this.initiator = initiator;
    }

    public User getAccepting() {
        return accepting;
    }

    public void setAccepting(User accepting) {
        this.accepting = accepting;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return id == that.id &&
                status == that.status &&
                Objects.equals(initiator, that.initiator) &&
                Objects.equals(accepting, that.accepting);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, initiator, accepting, status);
    }

    @Override
    public String toString() {
        return "com.getjavajob.training.chekushova.socialnetwork.common.Friendship{" +
                "id=" + id +
                ", initiator=" + initiator +
                ", accepting=" + accepting +
                ", status=" + status +
                '}';
    }
}