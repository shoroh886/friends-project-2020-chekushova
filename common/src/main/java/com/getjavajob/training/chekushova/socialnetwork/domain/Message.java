package com.getjavajob.training.chekushova.socialnetwork.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import java.time.LocalDateTime;
import java.util.Objects;

import static java.time.LocalDateTime.now;

@Entity
@NamedEntityGraph(name = "Message.detail", attributeNodes = {
        @NamedAttributeNode("sender"),
        @NamedAttributeNode("destination")
})
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender", nullable = false, updatable = false)
    private User sender;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "destination", nullable = false, updatable = false)
    private User destination;
    private String text;
    private LocalDateTime date;
    private boolean status;

    public Message() {
    }

    public Message(User sender, User destination, String text) {
        this(null, sender, destination, text, now(), false);
    }

    public Message(Long id, User sender, User destination, String text, LocalDateTime date, boolean status) {
        this.id = id;
        this.sender = sender;
        this.destination = destination;
        this.text = text;
        this.date = date;
        this.status = status;
    }

    public User getSender() {
        return sender;
    }

    public User getDestination() {
        return destination;
    }

    public String getText() {
        return text;
    }

    public void setText(String message) {
        this.text = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return status == message1.status &&
                Objects.equals(id, message1.id) &&
                sender.equals(message1.sender) &&
                destination.equals(message1.destination) &&
                text.equals(message1.text) &&
                date.equals(message1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sender, destination, text, date, status);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", sender=" + sender +
                ", destination=" + destination +
                ", message='" + text + '\'' +
                ", data=" + date +
                ", status=" + status +
                '}';
    }
}