package com.getjavajob.training.chekushova.socialnetwork.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "discussion_message")
@NamedEntityGraph(name = "DiscusMessage.detail",
        attributeNodes = @NamedAttributeNode(value = "author", subgraph = "withAvatar"),
        subgraphs = {
                @NamedSubgraph(
                        name = "withAvatar",
                        attributeNodes = {
                                @NamedAttributeNode("avatar")
                        }
                )
        }
)
public class DiscussionMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    private String text;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author", nullable = false, updatable = false)
    private User author;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "discussion", nullable = false, updatable = false)
    private Discussion discussion;

    public DiscussionMessage() {
    }

    public DiscussionMessage(Discussion discussion, User author, String text) {
        this.discussion = discussion;
        this.author = author;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public Discussion getDiscussion() {
        return discussion;
    }

    public void setDiscussion(Discussion discussion) {
        this.discussion = discussion;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscussionMessage that = (DiscussionMessage) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(discussion, that.discussion) &&
                Objects.equals(author, that.author) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, discussion, author, text);
    }

    @Override
    public String toString() {
        return "DiscusMessage{" +
                "id=" + id +
                ", discus=" + discussion +
                ", author=" + author +
                ", text='" + text +
                '}';
    }
}