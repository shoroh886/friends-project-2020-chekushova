package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

@Entity
@SqlResultSetMapping(
        name = "ChatMapping",
        entities = @EntityResult(
                entityClass = com.getjavajob.training.chekushova.socialnetwork.domain.Chat.class,
                fields = {
                        @FieldResult(name = "sender", column = "sender"),
                        @FieldResult(name = "name", column = "name"),
                        @FieldResult(name = "surname", column = "surname"),
                        @FieldResult(name = "newMessageCount", column = "cnt"),
                        @FieldResult(name = "avatar", column = "avatar")
                }
        )
)
public class Chat {
    @Id
    private Long sender;
    private String name;
    private String surname;
    @Column(name = "cnt")
    private Integer newMessageCount;
    private byte[] avatar;

    public Chat() {
    }

    public Chat(Long sender, String name, String surname, Integer newMessageCount, byte[] avatar) {
        this.sender = sender;
        this.name = name;
        this.surname = surname;
        this.newMessageCount = newMessageCount;
        this.avatar = avatar;
    }

    public long getSender() {
        return sender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getNewMessageCount() {
        return newMessageCount;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public String getBase64Avatar() {
        return Base64.getEncoder().encodeToString(avatar);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chat chat = (Chat) o;
        return Arrays.equals(avatar, chat.avatar) &&
                Objects.equals(sender, chat.sender) &&
                Objects.equals(name, chat.name) &&
                Objects.equals(surname, chat.surname) &&
                Objects.equals(newMessageCount, chat.newMessageCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, name, surname, newMessageCount, avatar);
    }

    @Override
    public String toString() {
        return "Chat{" +
                "sender=" + sender +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", newMessageCount=" + newMessageCount +
                ", avatarLength=" + avatar.length +
                '}';
    }
}