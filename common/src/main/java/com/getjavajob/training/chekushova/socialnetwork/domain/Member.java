package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SqlResultSetMapping;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

@Entity
@IdClass(MemberId.class)
@SqlResultSetMapping(
        name = "MemberMapping",
        entities = @EntityResult(
                entityClass = Member.class,
                fields = {
                        @FieldResult(name = "group", column = "group"),
                        @FieldResult(name = "account", column = "account"),
                        @FieldResult(name = "name", column = "name"),
                        @FieldResult(name = "surname", column = "surname"),
                        @FieldResult(name = "rights", column = "rights"),
                        @FieldResult(name = "avatar", column = "avatar")}
        )
)
public class Member implements Serializable {
    @Id
    private String group;
    @Id
    private Long account;
    @Column(insertable = false, updatable = false)
    private String name;
    @Column(insertable = false, updatable = false)
    private String surname;
    private int rights;
    @Column(insertable = false, updatable = false)
    private byte[] avatar;

    public Member() {
    }

    public Member(String group, Long account, String name, String surname, int rights, byte[] avatar) {
        this.group = group;
        this.account = account;
        this.name = name;
        this.surname = surname;
        this.rights = rights;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String firstName) {
        this.name = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String secondName) {
        this.surname = secondName;
    }

    public int getRights() {
        return rights;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String groupName) {
        this.group = groupName;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long memberId) {
        this.account = memberId;
    }

    public String getAvatar() {
        return avatar == null ? null : Base64.getEncoder().encodeToString(avatar);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return rights == member.rights &&
                group.equals(member.group) &&
                account.equals(member.account) &&
                name.equals(member.name) &&
                surname.equals(member.surname) &&
                Arrays.equals(avatar, member.avatar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, account, name, surname, rights);
    }

    @Override
    public String toString() {
        return "GroupMember{" +
                "groupName='" + group + '\'' +
                ", memberId=" + account +
                ", firstName='" + name + '\'' +
                ", secondName='" + surname + '\'' +
                ", rights=" + rights +
                '}';
    }
}