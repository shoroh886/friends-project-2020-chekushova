package com.getjavajob.training.chekushova.socialnetwork.domain;

import java.io.Serializable;
import java.util.Objects;

public class MemberId implements Serializable {
    String group;
    Long account;

    public MemberId() {
    }

    public MemberId(String group, Long account) {
        this.group = group;
        this.account = account;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemberId memberId1 = (MemberId) o;
        return Objects.equals(group, memberId1.group) && Objects.equals(account, memberId1.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, account);
    }
}