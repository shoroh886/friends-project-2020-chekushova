package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.SqlResultSetMapping;
import java.util.Objects;

@Entity
@SqlResultSetMapping(
        name = "DiscussionMapping",
        entities = @EntityResult(
                entityClass = Discussion.class,
                fields = {
                        @FieldResult(name = "id", column = "id"),
                        @FieldResult(name = "name", column = "name"),
                        @FieldResult(name = "access", column = "access"),
                        @FieldResult(name = "status", column = "status"),
                        @FieldResult(name = "count", column = "count"),
                        @FieldResult(name = "members", column = "members")
                }
        )
)
@NamedEntityGraph(name = "Discussion.detail", attributeNodes = @NamedAttributeNode("group"))
public class Discussion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int access;
    private boolean status;
    @Column(insertable = false, updatable = false)
    private int count;
    @Column(insertable = false, updatable = false)
    private int members;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "`group`", nullable = false, updatable = false)
    private Group group;

    public Discussion() {
    }

    public Discussion(String name, int access, boolean status) {
        this.name = name;
        this.access = access;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public int getMembers() {
        return members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Discussion discussion = (Discussion) o;
        return access == discussion.access &&
                Objects.equals(id, discussion.id) &&
                Objects.equals(name, discussion.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, access);
    }
}