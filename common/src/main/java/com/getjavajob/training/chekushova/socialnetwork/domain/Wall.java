package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Wall {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long account;
    private String text;
    private Long content;

    public Wall() {
    }

    public Wall(Long account, String text) {
        this(account, text, null);
    }

    public Wall(Long account, String text, Long content) {
        this.account = account;
        this.text = text;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long wallId) {
        this.account = wallId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getContent() {
        return content;
    }

    public void setContent(Long contentId) {
        this.content = contentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wall wall = (Wall) o;
        return id.equals(wall.id) &&
                Objects.equals(account, wall.account) &&
                content.equals(wall.content) &&
                Objects.equals(text, wall.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, text, content);
    }
}