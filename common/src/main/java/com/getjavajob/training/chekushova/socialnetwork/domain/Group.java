package com.getjavajob.training.chekushova.socialnetwork.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "`group`")
public class Group {
    @Id
    private String name;
    private String description;
    private Long avatar;

    public Group() {
    }

    public Group(String groupName, String groupDescription) {
        this.name = groupName;
        this.description = groupDescription;
    }

    public Group(String groupName) {
        this(groupName, "");
    }

    public String getName() {
        return name;
    }

    public void setName(String groupName) {
        this.name = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String groupDescription) {
        this.description = groupDescription;
    }

    public Long getAvatar() {
        return avatar;
    }

    public void setAvatar(Long avatarId) {
        this.avatar = avatarId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return name.equals(group.name) &&
                description.equals(group.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    @Override
    public String toString() {
        return "com.getjavajob.training.chekushova.socialnetwork.common.Group{" +
                "groupName='" + name + '\'' +
                ", groupDescription='" + description + '\'' +
                '}';
    }
}