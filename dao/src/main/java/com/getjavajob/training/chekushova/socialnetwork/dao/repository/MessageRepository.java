package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    @Query(value = "SELECT m FROM Message m " +
            "WHERE (m.sender.id = ?1 AND m.destination.id = ?2) " +
            "OR (m.sender.id = ?2 AND m.destination.id = ?1) ORDER BY m.id DESC")
    List<Message> getMessages(Long sender, Long destination, Pageable page);

    @Modifying
    @Query(value = "UPDATE message " +
            "SET status = true " +
            "WHERE sender = :sender AND destination = :destination AND status = false", nativeQuery = true)
    void readMessages(@Param("sender") Long self, @Param("destination") Long destination);

    @Query(value = "SELECT count(m.id) FROM Message m " +
            "WHERE m.destination.id = ?1 AND m.status = false")
    int getSumUnreadMessages(Long self);
}