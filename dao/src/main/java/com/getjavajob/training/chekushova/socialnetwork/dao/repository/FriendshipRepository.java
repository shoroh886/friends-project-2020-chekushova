package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Friendship;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FriendshipRepository extends JpaRepository<Friendship, Long> {

    @Query(value = "SELECT f FROM Friendship f " +
            "WHERE f.initiator.id = ?1 and f.accepting.id = ?2 " +
            "OR f.initiator.id = ?2 and f.accepting.id = ?1")
    Friendship getFriendship(Long initiator, Long accepting);

    @EntityGraph(value = "friendsFetchAvatar", type = EntityGraph.EntityGraphType.LOAD)
    List<Friendship> findAllByInitiator_IdOrAccepting_Id(Long Initiator_Id, Long Accepting_Id);
}