package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Member;
import com.getjavajob.training.chekushova.socialnetwork.domain.MemberId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupMemberRepository extends JpaRepository<Member, MemberId> {

    @Query(value = "SELECT m.group, u.id AS account, u.name, u.surname, m.rights, a.image AS avatar " +
            "FROM member m " +
            "JOIN `user` u ON u.id = m.account " +
            "JOIN avatar a ON u.id = a.id " +
            "WHERE m.group = :groupName", nativeQuery = true)
    List<Member> findAllByGroupName(@Param("groupName") String groupName);

    @Query(value = "SELECT m.group, u.id AS account, u.name, u.surname, m.rights, a.image AS avatar " +
            "FROM member m " +
            "JOIN `user` u ON u.id = m.account " +
            "JOIN avatar a ON u.id = a.id " +
            "WHERE m.group = :groupName AND m.account = :memberId", nativeQuery = true)
    Member findByGroupNameAndMemberId(@Param("groupName") String groupName, @Param("memberId") Long memberId);

    @Modifying
    @Query(value = "UPDATE member SET rights = :rights WHERE `group` = :groupName AND account = :memberId", nativeQuery = true)
    void updateRightsByGroupNameAndMemberId(@Param("groupName") String groupName, @Param("memberId") Long memberId, @Param("rights") int rights);

    @Modifying
    @Query(value = "INSERT INTO member (`group`, account, rights) values (:groupName, :memberId, :rights)", nativeQuery = true)
    void insertMember(@Param("groupName") String groupName, @Param("memberId") Long memberId, @Param("rights") int rights);

    @Modifying
    @Query(value = "DELETE FROM member WHERE `group` = :groupName AND account = :memberId", nativeQuery = true)
    void deleteByGroupNameAndMemberId(@Param("groupName") String groupName, @Param("memberId") Long memberId);

    void deleteAllByAccount(Long memberId);
}