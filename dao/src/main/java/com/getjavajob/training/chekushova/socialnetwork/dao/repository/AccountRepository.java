package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Account getById(Long id);

    Account getByLogin(String login);

    void deleteById(@NotNull Long id);

    @Query(value = "SELECT a FROM Account a WHERE a.login = ?1")
    List<Account> checkAllLogin(String value);
}