package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    User getById(Long id);

    @Query(value = "SELECT u FROM User u WHERE u.id = ?1")
    @EntityGraph(value = "fullUser", type = EntityGraph.EntityGraphType.LOAD)
    User getByIdFull(Long id);

    @Query(value = "SELECT u FROM User u WHERE u.id = ?1")
    @EntityGraph(value = "withPhones", type = EntityGraph.EntityGraphType.LOAD)
    User getByIdPhones(Long id);

    @Query(value = "SELECT u FROM User u WHERE u.id = ?1")
    @EntityGraph(value = "withAvatar", type = EntityGraph.EntityGraphType.LOAD)
    User getByIdAvatar(Long id);

    List<User> getAllByIdNot(Long id);

    @Query(value = "SELECT u FROM User u " +
            "WHERE (u.name LIKE %?1% OR u.surname LIKE %?1% OR u.patronymic LIKE %?1%) " +
            "AND u.id <> ?2 ")
    List<User> getFiltered(String filter, Long selfId, Pageable page);

    @Query(value = "SELECT u FROM User u " +
            "WHERE (u.name LIKE %?1% OR u.surname LIKE %?1% OR u.patronymic LIKE %?1%) " +
            "AND u.id <> ?2 ")
    @EntityGraph(value = "withAvatar", type = EntityGraph.EntityGraphType.LOAD)
    List<User> getFilteredAvatar(String filter, Long selfId, Pageable page);

    @Query(value = "SELECT count(u.id) FROM User u " +
            "WHERE (u.name LIKE %?1% OR u.surname LIKE %?1% OR u.patronymic LIKE %?1%) " +
            "AND u.id <> ?2 ")
    int getCount(String filter, Long selfId);

    @Query(value = "SELECT u FROM User u WHERE u.icq = ?1")
    List<User> checkAllIcq(String value);

    @Query(value = "SELECT u FROM User u WHERE u.skype = ?1")
    List<User> checkAllSkype(String value);

    @Query(value = "SELECT u FROM User u WHERE u.email = ?1")
    List<User> checkAllEmail(String value);

    @Query(value = "SELECT u FROM User u")
    List<User> getAll();
}