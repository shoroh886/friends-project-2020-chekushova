package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Discussion;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface DiscussionRepository extends JpaRepository<Discussion, Long> {

    @Query(value = "SELECT d.id, d.group, d.name, d.access, d.status, count(dm.id) as `count`, count(distinct dm.author) as members " +
            "FROM discussion d JOIN discussion_message dm on d.id = dm.discussion " +
            "WHERE d.access <= :access and d.group = :groupName GROUP BY d.id ORDER BY `count` DESC", nativeQuery = true)
    List<Discussion> findByGroupName(@Param("groupName") String groupName, @Param("access") int access, Pageable pageable);

    @NotNull
    @Override
    @Query(value = "SELECT d.id, d.group, d.name, d.access, d.status, count(dm.id) as `count`, count(distinct dm.author) as members " +
            "FROM discussion d JOIN discussion_message dm on d.id = dm.discussion " +
            "WHERE d.id = :id", nativeQuery = true)
    Optional<Discussion> findById(@Param("id") @NotNull Long id);

    @Modifying
    @Query(value = "UPDATE discussion SET access = :access, status = :status WHERE id = :discussionId", nativeQuery = true)
    void updateOptions(@Param("discussionId") Long discussionId, @Param("access") int access, @Param("status") boolean status);
}