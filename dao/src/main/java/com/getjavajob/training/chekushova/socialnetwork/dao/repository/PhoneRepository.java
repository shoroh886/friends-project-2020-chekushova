package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PhoneRepository extends JpaRepository<Phone, Long>, JpaSpecificationExecutor<Phone> {

    @Query(value = "SELECT p.number FROM Phone p")
    List<String> getAllNumbers();

    @Query(value = "SELECT p FROM Phone p")
    List<Phone> getAll();

    @Query(value = "SELECT p FROM Phone p WHERE p.number = ?1 AND p.user.id <> ?2")
    List<Phone> checkAllPhone(String value, Long userId);

    @Query(value = "SELECT p FROM Phone p WHERE p.number = ?1")
    List<Phone> checkAllPhone(String value);
}