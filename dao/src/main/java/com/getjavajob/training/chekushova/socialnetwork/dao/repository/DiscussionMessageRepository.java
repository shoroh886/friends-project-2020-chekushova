package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Discussion;
import com.getjavajob.training.chekushova.socialnetwork.domain.DiscussionMessage;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiscussionMessageRepository extends JpaRepository<DiscussionMessage, Long> {

    @EntityGraph(value = "DiscusMessage.detail", type = EntityGraphType.LOAD)
    List<DiscussionMessage> findAllByDiscussionOrderByIdDesc(Discussion discussion, Pageable pageable);
}