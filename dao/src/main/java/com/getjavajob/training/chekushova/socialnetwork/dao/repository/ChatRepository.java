package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ChatRepository extends JpaRepository<Chat, Long> {

    @Query(value = "SELECT m.sender, u.name, u.surname, " +
            "SUM(CASE WHEN status = false THEN 1 ELSE 0 END) AS cnt, a.image as avatar  " +
            "FROM message as m " +
            "JOIN `user` u on u.id = m.sender " +
            "JOIN avatar a on u.id = a.id " +
            "WHERE destination = ? GROUP BY u.name, u.surname, u.id", nativeQuery = true)
    List<Chat> getChatList(@Param("destination") long destination);
}