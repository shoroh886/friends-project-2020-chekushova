package com.getjavajob.training.chekushova.socialnetwork.dao.repository;

import com.getjavajob.training.chekushova.socialnetwork.domain.Group;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, String> {

    long countByNameContainingIgnoreCase(String filter);

    List<Group> findByNameContainingIgnoreCase(String filter, Pageable pageable);

    Group findByName(String groupName);

    void deleteByName(String groupName);

    @Query(value = "SELECT g.name, description, m.avatar " +
            "FROM `group` g " +
            "JOIN member m ON m.group = g.name WHERE m.account = :memberId", nativeQuery = true)
    List<Group> findAllByMemberId(@Param("memberId") Long memberId);
}