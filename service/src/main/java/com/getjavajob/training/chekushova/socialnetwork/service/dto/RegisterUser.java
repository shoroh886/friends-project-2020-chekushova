package com.getjavajob.training.chekushova.socialnetwork.service.dto;

import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RegisterUser {
    @Size(min = 4, max = 45)
    private String login;
    @Size(min = 4, max = 45)
    private String password;
    @Size(min = 4, max = 45)
    private String repeatPassword;
    @NotBlank
    @Size(max = 15)
    private String name;
    @NotBlank
    @Size(max = 15)
    private String surname;
    @Size(max = 15)
    private String patronymic;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    @Size(min = 1, max = 5)
    @Valid
    private List<Phone> phones = new ArrayList<>();
    @PastOrPresent
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birth;
    @Size(max = 90)
    private String home;
    @Size(max = 90)
    private String work;
    @Email
    @NotBlank
    @Size(max = 45)
    private String email;
    @Pattern(regexp = "(^$)|(^\\d{9}$)")
    private String icq;
    @Size(max = 45)
    private String skype;
    @Size(max = 255)
    private String additionally;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionally() {
        return additionally;
    }

    public void setAdditionally(String additionally) {
        this.additionally = additionally;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterUser that = (RegisterUser) o;
        return Objects.equals(login, that.login) &&
                Objects.equals(password, that.password) &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(phones, that.phones) &&
                Objects.equals(birth, that.birth) &&
                Objects.equals(home, that.home) &&
                Objects.equals(work, that.work) &&
                Objects.equals(email, that.email) &&
                Objects.equals(icq, that.icq) &&
                Objects.equals(skype, that.skype) &&
                Objects.equals(additionally, that.additionally);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password, name, surname, patronymic, phones, birth, home, work, email, icq, skype, additionally);
    }

    public static class UserBuilder {
        private final RegisterUser newUser;

        public UserBuilder() {
            newUser = new RegisterUser();
        }

        public UserBuilder login(String login) {
            newUser.login = login;
            return this;
        }

        public UserBuilder password(String password) {
            newUser.password = password;
            return this;
        }

        public UserBuilder repeatPassword(String repeatPassword) {
            newUser.repeatPassword = repeatPassword;
            return this;
        }

        public UserBuilder name(String name) {
            newUser.name = name;
            return this;
        }

        public UserBuilder surname(String surname) {
            newUser.surname = surname;
            return this;
        }

        public UserBuilder patronymic(String patronymic) {
            newUser.patronymic = patronymic;
            return this;
        }

        public UserBuilder phones(List<Phone> phones) {
            newUser.phones = phones;
            return this;
        }

        public UserBuilder birth(LocalDate birth) {
            newUser.birth = birth;
            return this;
        }

        public UserBuilder homeAddress(String homeAddress) {
            newUser.home = homeAddress;
            return this;
        }

        public UserBuilder workAddress(String workAddress) {
            newUser.work = workAddress;
            return this;
        }

        public UserBuilder email(String email) {
            newUser.email = email;
            return this;
        }

        public UserBuilder icq(String icq) {
            newUser.icq = icq;
            return this;
        }

        public UserBuilder skype(String skype) {
            newUser.skype = skype;
            return this;
        }

        public UserBuilder additionally(String additionally) {
            newUser.additionally = additionally;
            return this;
        }

        public RegisterUser build() {
            return newUser;
        }
    }
}