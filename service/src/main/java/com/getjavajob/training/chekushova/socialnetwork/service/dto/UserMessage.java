package com.getjavajob.training.chekushova.socialnetwork.service.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.singletonList;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = -6737524093987265685L;

    private String name;
    private String email;
    private List<String> groups;

    public UserMessage(String name, String email) {
        this.name = name;
        this.email = email;
        this.groups = singletonList("user");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMessage that = (UserMessage) o;
        return name.equals(that.name) &&
                email.equals(that.email) &&
                groups.equals(that.groups);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, groups);
    }
}
