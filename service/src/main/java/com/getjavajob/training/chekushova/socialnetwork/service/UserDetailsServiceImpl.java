package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.AccountRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import static com.getjavajob.training.chekushova.socialnetwork.domain.SecurityAccount.fromUser;

@Component("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    private AccountRepository accountRepository;

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) {
        Account account = accountRepository.getByLogin(login);
        return fromUser(account);
    }
}