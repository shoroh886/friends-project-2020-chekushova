package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.service.dto.EmailMessage;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class EmailingService {
    private final JmsTemplate template;

    public EmailingService(JmsTemplate template) {
        this.template = template;
    }

    public void sendEmailing(EmailMessage message) {
        template.convertAndSend("mailing", message);
    }
}
