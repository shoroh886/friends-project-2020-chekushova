package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.ChatRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.MessageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Chat;
import com.getjavajob.training.chekushova.socialnetwork.domain.Message;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.data.domain.PageRequest.of;

@Component
public class ChatService {
    private static final int MESSAGE_PAGE_SIZE = 5;
    private final ChatRepository chatRepository;
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    public ChatService(ChatRepository chatRepository, MessageRepository messageRepository, UserRepository userRepository) {
        this.chatRepository = chatRepository;
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public Message sendMessage(Long senderId, Long destinationId, String message) {
        User sender = userRepository.getById(senderId);
        User destination = userRepository.getById(destinationId);
        Message newMessage = new Message(sender, destination, message);
        return messageRepository.save(newMessage);
    }

    @Transactional
    public List<Message> showChat(long selfId, long userId, int page) {
        Pageable pageable = of(page - 1, MESSAGE_PAGE_SIZE);
        return messageRepository.getMessages(selfId, userId, pageable);
    }

    @Transactional
    public List<Chat> getChatList(long selfId) {
        return chatRepository.getChatList(selfId);
    }

    @Transactional
    public int checkMessages(User self) {
        return messageRepository.getSumUnreadMessages(self.getId());
    }

    @Transactional
    public void readAll(User self, long userId) {
        messageRepository.readMessages(userId, self.getId());
    }
}
