package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.AccountRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.PhoneRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.RegisterUser;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Component
public class UserValidator implements Validator {
    private final UserRepository userRepository;
    private final PhoneRepository phoneRepository;
    private final AccountRepository accountRepository;
    private final javax.validation.Validator validator;
    private final ApplicationContext appContext;

    public UserValidator(UserRepository userRepository, AccountRepository accountRepository,
                         ApplicationContext applicationContext, PhoneRepository phoneRepository) {
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.appContext = applicationContext;
        this.phoneRepository = phoneRepository;
        this.validator = applicationContext.getBean("validator", LocalValidatorFactoryBean.class).getValidator();
    }

    @Override
    public boolean supports(@NotNull Class<?> user) {
        return RegisterUser.class.equals(user);
    }

    @Override
    public void validate(@NotNull Object user, @NotNull Errors errors) {
        RegisterUser newUser = (RegisterUser) user;
        String busy = "registration.error.busy";
        String mismatch = "registration.error.mismatch";
        String duplicate = "registration.error.duplicate";
        if (!newUser.getPassword().equals(newUser.getRepeatPassword())) {
            String message = appContext.getMessage(mismatch, new Object[]{}, getLocale());
            errors.rejectValue("password", "", message);
        }
        if (!accountRepository.checkAllLogin(newUser.getLogin()).isEmpty()) {
            String message = appContext.getMessage(busy, new Object[]{}, getLocale());
            errors.rejectValue("login", "", message);
        }
        if (!userRepository.checkAllEmail(newUser.getEmail()).isEmpty()) {
            String message = appContext.getMessage(busy, new Object[]{}, getLocale());
            errors.rejectValue("email", "", message);
        }
        if (!newUser.getIcq().isEmpty() && !userRepository.checkAllIcq(newUser.getIcq()).isEmpty()) {
            String message = appContext.getMessage(busy, new Object[]{}, getLocale());
            errors.rejectValue("icq", "", message);
        }
        if (!newUser.getSkype().isEmpty() && !userRepository.checkAllSkype(newUser.getSkype()).isEmpty()) {
            String message = appContext.getMessage(busy, new Object[]{}, getLocale());
            errors.rejectValue("skype", "", message);
        }
        for (int i = 0; i < newUser.getPhones().size(); i++) {
            if (!phoneUniqCheck(newUser.getPhones().get(i).getNumber())) {
                String message = appContext.getMessage(busy, new Object[]{}, getLocale());
                errors.rejectValue("phones[" + i + "].phone", "", message);
            }
            if (phoneUniqCheck(newUser.getPhones().get(i).getNumber(), newUser.getPhones())) {
                String message = appContext.getMessage(duplicate, new Object[]{}, getLocale());
                errors.rejectValue("phones[" + i + "].phone", "", message);
            }
        }
    }

    public void validateUser(RegisterUser newUser, Errors errors) {
        Set<ConstraintViolation<RegisterUser>> validates = validator.validate(newUser);
        for (ConstraintViolation<RegisterUser> constraintViolation : validates) {
            String propertyPath = constraintViolation.getPropertyPath().toString();
            String message = constraintViolation.getMessage();
            errors.rejectValue(propertyPath, "", message);
        }
        validate(newUser, errors);
    }

    private boolean phoneUniqCheck(String phone) {
        return phoneRepository.checkAllPhone(phone).isEmpty();
    }

    private boolean phoneUniqCheck(String number, List<Phone> phones) {
        int cnt = 0;
        for (Phone phone : phones) {
            if (number.equals(phone.getNumber())) {
                cnt++;
            }
        }
        return cnt > 1;
    }

    public boolean checkUniq(String value, String sql, Long userId) {
        switch (sql) {
            case "icq":
                return !userRepository.checkAllIcq(value).isEmpty();
            case "skype":
                return !userRepository.checkAllSkype(value).isEmpty();
            case "email":
                return !userRepository.checkAllEmail(value).isEmpty();
            case "login":
                return !accountRepository.checkAllLogin(value).isEmpty();
            case "phone":
                return !phoneRepository.checkAllPhone(value, userId).isEmpty();
            default:
                return false;
        }
    }
}

