package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.FriendshipRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Friendship;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class FriendshipService {
    private static final int REQUEST = 1;
    private static final int SUBSCRIBER = 2;
    private static final int FRIENDSHIP = 3;

    private final UserRepository userRepository;
    private final FriendshipRepository friendshipRepository;

    public FriendshipService(UserRepository userRepository, FriendshipRepository friendshipRepository) {
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
    }

    @Transactional
    public Map<Integer, List<User>> getAllFriendships(long userId) {
        List<Friendship> friendships = friendshipRepository.findAllByInitiator_IdOrAccepting_Id(userId, userId);
        List<User> friends = new ArrayList<>();
        List<User> requests = new ArrayList<>();
        List<User> subscribers = new ArrayList<>();
        List<User> subscriptions = new ArrayList<>();
        for (Friendship friendship : friendships) {
            if (friendship.getStatus() == FRIENDSHIP) {
                friends.add(friendship.getInitiator().getId() == userId ? friendship.getAccepting() : friendship.getInitiator());
            } else if (friendship.getStatus() == REQUEST) {
                if (friendship.getAccepting().getId() == userId) {
                    requests.add(friendship.getInitiator());
                }
            } else if (friendship.getStatus() == SUBSCRIBER) {
                if (friendship.getAccepting().getId() == userId) {
                    subscribers.add(friendship.getInitiator());
                } else {
                    subscriptions.add(friendship.getAccepting());
                }
            }
        }
        Map<Integer, List<User>> contacts = new HashMap<>();
        contacts.put(4, friends);
        contacts.put(3, subscribers);
        contacts.put(2, subscriptions);
        contacts.put(1, requests);
        return contacts;
    }

    @Transactional
    public Friendship getFriendship(long selfId, long userId) {
        return friendshipRepository.getFriendship(selfId, userId);
    }

    @Transactional
    public void addFriend(long selfId, long friendId) {
        User friend = userRepository.getById(friendId);
        User self = userRepository.getById(selfId);
        friendshipRepository.save(new Friendship(self, friend, REQUEST));
    }

    @Transactional
    public void acceptFriendship(long selfId, long friendId) {
        Friendship friendship = friendshipRepository.getFriendship(selfId, friendId);
        friendship.setStatus(FRIENDSHIP);
    }

    @Transactional
    public void rejectFriendship(long selfId, long friendId) {
        Friendship friendship = friendshipRepository.getFriendship(selfId, friendId);
        friendship.setStatus(SUBSCRIBER);
        friendship.setInitiator(userRepository.getById(friendId));
        friendship.setAccepting(userRepository.getById(selfId));
    }

    @Transactional
    public void removeFriend(long selfId, long friendId) {
        Friendship friendship = friendshipRepository.getFriendship(selfId, friendId);
        User self = userRepository.getById(selfId);
        int status = friendship.getStatus();
        if (status == FRIENDSHIP) {
            if (self.getId().equals(friendship.getInitiator().getId())) {
                User oldInitiator = friendship.getInitiator();
                friendship.setInitiator(friendship.getAccepting());
                friendship.setAccepting(oldInitiator);
                friendship.setStatus(SUBSCRIBER);
            } else if (self.getId().equals(friendship.getAccepting().getId())) {
                friendship.setStatus(SUBSCRIBER);
            }
        } else if (status == SUBSCRIBER || status == REQUEST) {
            friendshipRepository.delete(friendship);
        }
    }
}