package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.DiscussionMessageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.DiscussionRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.GroupMemberRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.GroupRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Discussion;
import com.getjavajob.training.chekushova.socialnetwork.domain.DiscussionMessage;
import com.getjavajob.training.chekushova.socialnetwork.domain.Group;
import com.getjavajob.training.chekushova.socialnetwork.domain.Member;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;
import static org.springframework.data.domain.PageRequest.of;

@Component
public class GroupService {
    private static final int SEARCH_PAGE_SIZE = 5;
    private static final int DISCUS_PAGE_SIZE = 10;
    private static final int MESSAGES_PAGE_SIZE = 10;
    private static final int REQUEST = 1;
    private static final int MEMBER = 2;
    private static final int ADMIN = 3;
    private static final int CREATOR = 4;
    private static final int MIN_GROUP_NAME_LENGTH = 8;
    private static final int MAX_GROUP_NAME_LENGTH = 90;
    private static final Pattern GROUP_NAME_PATTERN = compile("^[\\w&&[^_]&&a-z0-9]+(\\s*[\\w&&[^_]&&a-z0-9])*${8,90}");

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final GroupMemberRepository groupMemberRepository;
    private final DiscussionRepository discussionRepository;
    private final DiscussionMessageRepository discussionMessageRepository;

    public GroupService(UserRepository userRepository, GroupRepository groupRepository,
                        GroupMemberRepository groupMemberRepository, DiscussionRepository discussionRepository,
                        DiscussionMessageRepository discussionMessageRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.groupMemberRepository = groupMemberRepository;
        this.discussionRepository = discussionRepository;
        this.discussionMessageRepository = discussionMessageRepository;
    }

    @Transactional
    public boolean createGroup(Group group, Long selfId) {
        if (GROUP_NAME_PATTERN.matcher(group.getName().toLowerCase()).matches()) {
            group.setName(replaceNameBase(group.getName()));
            groupRepository.save(group);
            groupMemberRepository.insertMember(group.getName(), selfId, CREATOR);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public void join(String groupName, Long memberId) {
        groupMemberRepository.insertMember(replaceNameBase(groupName), memberId, REQUEST);
    }

    @Transactional
    public void acceptRequest(String groupName, Long memberId) {
        groupMemberRepository.updateRightsByGroupNameAndMemberId(replaceNameBase(groupName), memberId, MEMBER);
    }

    @Transactional
    public void approveRank(String groupName, Long memberId) {
        groupMemberRepository.updateRightsByGroupNameAndMemberId(replaceNameBase(groupName), memberId, ADMIN);
    }

    @Transactional
    public void giveGroup(String groupName, Long memberId, Long selfId) {
        groupMemberRepository.updateRightsByGroupNameAndMemberId(replaceNameBase(groupName), memberId, CREATOR);
        groupMemberRepository.updateRightsByGroupNameAndMemberId(replaceNameBase(groupName), selfId, ADMIN);
    }

    @Transactional
    public void downgrade(String groupName, Long memberId) {
        groupMemberRepository.updateRightsByGroupNameAndMemberId(replaceNameBase(groupName), memberId, MEMBER);
    }

    @Transactional
    public void deleteUser(String groupName, Long memberId) {
        groupMemberRepository.deleteByGroupNameAndMemberId(replaceNameBase(groupName), memberId);
    }

    @Transactional
    public void deleteGroup(String groupName) {
        groupRepository.deleteByName(replaceNameBase(groupName));
    }

    @Transactional
    public int checkRights(String groupName, Long userId) {
        Member member = groupMemberRepository.findByGroupNameAndMemberId(replaceNameBase(groupName), userId);
        return member == null ? 0 : member.getRights();
    }

    @Transactional
    public List<Member> getMembers(String groupName) {
        return groupMemberRepository.findAllByGroupName(replaceNameBase(groupName));
    }

    @Transactional
    public List<Group> getUserGroups(Long memberId) {
        return groupRepository.findAllByMemberId(memberId);
    }

    @Transactional
    public Group findGroup(String groupName) {
        return groupRepository.findByName(replaceNameBase(groupName));
    }

    @Transactional
    public List<Group> getFilteredGroups(String filter, int page) {
        Pageable pageable = of(page - 1, SEARCH_PAGE_SIZE);
        return groupRepository.findByNameContainingIgnoreCase(filter, pageable);
    }

    @Transactional
    public Long getFilteredCount(String filter) {
        return groupRepository.countByNameContainingIgnoreCase(filter);
    }

    @Transactional
    public void createDiscussion(String groupName, Discussion discussion, DiscussionMessage message) {
        discussion.setGroup(groupRepository.findByName(groupName));
        Discussion newDiscussion = discussionRepository.save(discussion);
        message.setDiscussion(newDiscussion);
        discussionMessageRepository.save(message);
    }

    @Transactional
    public DiscussionMessage createMessage(Long discussionId, Long authorId, String text) {
        DiscussionMessage message = new DiscussionMessage(discussionRepository.findById(discussionId).orElse(null),
                userRepository.getByIdAvatar(authorId), text);
        discussionMessageRepository.save(message);
        return message;
    }

    @Transactional
    public Discussion getDiscussion(Long id) {
        return discussionRepository.findById(id).orElse(null);
    }

    @Transactional
    public List<Discussion> getDiscussionPage(String groupName, int access, int page) {
        Pageable pageable = of(page - 1, DISCUS_PAGE_SIZE);
        return discussionRepository.findByGroupName(replaceNameBase(groupName), access, pageable);
    }

    @Transactional
    public List<DiscussionMessage> getDiscussionMessagePage(Long discusId, int page) {
        Pageable pageable = of(page - 1, MESSAGES_PAGE_SIZE);
        return discussionMessageRepository.findAllByDiscussionOrderByIdDesc(discussionRepository.getOne(discusId), pageable);
    }

    @Transactional
    public void changeDiscussionOptions(Long discussionId, String option) {
        int access = option.charAt(0) - '0';
        boolean status = option.charAt(1) == '1';
        discussionRepository.updateOptions(discussionId, access, status);
    }

    @Transactional
    public String validateGroup(String groupName) {
        if (groupName.length() < MIN_GROUP_NAME_LENGTH) {
            return "short";
        } else if (groupName.length() > MAX_GROUP_NAME_LENGTH) {
            return "long";
        } else if (!GROUP_NAME_PATTERN.matcher(groupName.toLowerCase()).matches()) {
            return "incorrect";
        } else {
            List<Group> groups = groupRepository.findAll();
            String testName = groupName.replace(" ", "_");
            for (Group group : groups) {
                if (group.getName().equalsIgnoreCase(testName)) {
                    return "taken";
                }
            }
        }
        return null;
    }

    public String replaceNameBase(String groupName) {
        return groupName.replace("%20", "_").replace(" ", "_");
    }

    public String replaceNameClient(String groupName) {
        return groupName.replace("_", " ");
    }

    public List<Group> fixGroupsName(List<Group> groups) {
        for (Group group : groups) {
            group.setName(replaceNameClient(group.getName()));
        }
        return groups;
    }
}