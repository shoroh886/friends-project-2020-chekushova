package com.getjavajob.training.chekushova.socialnetwork.service.dto;

import com.getjavajob.training.chekushova.socialnetwork.domain.Group;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;

import java.util.Objects;

public class SearchResult {
    private String searchField;
    private String type;
    private String link;

    public SearchResult(String searchField, String type, String link) {
        this.searchField = searchField;
        this.type = type;
        this.link = link;
    }

    public static SearchResult parseGroup(Group group) {
        return new SearchResult(group.getName(), "Group", "/group" + group.getName());
    }

    public static SearchResult parseUser(User user) {
        return new SearchResult(user.getName() + " " + user.getSurname() + " " + user.getPatronymic(), "User", "/id" +
                user.getId().toString());
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchResult that = (SearchResult) o;
        return searchField.equals(that.searchField) &&
                type.equals(that.type) &&
                link.equals(that.link);
    }

    @Override
    public int hashCode() {
        return Objects.hash(searchField, type, link);
    }
}