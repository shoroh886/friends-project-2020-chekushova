package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.AccountRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.ImageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.WallRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import com.getjavajob.training.chekushova.socialnetwork.domain.Avatar;
import com.getjavajob.training.chekushova.socialnetwork.domain.Image;
import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.domain.Wall;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.RegisterUser;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.springframework.data.domain.PageRequest.of;

@Component
public class AccountService {
    private static final int SEARCH_PAGE_SIZE = 5;
    private static final int WALL_PAGE_SIZE = 7;
    private static final Logger usersLogger = LoggerFactory.getLogger("users");
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final WallRepository wallRepository;
    private final ImageRepository imageRepository;
    private final JmsTemplate template;

    public AccountService(AccountRepository accountRepository, UserRepository userRepository,
                          WallRepository wallRepository, ImageRepository imageRepository, JmsTemplate template) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.wallRepository = wallRepository;
        this.imageRepository = imageRepository;
        this.template = template;
    }

    @Transactional
    public List<Wall> getWallById(Long wallId, int page) {
        Pageable pageable = of(page, WALL_PAGE_SIZE);
        return wallRepository.findAllByAccountOrderByIdDesc(wallId, pageable);
    }

    @Transactional
    public void addImageWall(Wall wall, Image image) {
        if (image.getContent() != null) {
            wall.setContent(imageRepository.save(image).getId());
        }
        wallRepository.save(wall);
    }

    @Transactional
    public void createAccount(Account account, User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12);
        account.setPassword(encoder.encode(account.getPassword()));
        Long newId = accountRepository.save(account).getId();
        user.setId(newId);
        user.setAvatar(new Avatar(newId, null));
        for (Phone phone : user.getPhones()) {
            phone.setUser(user);
        }
        userRepository.save(user);
        usersLogger.info("New user: id = {}. name = {}. surname = {}", user.getId(), user.getName(), user.getSurname());
//        template.convertAndSend("users", new UserMessage(user.getName(), user.getEmail()));
    }

    @Transactional
    public void changeUser(User newUser) {
        if (newUser.getAvatar() == null) {
            newUser.setAvatar(userRepository.getByIdAvatar(newUser.getId()).getAvatar());
        }
        for (Phone phone : newUser.getPhones()) {
            phone.setUser(newUser);
        }
        userRepository.save(newUser);
    }

    @Transactional
    public Account getAccount(Long id) {
        return accountRepository.getById(id);
    }

    @Transactional
    public Account getAccountByLogin(String login) {
        return accountRepository.getByLogin(login);
    }

    @Transactional
    public User getFullUser(long id) {
        User user = userRepository.getByIdFull(id);
        if (user == null) {
            logger.warn("User: id={} not found", id);
        }
        return user;
    }

    @Transactional
    public User getUserAvatar(long id) {
        return userRepository.getByIdAvatar(id);
    }

    @Transactional
    public List<User> getAnotherFilteredUsers(String filter, int page, Long selfId) {
        Pageable pageable = of(page - 1, SEARCH_PAGE_SIZE);
        return userRepository.getFiltered(filter, selfId, pageable);
    }

    @Transactional
    public List<User> getAnotherFilteredUsersAvatar(String filter, int page, Long selfId) {
        Pageable pageable = of(page - 1, SEARCH_PAGE_SIZE);
        return userRepository.getFilteredAvatar(filter, selfId, pageable);
    }

    @Transactional
    public int getFilteredCount(String filter, Long selfId) {
        return userRepository.getCount(filter, selfId);
    }

    @Transactional
    public void deleteAccount(long id) {
        accountRepository.deleteById(id);
    }

    public User createUser(RegisterUser registerUser) {
        return new User.UserBuilder()
                .name(registerUser.getName())
                .surname(registerUser.getSurname())
                .patronymic(registerUser.getPatronymic())
                .phones(registerUser.getPhones())
                .birth(registerUser.getBirth())
                .homeAddress(registerUser.getHome())
                .workAddress(registerUser.getWork())
                .email(registerUser.getEmail())
                .icq(registerUser.getIcq())
                .skype(registerUser.getSkype())
                .additionally(registerUser.getAdditionally())
                .build();
    }
}