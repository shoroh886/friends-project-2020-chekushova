package com.getjavajob.training.chekushova.socialnetwork.service.dto;

import java.io.Serializable;
import java.util.Objects;

public class EmailMessage implements Serializable {
    private static final long serialVersionUID = 4057711782425724085L;

    private String group;
    private String header;
    private String text;

    public EmailMessage(String group, String header, String text) {
        this.group = group;
        this.header = header;
        this.text = text;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailMessage that = (EmailMessage) o;
        return group.equals(that.group) &&
                header.equals(that.header) &&
                text.equals(that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, header, text);
    }
}
