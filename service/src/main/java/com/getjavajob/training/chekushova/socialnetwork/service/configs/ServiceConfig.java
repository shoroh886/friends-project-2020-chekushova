package com.getjavajob.training.chekushova.socialnetwork.service.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@EnableTransactionManagement
public class ServiceConfig {

    @Bean(name = "validator")
    public LocalValidatorFactoryBean validatorFactoryBean(ResourceBundleMessageSource bundleMessageSource) {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(bundleMessageSource);
        return localValidatorFactoryBean;
    }

    @Bean
    public ResourceBundleMessageSource bundleMessageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("validationMessages", "localeText");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
}