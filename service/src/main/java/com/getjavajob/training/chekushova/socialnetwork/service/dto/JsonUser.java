package com.getjavajob.training.chekushova.socialnetwork.service.dto;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

public class JsonUser {
    private String login;
    private String password;
    private Long id;
    private String name;
    private String surname;
    private String patronymic;
    private Object[] phones;
    private LocalDate birth;
    private String homeAddress;
    private String workAddress;
    private String email;
    private String icq;
    private String skype;
    private String additionally;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Object[] getPhones() {
        return phones;
    }

    public void setPhones(Object[] phones) {
        this.phones = phones;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionally() {
        return additionally;
    }

    public void setAdditionally(String additionally) {
        this.additionally = additionally;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JsonUser jsonUser = (JsonUser) o;
        return Objects.equals(login, jsonUser.login) &&
                Objects.equals(password, jsonUser.password) &&
                Objects.equals(id, jsonUser.id) &&
                Objects.equals(name, jsonUser.name) &&
                Objects.equals(surname, jsonUser.surname) &&
                Objects.equals(patronymic, jsonUser.patronymic) &&
                Arrays.equals(phones, jsonUser.phones) &&
                Objects.equals(birth, jsonUser.birth) &&
                Objects.equals(homeAddress, jsonUser.homeAddress) &&
                Objects.equals(workAddress, jsonUser.workAddress) &&
                Objects.equals(email, jsonUser.email) &&
                Objects.equals(icq, jsonUser.icq) &&
                Objects.equals(skype, jsonUser.skype) &&
                Objects.equals(additionally, jsonUser.additionally);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(login, password, id, name, surname, patronymic, birth, homeAddress,
                workAddress, email, icq, skype, additionally);
        result = 31 * result + Arrays.hashCode(phones);
        return result;
    }

    @Override
    public String toString() {
        return "JsonUser{" +
                "login='" + login + '\'' +
                ", id=" + id +
                ", firstName='" + name + '\'' +
                ", secondName='" + surname + '\'' +
                ", lastName='" + patronymic + '\'' +
                ", phones=" + Arrays.toString(phones) +
                ", birth=" + birth +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", eMail='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", additionally='" + additionally + '\'' +
                '}';
    }
}