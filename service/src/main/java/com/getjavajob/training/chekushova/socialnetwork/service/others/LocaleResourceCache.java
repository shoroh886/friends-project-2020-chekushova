package com.getjavajob.training.chekushova.socialnetwork.service.others;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Properties;
import java.util.TreeMap;

@Component
public class LocaleResourceCache {
    private static final Logger CACHE_LOGGER = LoggerFactory.getLogger(LocaleResourceCache.class);
    private final NavigableMap<String, String> resources = new TreeMap<>();

    public Map<String, String> getResources(String key) {
        if (resources.isEmpty()) {
            prepareResources();
        }
        char lastChar = key.charAt(key.length() - 1);
        lastChar = (char) ((int) lastChar + 1);
        StringBuilder keyLast = new StringBuilder(key);
        keyLast.replace(keyLast.length() - 1, keyLast.length(), Character.toString(lastChar));
        String first = resources.floorKey(key);
        String last = resources.higherKey(keyLast.toString());
        Map<String, String> pageResources;
        if (first == null) {
            pageResources = resources.headMap(last, false);
        } else if (last == null) {
            pageResources = resources.tailMap(first, false);
        } else {
            pageResources = resources.subMap(first, false, last, false);
        }
        CACHE_LOGGER.trace("Found {} page resources. Submitted.", pageResources.size());
        return pageResources;
    }

    public void prepareResources() {
        CACHE_LOGGER.trace("Preparing the cache");
        Properties locale = new Properties();
        try {
            locale.load(this.getClass().getClassLoader().getResourceAsStream("localeText_ru.properties"));
            for (String name : locale.stringPropertyNames()) {
                resources.put("ru." + name, locale.getProperty(name));
            }
            locale.clear();
            locale.load(this.getClass().getClassLoader().getResourceAsStream("localeText_en.properties"));
            for (String name : locale.stringPropertyNames()) {
                resources.put("en." + name, locale.getProperty(name));
            }
            CACHE_LOGGER.trace("Finish. Loaded resources : {}", resources.size());
        } catch (IOException e) {
            CACHE_LOGGER.error(e.toString(), e);
        }
    }
}