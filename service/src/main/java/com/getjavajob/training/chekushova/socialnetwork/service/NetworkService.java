package com.getjavajob.training.chekushova.socialnetwork.service;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.ImageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import com.getjavajob.training.chekushova.socialnetwork.domain.Image;
import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.JsonUser;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class NetworkService {
    private final UserRepository userRepository;
    private final ImageRepository imageRepository;
    private final ObjectMapper objectMapper;

    public NetworkService(UserRepository userRepository, ImageRepository imageRepository) {
        this.objectMapper = new ObjectMapper();
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Transactional
    public JsonUser createJsonUser(Account account) {
        User user = userRepository.getByIdFull(account.getId());
        JsonUser jsonUser = new JsonUser();
        jsonUser.setLogin(account.getLogin());
        jsonUser.setPassword(account.getPassword());
        jsonUser.setId(account.getId());
        jsonUser.setName(user.getName());
        jsonUser.setSurname(user.getSurname());
        jsonUser.setPatronymic(user.getPatronymic());
        jsonUser.setPhones(user.getPhones().toArray());
        jsonUser.setBirth(user.getBirth());
        jsonUser.setHomeAddress(user.getHome());
        jsonUser.setWorkAddress(user.getWork());
        jsonUser.setEmail(user.getEmail());
        jsonUser.setIcq(user.getIcq());
        jsonUser.setSkype(user.getSkype());
        jsonUser.setAdditionally(user.getAdditionally());
        return jsonUser;
    }

    @Transactional
    public Image getImage(Long id) {
        return imageRepository.findById(id).orElse(null);
    }

    public JsonUser parseFile(byte[] src) throws IOException {
        return objectMapper.readValue(src, JsonUser.class);
    }

    public List<Phone> extractPhones(JsonUser jsonUser) {
        List<Phone> phones = new ArrayList<>();
        for (Object jsonPhone : jsonUser.getPhones()) {
            Phone phone = new Phone();
            Map<String, String> mapPhone = (LinkedHashMap<String, String>) jsonPhone;
            Iterator<java.util.Map.Entry<String, String>> iter = mapPhone.entrySet().iterator();
            phone.setNumber(iter.next().getValue());
            phone.setDescription(iter.next().getValue());
            phones.add(phone);
        }
        return phones;
    }

    public void writeFile(Object user, String path) throws IOException {
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(new File(path), JsonEncoding.UTF16_BE);
        objectMapper.writeValue(jsonGenerator, user);
    }
}