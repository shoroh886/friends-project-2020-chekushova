package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.FriendshipRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Friendship;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FriendshipServiceTest {
    UserRepository userRepository;
    FriendshipRepository friendshipRepository;
    FriendshipService friendshipService;

    @Before
    public void init() {
        userRepository = Mockito.mock(UserRepository.class);
        friendshipRepository = Mockito.mock(FriendshipRepository.class);
        friendshipService = new FriendshipService(userRepository, friendshipRepository);
    }

    @Test
    public void showChat() {
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User first = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User second = new User.UserBuilder()
                .id(3L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User third = new User.UserBuilder()
                .id(4L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User fourth = new User.UserBuilder()
                .id(5L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        List<User> friends = Collections.singletonList(first);
        List<User> requests = Collections.singletonList(second);
        List<User> subscribers = Collections.singletonList(third);
        List<User> subscriptions = Collections.singletonList(fourth);
        Friendship friendshipFriend = new Friendship(self, first, 3);
        Friendship friendshipRequest = new Friendship(second, self, 1);
        Friendship friendshipSubscriber = new Friendship(third, self, 2);
        Friendship friendshipSubscriptions = new Friendship(self, fourth, 2);
        List<Friendship> friendships = Arrays.asList(friendshipFriend, friendshipRequest, friendshipSubscriber, friendshipSubscriptions);
        when(friendshipRepository.findAllByInitiator_IdOrAccepting_Id(1L, 1L)).thenReturn(friendships);
        Map<Integer, List<User>> friendshipsMap = new HashMap<>();
        friendshipsMap.put(1, requests);
        friendshipsMap.put(2, subscriptions);
        friendshipsMap.put(3, subscribers);
        friendshipsMap.put(4, friends);
        assertEquals(friendshipsMap, friendshipService.getAllFriendships(1L));
    }

    @Test
    public void getFriendship() {
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User first = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        Friendship friendship = new Friendship(self, first, 3);
        when(friendshipRepository.getFriendship(1L, 2L)).thenReturn(friendship);
        assertEquals(friendship, friendshipService.getFriendship(1, 2));
    }

    @Test
    public void addFriend() {
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User first = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        when(userRepository.getById(2L)).thenReturn(first);
        when(userRepository.getById(1L)).thenReturn(self);
        friendshipService.addFriend(1, 2);
        InOrder inOrder = Mockito.inOrder(userRepository);
        inOrder.verify(userRepository, times(2)).getById(Mockito.any());
    }

    @Test
    public void acceptFriendship() {
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User first = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        Friendship friendship = new Friendship(self, first, 3);
        when(friendshipRepository.getFriendship(1L, 2L)).thenReturn(friendship);
        friendshipService.acceptFriendship(1, 2);
        verify(friendshipRepository).getFriendship(Mockito.any(), Mockito.any());
    }

    @Test
    public void rejectFriendship() {
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User first = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        Friendship friendship = new Friendship(self, first, 3);
        when(friendshipRepository.getFriendship(1L, 2L)).thenReturn(friendship);
        when(userRepository.getById(1L)).thenReturn(self);
        friendshipService.rejectFriendship(1, 2);
        verify(userRepository, times(2)).getById(Mockito.any());
        verify(friendshipRepository).getFriendship(Mockito.any(), Mockito.any());
    }

    @Test
    public void removeFriendFirst() {
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User first = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        Friendship friendship = new Friendship(self, first, 3);
        when(friendshipRepository.getFriendship(1L, 2L)).thenReturn(friendship);
        when(userRepository.getById(1L)).thenReturn(self);
        friendshipService.removeFriend(1, 2);
        verify(userRepository).getById(Mockito.any());
        verify(friendshipRepository).getFriendship(Mockito.any(), Mockito.any());
    }

    @Test
    public void removeFriendSecond() {
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User first = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        Friendship friendship = new Friendship(self, first, 2);
        when(friendshipRepository.getFriendship(1L, 2L)).thenReturn(friendship);
        when(userRepository.getById(1L)).thenReturn(self);
        friendshipService.removeFriend(1, 2);
        verify(userRepository).getById(Mockito.any());
        verify(friendshipRepository).getFriendship(Mockito.any(), Mockito.any());
        verify(friendshipRepository).delete(Mockito.any());
    }
}
