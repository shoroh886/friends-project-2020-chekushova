package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.ChatRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.MessageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Chat;
import com.getjavajob.training.chekushova.socialnetwork.domain.Message;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.data.domain.PageRequest.of;

@RunWith(MockitoJUnitRunner.class)
public class ChatServiceTest {
    ChatRepository chatRepository;
    MessageRepository messageRepository;
    UserRepository userRepository;
    ChatService chatService;

    @Before
    public void init() {
        chatRepository = Mockito.mock(ChatRepository.class);
        userRepository = Mockito.mock(UserRepository.class);
        messageRepository = Mockito.mock(MessageRepository.class);
        chatService = new ChatService(chatRepository, messageRepository, userRepository);
    }

    @Test
    public void showChat() {
        Pageable pageable = of(0, 5);
        User sender = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        User destination = new User.UserBuilder()
                .id(2L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        List<Message> messages = Collections.singletonList(new Message(sender, destination, "test"));
        when(messageRepository.getMessages(1L, 2L, pageable)).thenReturn(messages);
        assertEquals(messages, chatService.showChat(1L, 2L, 1));
    }

    @Test
    public void getChatList() {
        List<Chat> chats = Collections.singletonList(new Chat(1L, "test", "test", 1, ("test").getBytes()));
        when(chatRepository.getChatList(1L)).thenReturn(chats);
        assertEquals(chats, chatService.getChatList(1L));
    }

    @Test
    public void checkMessages() {
        User user = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        when(messageRepository.getSumUnreadMessages(1L)).thenReturn(1);
        assertEquals(1, chatService.checkMessages(user));
    }
}
