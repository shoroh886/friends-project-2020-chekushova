package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.AccountRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.ImageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.WallRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import com.getjavajob.training.chekushova.socialnetwork.domain.Avatar;
import com.getjavajob.training.chekushova.socialnetwork.domain.Image;
import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.domain.Wall;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jms.core.JmsTemplate;

import java.time.LocalDate;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.data.domain.PageRequest.of;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    private AccountRepository accountRepository;
    private UserRepository userRepository;
    private WallRepository wallRepository;
    private ImageRepository imageRepository;
    private AccountService accountService;
    private JmsTemplate jmsTemplate;

    @Before
    public void init() {
        accountRepository = Mockito.mock(AccountRepository.class);
        userRepository = Mockito.mock(UserRepository.class);
        wallRepository = Mockito.mock(WallRepository.class);
        imageRepository = Mockito.mock(ImageRepository.class);
        jmsTemplate = Mockito.mock(JmsTemplate.class);
        accountService = new AccountService(accountRepository, userRepository, wallRepository, imageRepository, jmsTemplate);
    }

    @Test
    public void createAccount() {
        Account accountTest = new Account(1L, "test", "test", "test");
        List<Phone> phonesTest = singletonList(new Phone(1L, "11111", "test"));
        User userTest = new User.UserBuilder()
                .id(0)
                .name("firstName")
                .surname("secondName")
                .patronymic("lastName")
                .phones(phonesTest)
                .birth(LocalDate.now())
                .homeAddress("homeAddress")
                .workAddress("workAddress")
                .email("email")
                .icq("icq")
                .skype("skype")
                .additionally("additionally")
                .build();
        when(accountRepository.save(accountTest)).thenReturn(accountTest);
        when(userRepository.save(userTest)).thenReturn(userTest);
        accountService.createAccount(accountTest, userTest);
        assertEquals(1L, (long) userTest.getId());
    }

    @Test
    public void getWall() {
        List<Wall> testWall = singletonList(new Wall(1L, "test"));
        when(wallRepository.findAllByAccountOrderByIdDesc(1L, of(1, 7))).thenReturn(testWall);
        assertEquals(testWall, accountService.getWallById(1L, 1));
    }

    @Test
    public void addImageWall() {
        Image image = new Image(1L, ("test").getBytes());
        Wall wall = new Wall(1L, "test", 0L);
        when(imageRepository.save(image)).thenReturn(image);
        when(wallRepository.save(wall)).thenReturn(wall);
        accountService.addImageWall(wall, image);
        assertEquals(1L, (long) wall.getContent());
    }

    @Test
    public void changeUser() {
        Avatar avatar = new Avatar(1L, ("test").getBytes());
        List<Phone> phonesTest = singletonList(new Phone(1L, "11111", "test"));
        User userNew = new User.UserBuilder()
                .id(1)
                .name("firstName")
                .surname("secondName")
                .patronymic("lastName")
                .phones(phonesTest)
                .birth(LocalDate.now())
                .homeAddress("homeAddress")
                .workAddress("workAddress")
                .email("email")
                .icq("icq")
                .skype("skype")
                .additionally("additionally")
                .build();
        User userWithAvatar = new User.UserBuilder()
                .id(0)
                .name("firstName")
                .surname("secondName")
                .patronymic("lastName")
                .birth(LocalDate.now())
                .homeAddress("homeAddress")
                .workAddress("workAddress")
                .email("email")
                .icq("icq")
                .skype("skype")
                .additionally("additionally")
                .avatar(avatar)
                .build();
        when(userRepository.getByIdAvatar(1L)).thenReturn(userWithAvatar);
        accountService.changeUser(userNew);
        assertEquals(avatar, userNew.getAvatar());
    }

    @Test
    public void getAccount() {
        Account accountTest = new Account(1L, "test", "test", "test");
        when(accountRepository.getById(1L)).thenReturn(accountTest);
        assertEquals(accountTest, accountRepository.getById(1L));
    }

    @Test
    public void getAccountByLogin() {
        Account accountTest = new Account(1L, "test", "test", "test");
        when(accountRepository.getByLogin("test")).thenReturn(accountTest);
        assertEquals(accountTest, accountRepository.getByLogin("test"));
    }
}