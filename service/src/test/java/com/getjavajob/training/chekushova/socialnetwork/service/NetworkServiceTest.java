package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.ImageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import com.getjavajob.training.chekushova.socialnetwork.domain.Image;
import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.JsonUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NetworkServiceTest {
    private UserRepository userRepository;
    private ImageRepository imageRepository;
    private NetworkService networkService;

    @Before
    public void init() {
        this.userRepository = Mockito.mock(UserRepository.class);
        this.imageRepository = Mockito.mock(ImageRepository.class);
        networkService = new NetworkService(userRepository, imageRepository);
    }

    @Test
    public void createJsonUser() {
        Account accountTest = new Account(1L, "test", "test", "test");
        List<Phone> phonesTest = singletonList(new Phone(1L, "11111", "test"));
        LocalDate birth = LocalDate.parse("1998-12-12");
        User user = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .patronymic("lastName")
                .phones(phonesTest)
                .birth(birth)
                .homeAddress("homeAddress")
                .workAddress("workAddress")
                .email("email")
                .icq("icq")
                .skype("skype")
                .additionally("additionally")
                .build();
        JsonUser jsonUser = new JsonUser();
        jsonUser.setId(1L);
        jsonUser.setLogin("test");
        jsonUser.setPassword("test");
        jsonUser.setName("firstName");
        jsonUser.setSurname("secondName");
        jsonUser.setPatronymic("lastName");
        jsonUser.setPhones(phonesTest.toArray());
        jsonUser.setBirth(birth);
        jsonUser.setHomeAddress("homeAddress");
        jsonUser.setWorkAddress("workAddress");
        jsonUser.setEmail("email");
        jsonUser.setIcq("icq");
        jsonUser.setSkype("skype");
        jsonUser.setAdditionally("additionally");
        when(userRepository.getByIdFull(1L)).thenReturn(user);
        assertEquals(jsonUser, networkService.createJsonUser(accountTest));
    }

    @Test
    public void getImage() {
        Account accountTest = new Account(1L, "test", "test", "test");
        List<Phone> phonesTest = singletonList(new Phone(1L, "11111", "test"));
        Image image = new Image(1L, "test".getBytes());
        when(imageRepository.findById(1L)).thenReturn(java.util.Optional.of(image));
        assertEquals(image, imageRepository.findById(1L).orElse(null));
    }
}