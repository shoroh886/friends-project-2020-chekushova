package com.getjavajob.training.chekushova.socialnetwork.service;

import com.getjavajob.training.chekushova.socialnetwork.dao.repository.DiscussionMessageRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.DiscussionRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.GroupMemberRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.GroupRepository;
import com.getjavajob.training.chekushova.socialnetwork.dao.repository.UserRepository;
import com.getjavajob.training.chekushova.socialnetwork.domain.Discussion;
import com.getjavajob.training.chekushova.socialnetwork.domain.DiscussionMessage;
import com.getjavajob.training.chekushova.socialnetwork.domain.Group;
import com.getjavajob.training.chekushova.socialnetwork.domain.Member;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.data.domain.PageRequest.of;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    private UserRepository userRepository;
    private GroupRepository groupRepository;
    private GroupMemberRepository groupMemberRepository;
    private DiscussionRepository discussionRepository;
    private DiscussionMessageRepository discussionMessageRepository;
    private GroupService groupService;

    @Before
    public void init() {
        userRepository = mock(UserRepository.class);
        groupRepository = mock(GroupRepository.class);
        groupMemberRepository = mock(GroupMemberRepository.class);
        discussionRepository = mock(DiscussionRepository.class);
        discussionMessageRepository = mock(DiscussionMessageRepository.class);
        groupService = new GroupService(userRepository, groupRepository, groupMemberRepository, discussionRepository,
                discussionMessageRepository);
    }

    @Test
    public void createGroupTrue() {
        Group group = new Group("group test88", "test");
        assertTrue(groupService.createGroup(group, 1L));
    }

    @Test
    public void createGroupFalse() {
        Group group = new Group("не верное имя66", "test");
        assertFalse(groupService.createGroup(group, 1L));
    }

    @Test
    public void join() {
        groupService.join("test 88", 1L);
        verify(groupMemberRepository).insertMember("test_88", 1L, 1);
    }

    @Test
    public void acceptRequest() {
        groupService.acceptRequest("test 88", 1L);
        verify(groupMemberRepository).updateRightsByGroupNameAndMemberId("test_88", 1L, 2);
    }

    @Test
    public void approveRank() {
        groupService.approveRank("test 88", 1L);
        verify(groupMemberRepository).updateRightsByGroupNameAndMemberId("test_88", 1L, 3);
    }

    @Test
    public void giveGroup() {
        groupService.giveGroup("test 88", 2L, 1L);
        verify(groupMemberRepository).updateRightsByGroupNameAndMemberId("test_88", 2L, 4);
        verify(groupMemberRepository).updateRightsByGroupNameAndMemberId("test_88", 1L, 3);
    }

    @Test
    public void downgrade() {
        groupService.downgrade("test 88", 2L);
        verify(groupMemberRepository).updateRightsByGroupNameAndMemberId("test_88", 2L, 2);
    }

    @Test
    public void deleteUser() {
        groupService.deleteUser("test 88", 2L);
        verify(groupMemberRepository).deleteByGroupNameAndMemberId("test_88", 2L);
    }

    @Test
    public void deleteGroup() {
        groupService.deleteGroup("test 88");
        verify(groupRepository).deleteByName("test_88");
    }

    @Test
    public void checkRightsFirst() {
        Member member = new Member("test_88", 1L, "test", "test", 1, ("test").getBytes());
        when(groupMemberRepository.findByGroupNameAndMemberId("test_88", 1L)).thenReturn(member);
        assertEquals(1, groupService.checkRights("test 88", 1L));
    }

    @Test
    public void checkRightsSecond() {
        groupService.checkRights("test 88", 1L);
        when(groupMemberRepository.findByGroupNameAndMemberId("test_88", 1L)).thenReturn(null);
        assertEquals(0, groupService.checkRights("test 88", 1L));
    }

    @Test
    public void getMembers() {
        List<Member> members = Collections.singletonList(new Member("test_88", 1L, "test", "test", 1, ("test").getBytes()));
        when(groupMemberRepository.findAllByGroupName("test_88")).thenReturn(members);
        assertEquals(members, groupService.getMembers("test 88"));
    }

    @Test
    public void getUserGroups() {
        List<Group> groups = Collections.singletonList(new Group("test_88", "test"));
        when(groupRepository.findAllByMemberId(1L)).thenReturn(groups);
        assertEquals(groups, groupService.getUserGroups(1L));
    }

    @Test
    public void findGroup() {
        Group group = new Group("test_88", "test");
        when(groupRepository.findByName("test_88")).thenReturn(group);
        assertEquals(group, groupService.findGroup("test%2088"));
    }

    @Test
    public void getFilteredGroups() {
        List<Group> members = Collections.singletonList(new Group("test_88", "test"));
        Pageable pageable = of(0, 5);
        when(groupRepository.findByNameContainingIgnoreCase("test", pageable)).thenReturn(members);
        assertEquals(members, groupService.getFilteredGroups("test", 1));
    }

    @Test
    public void getFilteredCount() {
        when(groupRepository.countByNameContainingIgnoreCase("test")).thenReturn(1L);
        assertEquals(1L, (long) groupService.getFilteredCount("test"));
    }

    @Test
    public void createDiscussion() {
        Group group = new Group("test_88", "test");
        Discussion discussion = new Discussion("test", 1, false);
        User self = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        DiscussionMessage discussionMessage = new DiscussionMessage(discussion, self, "test");
        when(groupRepository.findByName("test_88")).thenReturn(group);
        when(discussionRepository.save(discussion)).thenReturn(discussion);
        groupService.createDiscussion("test_88", discussion, discussionMessage);
        verify(groupRepository).findByName("test_88");
        verify(discussionRepository).save(discussion);
        verify(discussionMessageRepository).save(discussionMessage);
    }

    @Test
    public void createMessage() {
        Discussion discussion = new Discussion("test", 1, false);
        discussion.setId(1L);
        User author = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        DiscussionMessage discussionMessage = new DiscussionMessage(discussion, author, "test");
        when(discussionRepository.findById(1L)).thenReturn(java.util.Optional.of(discussion));
        when(userRepository.getByIdAvatar(1L)).thenReturn(author);
        assertEquals(discussionMessage, groupService.createMessage(1L, 1L, "test"));
    }

    @Test
    public void getDiscussion() {
        Discussion discussion = new Discussion("test", 1, false);
        when(discussionRepository.findById(1L)).thenReturn(java.util.Optional.of(discussion));
        assertEquals(discussion, groupService.getDiscussion(1L));
    }

    @Test
    public void getDiscussionPage() {
        Discussion discussion = new Discussion("test", 1, false);
        Pageable pageable = of(0, 10);
        List<Discussion> discussions = Collections.singletonList(discussion);
        when(discussionRepository.findByGroupName("test_88", 1, pageable)).thenReturn(discussions);
        assertEquals(discussions, groupService.getDiscussionPage("test 88", 1, 1));
    }

    @Test
    public void getDiscussionMessagePage() {
        Discussion discussion = new Discussion("test", 1, false);
        Pageable pageable = of(0, 10);
        User author = new User.UserBuilder()
                .id(1L)
                .name("firstName")
                .surname("secondName")
                .email("email")
                .build();
        DiscussionMessage discussionMessage = new DiscussionMessage(discussion, author, "test");
        List<DiscussionMessage> discussionMessages = Collections.singletonList(discussionMessage);
        when(discussionRepository.getOne(1L)).thenReturn(discussion);
        when(discussionMessageRepository.findAllByDiscussionOrderByIdDesc(discussion, pageable)).thenReturn(discussionMessages);
        assertEquals(discussionMessages, groupService.getDiscussionMessagePage(1L, 1));
    }

    @Test
    public void changeDiscussionOptions() {
        groupService.changeDiscussionOptions(1L, "01");
        verify(discussionRepository).updateOptions(1L, 0, true);
    }

    @Test
    public void validateGroupTaken() {
        List<Group> groups = Collections.singletonList(new Group("test_8888", "test"));
        when(groupRepository.findAll()).thenReturn(groups);
        assertEquals("taken", groupService.validateGroup("test 8888"));
    }

    @Test
    public void validateGroupFree() {
        when(groupRepository.findAll()).thenReturn(new ArrayList<>());
        assertNull(groupService.validateGroup("test 8888"));
    }
}
