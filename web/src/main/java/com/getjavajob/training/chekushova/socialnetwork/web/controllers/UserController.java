package com.getjavajob.training.chekushova.socialnetwork.web.controllers;

import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import com.getjavajob.training.chekushova.socialnetwork.domain.Avatar;
import com.getjavajob.training.chekushova.socialnetwork.domain.Friendship;
import com.getjavajob.training.chekushova.socialnetwork.domain.Image;
import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.domain.Wall;
import com.getjavajob.training.chekushova.socialnetwork.service.AccountService;
import com.getjavajob.training.chekushova.socialnetwork.service.ChatService;
import com.getjavajob.training.chekushova.socialnetwork.service.EmailingService;
import com.getjavajob.training.chekushova.socialnetwork.service.FriendshipService;
import com.getjavajob.training.chekushova.socialnetwork.service.NetworkService;
import com.getjavajob.training.chekushova.socialnetwork.service.UserValidator;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.EmailMessage;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.RegisterUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.Long.valueOf;
import static java.lang.Math.max;

@Controller
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final int NONE = 0;
    private static final int SENT = 1;
    private static final int REQUEST = 2;
    private static final int SUBSCRIPTION = 3;
    private static final int SUBSCRIBER = 4;
    private static final int FRIEND = 5;


    private final AccountService accountService;
    private final ChatService chatService;
    private final FriendshipService friendshipService;
    private final NetworkService networkService;
    private final UserValidator validator;
    private final EmailingService emailingService;

    public UserController(AccountService accountService, ChatService chatService, FriendshipService friendshipService,
                          NetworkService networkService, UserValidator validator, EmailingService emailingService) {
        this.accountService = accountService;
        this.chatService = chatService;
        this.friendshipService = friendshipService;
        this.networkService = networkService;
        this.validator = validator;
        this.emailingService = emailingService;
    }

    @RequestMapping(value = "/")
    public ModelAndView defaultPage() {
        return loginPage();
    }

    @PostMapping(value = "wall/post")
    public String printWall(@RequestParam("file") MultipartFile multipartFile,
                            @RequestParam("text") String text, HttpServletRequest req) {
        try {
            Wall wall = new Wall();
            long id = (long) req.getSession().getAttribute("id");
            wall.setAccount(id);
            wall.setText(text);
            Image image = new Image();
            if (!multipartFile.isEmpty()) {
                byte[] bytes;
                bytes = multipartFile.getBytes();
                image.setContent(bytes);
            } else {
                image.setContent(null);
            }
            accountService.addImageWall(wall, image);
            return "redirect:/id" + id;
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
            return "redirect:/error";
        }
    }

    @GetMapping(value = "wall/scroll")
    @ResponseBody
    public List<Wall> scroll(@RequestParam("page") String page, @RequestParam("userId") String id) {
        return accountService.getWallById(valueOf(id), parseInt(page));
    }

    @GetMapping(value = "wall/content*")
    @ResponseBody
    public void getBlob(@RequestParam("id") String contentId, HttpServletResponse res) throws IOException {
        Image image = networkService.getImage(valueOf(contentId));
        byte[] img = image.getContent();
        OutputStream outputStream = res.getOutputStream();
        res.setContentType("image");
        try {
            outputStream.write(img);
            outputStream.close();
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
        }
    }

    @GetMapping(value = "/login")
    public ModelAndView loginPage() {
        return new ModelAndView("login");
    }

    @GetMapping(value = "/id*")
    public ModelAndView showUser(HttpServletRequest req) {
        String[] parts = req.getRequestURI().split("/");
        long userId = parseLong(parts[parts.length - 1].substring(2));
        long selfId = (long) req.getSession().getAttribute("id");
        List<Wall> wall = accountService.getWallById(userId, 0);
        ModelAndView userView = new ModelAndView("user");
        if (userId != selfId) {
            long status = getFriendshipStatus(selfId, userId);
            userView.addObject("status", status);
        }
        User user = accountService.getFullUser(userId);
        if (user == null) {
            ModelAndView errorView = new ModelAndView("error");
            errorView.addObject("error", "found");
            return errorView;
        }
        userView.addObject("user", user);
        userView.addObject("unread", max(chatService.checkMessages(user), 0));
        userView.addObject("wall", wall);
        return userView;
    }

    @GetMapping(value = "/registration")
    public String registration(ModelMap model) {
        model.addAttribute("registerUser", new RegisterUser());
        model.addAttribute("phone", new Phone());
        return "registration";
    }

    @PostMapping(value = "/register")
    public ModelAndView register(@Valid @ModelAttribute("registerUser") RegisterUser user, BindingResult result) {
        validator.validate(user, result);
        if (result.hasErrors()) {
            return new ModelAndView("registration");
        } else {
            return finishRegister(new Account(user.getLogin(), user.getPassword(), "user"), accountService.createUser(user));
        }
    }

    private ModelAndView finishRegister(Account newAccount, User newUser) {
        accountService.createAccount(newAccount, newUser);
        ModelAndView loginView = new ModelAndView("login");
        loginView.addObject("message", "Successful registration.");
        String text = "Dear $user$ we are glad to have a new friend. Welcome, " + newUser.getName() + ".";
//        emailingService.sendEmailing(new EmailMessage("user", "New friend!", text));
        return loginView;
    }

    private User createUser(HttpServletRequest req) {
        String firstName = req.getParameter("firstName");
        String secondName = req.getParameter("secondName");
        String lastName = req.getParameter("lastName");
        String phone1 = req.getParameter("phone1");
        String description1 = req.getParameter("description1");
        String phone2 = req.getParameter("phone2");
        String description2 = req.getParameter("description2");
        String phone3 = req.getParameter("phone3");
        String description3 = req.getParameter("description3");
        String phone4 = req.getParameter("phone4");
        String description4 = req.getParameter("description4");
        String phone5 = req.getParameter("phone5");
        String description5 = req.getParameter("description5");
        List<Phone> phones = new ArrayList<>();
        phones.add(new Phone(null, phone1, description1));
        if (phone2 != null) {
            phones.add(new Phone(null, phone2, description2));
        }
        if (phone3 != null) {
            phones.add(new Phone(null, phone3, description3));
        }
        if (phone4 != null) {
            phones.add(new Phone(null, phone4, description4));
        }
        if (phone5 != null) {
            phones.add(new Phone(null, phone5, description5));
        }
        String birth = req.getParameter("birth");
        String homeAddress = req.getParameter("homeAddress");
        String workAddress = req.getParameter("workAddress");
        String email = req.getParameter("email");
        String icq = req.getParameter("icq");
        String skype = req.getParameter("skype");
        String additionally = req.getParameter("additionally");
        return new User.UserBuilder()
                .name(firstName)
                .surname(secondName)
                .patronymic(lastName)
                .phones(phones)
                .birth(birth)
                .homeAddress(homeAddress)
                .workAddress(workAddress)
                .email(email)
                .icq(icq)
                .skype(skype)
                .additionally(additionally)
                .build();
    }

    @GetMapping(value = "/user/info/get")
    public ModelAndView showUserInfo(HttpServletRequest req) {
        ModelAndView modelAndView = new ModelAndView("info");
        long selfId = ((long) req.getSession().getAttribute("id"));
        User user = accountService.getFullUser(selfId);
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @GetMapping(value = "/user/delete")
    @ResponseBody
    public void deleteUser(@RequestParam(name = "id") String id) {
        accountService.deleteAccount(parseInt(id));
    }

    @PostMapping(value = "/user/avatar/set")
    public String setAvatar(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest req) {
        try {
            long id = (long) req.getSession().getAttribute("id");
            Avatar avatar;
            if (!multipartFile.isEmpty()) {
                avatar = new Avatar(id, multipartFile.getBytes());
            } else {
                return "redirect:/user/info/get";
            }
            User user = accountService.getFullUser(id);
            user.setAvatar(avatar);
            accountService.changeUser(user);
            req.getSession().setAttribute("user", user);
            return "redirect:/user/info/get";
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
            return "redirect:/error";
        }
    }

    @PostMapping(value = "/user/info/change")
    public String changeUser(HttpServletRequest req) {
        User newUser = createUser(req);
        newUser.setId(parseLong(req.getParameter("id")));
        accountService.changeUser(newUser);
        req.getSession().setAttribute("user", newUser);
        return "redirect:/user/info/get";
    }

    public int getFriendshipStatus(long selfId, long userId) {
        Friendship friendship = friendshipService.getFriendship(selfId, userId);
        if (friendship == null) {
            return NONE;
        } else if (friendship.getInitiator().getId() == selfId && friendship.getStatus() == 1) {
            return SENT;
        } else if (friendship.getAccepting().getId() == selfId && friendship.getStatus() == 1) {
            return REQUEST;
        } else if (friendship.getInitiator().getId() == selfId && friendship.getStatus() == 2) {
            return SUBSCRIPTION;
        } else if (friendship.getAccepting().getId() == selfId && friendship.getStatus() == 2) {
            return SUBSCRIBER;
        } else if (friendship.getStatus() == 3) {
            return FRIEND;
        }
        return 0;
    }
}