package com.getjavajob.training.chekushova.socialnetwork.web.controllers;

import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.service.FriendshipService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

import static java.lang.Long.parseLong;

@Controller
public class FriendshipController {
    private static final String FRIENDS_REDIRECT_URL = "redirect:/friends/?userPageId=";
    private static final String SELF_ID = "selfId";
    private static final String FRIEND_ID = "friendId";

    private final FriendshipService friendshipService;

    public FriendshipController(FriendshipService friendshipService) {
        this.friendshipService = friendshipService;
    }

    @GetMapping(value = "friends")
    public ModelAndView showFriends(HttpServletRequest req) {
        long id = parseLong(req.getParameter("userPageId"));
        Map<Integer, List<User>> contacts = friendshipService.getAllFriendships(id);
        List<User> requests = contacts.get(1);
        List<User> subscriptions = contacts.get(2);
        List<User> subscribers = contacts.get(3);
        List<User> friends = contacts.get(4);
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("userPageId", id);
        modelAndView.addObject("requests", requests);
        modelAndView.addObject("subscriptions", subscriptions);
        modelAndView.addObject("subscribers", subscribers);
        modelAndView.addObject("friends", friends);
        return modelAndView;
    }

    @PostMapping(value = "/friend/add")
    public String addFriend(HttpServletRequest req) {
        long selfId = parseLong(req.getParameter(SELF_ID));
        long friendId = parseLong(req.getParameter(FRIEND_ID));
        friendshipService.addFriend(selfId, friendId);
        return FRIENDS_REDIRECT_URL + selfId;
    }

    @PostMapping(value = "/friend/accept")
    public String acceptFriendship(HttpServletRequest req) {
        long selfId = parseLong(req.getParameter(SELF_ID));
        long friendId = parseLong(req.getParameter(FRIEND_ID));
        friendshipService.acceptFriendship(selfId, friendId);
        return FRIENDS_REDIRECT_URL + selfId;
    }

    @PostMapping(value = "/friend/reject")
    public String rejectFriendship(HttpServletRequest req) {
        long selfId = parseLong(req.getParameter(SELF_ID));
        long friendId = parseLong(req.getParameter(FRIEND_ID));
        friendshipService.rejectFriendship(selfId, friendId);
        return FRIENDS_REDIRECT_URL + selfId;
    }

    @PostMapping(value = "/friend/remove")
    public String removeFriend(HttpServletRequest req) {
        long selfId = parseLong(req.getParameter(SELF_ID));
        long friendId = parseLong(req.getParameter(FRIEND_ID));
        friendshipService.removeFriend(selfId, friendId);
        return FRIENDS_REDIRECT_URL + selfId;
    }
}