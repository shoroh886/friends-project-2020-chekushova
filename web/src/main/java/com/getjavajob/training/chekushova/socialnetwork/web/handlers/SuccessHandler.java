package com.getjavajob.training.chekushova.socialnetwork.web.handlers;

import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.service.AccountService;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SuccessHandler implements AuthenticationSuccessHandler {
    private final AccountService accountService;

    public SuccessHandler(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication authentication) throws IOException {
        org.springframework.security.core.userdetails.User securityAccount = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        Account account = accountService.getAccountByLogin(securityAccount.getUsername());
        User user = accountService.getFullUser(account.getId());
        req.getSession().setAttribute("login", securityAccount.getUsername());
        req.getSession().setAttribute("password", securityAccount.getPassword());
        req.getSession().setAttribute("id", user.getId());
        req.getSession().setAttribute("user", user);
        req.getSession().setAttribute("role", account.getRole());
        res.sendRedirect(req.getContextPath() + "/id" + account.getId());
    }
}