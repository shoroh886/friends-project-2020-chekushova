package com.getjavajob.training.chekushova.socialnetwork.web.controllers;

import com.getjavajob.training.chekushova.socialnetwork.domain.Account;
import com.getjavajob.training.chekushova.socialnetwork.domain.Group;
import com.getjavajob.training.chekushova.socialnetwork.domain.Phone;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.service.AccountService;
import com.getjavajob.training.chekushova.socialnetwork.service.GroupService;
import com.getjavajob.training.chekushova.socialnetwork.service.NetworkService;
import com.getjavajob.training.chekushova.socialnetwork.service.UserValidator;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.JsonUser;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.RegisterUser;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.SearchResult;
import com.getjavajob.training.chekushova.socialnetwork.service.others.LocaleResourceCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.getjavajob.training.chekushova.socialnetwork.service.dto.SearchResult.parseGroup;
import static com.getjavajob.training.chekushova.socialnetwork.service.dto.SearchResult.parseUser;
import static java.lang.Integer.parseInt;
import static java.lang.Math.ceil;

@Controller
public class NetworkController {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkController.class);
    private static final int SEARCH_PAGE_SIZE = 5;

    private final AccountService accountService;
    private final GroupService groupService;
    private final NetworkService networkService;
    private final LocaleResourceCache resourceCache;
    private final UserValidator validator;

    public NetworkController(AccountService accountService, GroupService groupService, NetworkService networkService,
                             LocaleResourceCache resourceCache, UserValidator validator) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.networkService = networkService;
        this.resourceCache = resourceCache;
        this.validator = validator;
    }

    @GetMapping(value = "search/auto")
    @ResponseBody
    public List<SearchResult> getAutoResult(@RequestParam("filter") String filter, HttpServletRequest req) {
        List<User> users = accountService.getAnotherFilteredUsers(filter, 1, (long) req.getSession().getAttribute("id"));
        List<Group> groups = groupService.getFilteredGroups(filter, 1);
        List<SearchResult> results = usersToResult(users);
        results.addAll(groupsToResult(groups));
        return results;
    }

    @GetMapping(value = "/search")
    @ResponseBody
    public ModelAndView getSearchResultUsers(@RequestParam("filter") String filter, HttpServletRequest req) {
        long selfId = (long) req.getSession().getAttribute("id");
        List<User> users = accountService.getAnotherFilteredUsersAvatar(filter.toLowerCase(), 1, selfId);
        ModelAndView modelAndView = new ModelAndView("search");
        int noOfPages = (int) ceil((double) accountService.getFilteredCount(filter.toLowerCase(), selfId) / SEARCH_PAGE_SIZE);
        modelAndView.addObject("results", users);
        modelAndView.addObject("noOfPages", noOfPages);
        modelAndView.addObject("filter", filter);
        return modelAndView;
    }

    @GetMapping(value = "/users/search/pages")
    @ResponseBody
    public int getUserPages(@RequestParam("filter") String filter, HttpServletRequest req) {
        long selfId = (long) req.getSession().getAttribute("id");
        return (int) ceil((double) accountService.getFilteredCount(filter.toLowerCase(), selfId) / SEARCH_PAGE_SIZE);
    }

    @GetMapping(value = "/groups/search/pages")
    @ResponseBody
    public int getGroupPages(@RequestParam("filter") String filter) {
        return (int) ceil((double) groupService.getFilteredCount(filter.toLowerCase()) / SEARCH_PAGE_SIZE);
    }

    @GetMapping(value = "/users/search/page")
    @ResponseBody
    public List<User> getAjaxResultUsers(@RequestParam("filter") String filter, @RequestParam("page") String s, HttpServletRequest req) {
        int page = parseInt(s);
        long selfId = (long) req.getSession().getAttribute("id");
        List<User> users = accountService.getAnotherFilteredUsers(filter, page, selfId);
        for (User user : users) {
            user.setPhones(null);
        }
        return users;
    }

    @GetMapping(value = "/groups/search/page")
    @ResponseBody
    public List<Group> getAjaxResultGroups(@RequestParam("filter") String filter, @RequestParam("page") String s) {
        int page = parseInt(s);
        return groupService.fixGroupsName(groupService.getFilteredGroups(filter, page));
    }

    @PostMapping(value = "/user/json/register")
    public void jsonRegister(@RequestParam("file") MultipartFile multipartFile, HttpServletResponse res) throws IOException {
        try {
            byte[] bytes = multipartFile.getBytes();
            JsonUser jsonUser = networkService.parseFile(bytes);
            RegisterUser newUser = createRegisterUser(jsonUser);
            BeanPropertyBindingResult errors = new BeanPropertyBindingResult(newUser, "registerUser");
            validator.validateUser(newUser, errors);
            if (errors.hasErrors()) {
                writeJsonErrors(errors, res);
            } else {
                Account account = new Account(newUser.getLogin(), newUser.getPassword(), "user");
                User user = accountService.createUser(newUser);
                accountService.createAccount(account, user);
                res.sendRedirect("/login");
            }
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
            res.sendRedirect("/error");
        }
    }

    private RegisterUser createRegisterUser(JsonUser jsonUser) {
        return new RegisterUser.UserBuilder()
                .login(jsonUser.getLogin())
                .password(jsonUser.getPassword())
                .repeatPassword(jsonUser.getPassword())
                .name(jsonUser.getName())
                .surname(jsonUser.getSurname())
                .patronymic(jsonUser.getPatronymic())
                .phones(networkService.extractPhones(jsonUser))
                .birth(jsonUser.getBirth())
                .homeAddress(jsonUser.getHomeAddress())
                .workAddress(jsonUser.getWorkAddress())
                .email(jsonUser.getEmail())
                .icq(jsonUser.getIcq())
                .skype(jsonUser.getSkype())
                .additionally(jsonUser.getAdditionally()).build();
    }

    @GetMapping(value = "/user/json/get")
    public void writeJson(HttpServletRequest req, HttpServletResponse res) throws IOException {
        long selfId = (long) req.getSession().getAttribute("id");
        String fileName = "yourUser.json";
        JsonUser jsonUser = networkService.createJsonUser(accountService.getAccount(selfId));
        for (Object phone : jsonUser.getPhones()) {
            ((Phone) phone).setUser(null);
        }
        networkService.writeFile(jsonUser, "yourUser.json");
        sendFile(res, fileName);
    }

    private void writeJsonErrors(Errors errors, HttpServletResponse res) throws IOException {
        String fileName = "errorsJson.json";
        List<String> errorsReport = new ArrayList<>();
        for (FieldError error : errors.getFieldErrors()) {
            errorsReport.add(error.getField() + " : " + error.getDefaultMessage());
        }
        networkService.writeFile(errorsReport, "errorsJson.json");
        sendFile(res, fileName);
    }

    @GetMapping(value = "/logs/get")
    public void getLogs(HttpServletResponse res, @RequestParam(name = "log") String type) throws IOException {
        String fileName;
        switch (type) {
            case "error":
                fileName = "errors.log";
                break;
            case "user":
                fileName = "users.log";
                break;
            case "warn":
                fileName = "warn.log";
                break;
            default:
                fileName = "sn886.log";
                break;
        }
        sendFile(res, fileName);
    }

    private void sendFile(HttpServletResponse res, String fileName) throws IOException {
        res.setContentType("APPLICATION/OCTET-STREAM");
        res.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        try (java.io.FileInputStream fileInputStream = new java.io.FileInputStream(fileName)) {
            int i;
            while ((i = fileInputStream.read()) != -1) {
                res.getWriter().write(i);
            }
        }
    }

    private List<SearchResult> usersToResult(List<User> users) {
        List<SearchResult> results = new ArrayList<>();
        for (User user : users) {
            results.add(parseUser(user));
        }
        return results;
    }

    private List<SearchResult> groupsToResult(List<Group> groups) {
        List<SearchResult> results = new ArrayList<>();
        for (Group group : groups) {
            group.setName(group.getName().replace("_", " "));
            results.add(parseGroup(group));
        }
        return results;
    }

    @GetMapping(value = "data/check")
    @ResponseBody
    public boolean checkUnique(@RequestParam("value") String field, @RequestParam("sql") String sql,
                               @RequestParam("type") String type, HttpServletRequest req) {
        if (type.equals("user")) {
            return !validator.checkUniq(field, sql, ((User) req.getSession().getAttribute("user")).getId());
        } else if (type.equals("group")) {
            return groupService.validateGroup(field) == null;
        }
        return false;
    }

    @GetMapping(value = "/locale/get")
    @ResponseBody
    public Map<String, String> getLocaleResource(@RequestParam("lang") String lang, @RequestParam("key") String key) {
        String s = lang + '.' + key;
        return resourceCache.getResources(s);
    }
}