package com.getjavajob.training.chekushova.socialnetwork.web.interceptors;

import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

public class AnonymousFilter extends GenericFilterBean {
    private final Set<String> anonymousUrls = new HashSet<>(asList("/login", "/register", "/registration", "/"));

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        if (isAnonymousUrl(req) && isAuthenticated()) {
            (res).sendRedirect("/id" + req.getSession().getAttribute("id"));
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private boolean isAuthenticated() {
        return getContext().getAuthentication() != null && getContext().getAuthentication().isAuthenticated();
    }

    private boolean isAnonymousUrl(HttpServletRequest req) {
        return anonymousUrls.contains(req.getRequestURI());
    }
}
