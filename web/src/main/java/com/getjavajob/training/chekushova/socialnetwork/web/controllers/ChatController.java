package com.getjavajob.training.chekushova.socialnetwork.web.controllers;

import com.getjavajob.training.chekushova.socialnetwork.domain.Chat;
import com.getjavajob.training.chekushova.socialnetwork.domain.Message;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.service.AccountService;
import com.getjavajob.training.chekushova.socialnetwork.service.ChatService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

@Controller
public class ChatController {
    private final AccountService accountService;
    private final ChatService chatService;

    public ChatController(AccountService accountService, ChatService chatService) {
        this.accountService = accountService;
        this.chatService = chatService;
    }

    @GetMapping(value = "/chats")
    public ModelAndView showChats(HttpServletRequest req) {
        long selfId = (long) req.getSession().getAttribute("id");
        List<Chat> chats = chatService.getChatList(selfId);
        ModelAndView modelAndView = new ModelAndView("chats");
        modelAndView.addObject("chats", chats);
        return modelAndView;
    }

    @GetMapping(value = "/chat")
    public ModelAndView showChat(HttpServletRequest req) {
        long selfId = (Long) req.getSession().getAttribute("id");
        long userId = parseLong(req.getParameter("companionId"));
        User user = accountService.getUserAvatar(userId);
        user.setPhones(null);
        List<Message> messages = chatService.showChat(selfId, userId, 1);
        ModelAndView modelAndView = new ModelAndView("chat");
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("companion", user);
        return modelAndView;
    }

    @GetMapping(value = "/chat/page/get")
    @ResponseBody
    public List<Message> showChat(HttpServletRequest req, @RequestParam String companionId, @RequestParam String page) {
        long selfId = (long) req.getSession().getAttribute("id");
        List<Message> messages = chatService.showChat(selfId, parseInt(companionId), parseInt(page));
        for (Message message : messages) {
            message.getSender().setPhones(null);
            message.getSender().setAvatar(null);
            message.getDestination().setPhones(null);
            message.getDestination().setAvatar(null);
        }
        return messages;
    }

    @MessageMapping("/send/{key}")
    @SendTo("/topic/{key}")
    public Message send(Map<String, String> xmlMessage) {
        Message message = chatService.sendMessage(parseLong(xmlMessage.get("sender")),
                parseLong(xmlMessage.get("destination")), xmlMessage.get("message"));
        message.getSender().setPhones(null);
        message.getSender().setAvatar(null);
        message.getDestination().setPhones(null);
        message.getDestination().setAvatar(null);
        return message;
    }

    @PostMapping(value = "/chat/read")
    @ResponseBody
    public void readAll(HttpServletRequest req, @RequestParam(value = "companion") String companionId) {
        chatService.readAll((User) req.getSession().getAttribute("user"), parseInt(companionId));
    }
}