package com.getjavajob.training.chekushova.socialnetwork.web.config;

import com.getjavajob.training.chekushova.socialnetwork.web.handlers.DeniedHandler;
import com.getjavajob.training.chekushova.socialnetwork.web.handlers.FailureHandler;
import com.getjavajob.training.chekushova.socialnetwork.web.handlers.SuccessHandler;
import com.getjavajob.training.chekushova.socialnetwork.web.interceptors.AnonymousFilter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.ui.DefaultLoginPageGeneratingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final SuccessHandler accessHandler;
    private final FailureHandler failureHandler;
    private final DeniedHandler deniedHandler;
    private final UserDetailsService detailsService;

    public SecurityConfig(SuccessHandler accessHandler, FailureHandler failureHandler, DeniedHandler deniedHandler, @Qualifier("userDetailsService") UserDetailsService detailsService) {
        this.accessHandler = accessHandler;
        this.failureHandler = failureHandler;
        this.deniedHandler = deniedHandler;
        this.detailsService = detailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                csrf().disable().
                formLogin().loginPage("/login").loginProcessingUrl("/logging").usernameParameter("login").
                successHandler(accessHandler).failureHandler(failureHandler).permitAll().
                and().
                authorizeRequests().antMatchers("/user/delete", "group/delete").hasRole("admin").
                antMatchers("/resources/**", "/locale/get", "user/json/get", "/user/json/template/get", "/error", "/hello").permitAll().
                antMatchers("/register", "/registration", "/", "/login", "/user/json/register").anonymous().
                antMatchers("/common/error").permitAll().anyRequest().authenticated().
                and().
                logout().invalidateHttpSession(true).clearAuthentication(true).deleteCookies("JSESSIONID").
                logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login").
                and().
                rememberMe().rememberMeParameter("remember-me").key("WubbaLubbaDubDub").
                userDetailsService(detailsService).authenticationSuccessHandler(accessHandler).
                and().
                exceptionHandling().accessDeniedHandler(deniedHandler).
                and().
                addFilterBefore(new AnonymousFilter(), DefaultLoginPageGeneratingFilter.class);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    protected DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(detailsService);
        return daoAuthenticationProvider;
    }

    @Bean
    protected PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }
}