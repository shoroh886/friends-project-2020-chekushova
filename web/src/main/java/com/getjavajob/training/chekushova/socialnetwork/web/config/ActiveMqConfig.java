package com.getjavajob.training.chekushova.socialnetwork.web.config;

import com.getjavajob.training.chekushova.socialnetwork.service.dto.EmailMessage;
import com.getjavajob.training.chekushova.socialnetwork.service.dto.UserMessage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;

import java.util.Collections;

@Configuration
public class ActiveMqConfig {

    @Bean
    public MappingJackson2MessageConverter messageConverter() {
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
        messageConverter.setTypeIdPropertyName("content-type");
        messageConverter.setTypeIdMappings(Collections.singletonMap("emailMessage", EmailMessage.class));
        messageConverter.setTypeIdMappings(Collections.singletonMap("userMessage", UserMessage.class));
        return messageConverter;
    }
}
