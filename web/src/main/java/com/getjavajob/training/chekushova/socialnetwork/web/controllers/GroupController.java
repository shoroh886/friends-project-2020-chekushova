package com.getjavajob.training.chekushova.socialnetwork.web.controllers;

import com.getjavajob.training.chekushova.socialnetwork.domain.Discussion;
import com.getjavajob.training.chekushova.socialnetwork.domain.DiscussionMessage;
import com.getjavajob.training.chekushova.socialnetwork.domain.Group;
import com.getjavajob.training.chekushova.socialnetwork.domain.Member;
import com.getjavajob.training.chekushova.socialnetwork.domain.User;
import com.getjavajob.training.chekushova.socialnetwork.service.GroupService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.Long.valueOf;

@Controller
public class GroupController {
    private static final int DISCUS_PAGE_SIZE = 10;
    private static final String REDIRECT_GROUP = "redirect:/group";
    private static final String MEMBERS_GROUP = "redirect:/members";
    private static final String GROUP_NAME = "groupName";
    private static final String RIGHTS = "rights";
    private static final String MEMBER_ID = "memberId";
    private static final String DISCUSSION = "discussion";
    private static final String AUTHOR = "author";

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping(value = "/error")
    public ModelAndView showErrorPage(String errorType) {
        ModelAndView errorView = new ModelAndView("error");
        errorView.addObject("error", errorType);
        return errorView;
    }

    @GetMapping(value = "/groups")
    public ModelAndView showGroups(HttpServletRequest req) {
        ModelAndView groupsView = new ModelAndView("groups");
        List<Group> groups = groupService.getUserGroups(valueOf(req.getParameter("userPageId")));
        groupsView.addObject("groups", groupService.fixGroupsName(groups));
        return groupsView;
    }

    @GetMapping(value = "/group*")
    public ModelAndView showGroup(HttpServletRequest req) {
        String groupName;
        if (req.getParameter(GROUP_NAME) == null) {
            String[] urlParts = req.getRequestURI().split("/");
            groupName = urlParts[urlParts.length - 1].substring(5);
        } else {
            groupName = req.getParameter(GROUP_NAME);
        }
        Group group = groupService.findGroup(groupName);
        group.setName(groupService.replaceNameClient(group.getName()));
        ModelAndView groupView = new ModelAndView("group");
        int rights = groupService.checkRights(groupName, (long) req.getSession().getAttribute("id"));
        int access;
        if (rights > 1) {
            access = rights > 2 ? 3 : 2;
        } else {
            access = 1;
        }
        groupView.addObject("group", group);
        groupView.addObject(RIGHTS, rights);
        groupView.addObject("discuses", groupService.getDiscussionPage(groupName, access, 1));
        return groupView;
    }

    @GetMapping(value = "/members*")
    public ModelAndView showMembers(HttpServletRequest req) {
        String groupName;
        if (req.getParameter(GROUP_NAME) == null) {
            String[] parts = req.getRequestURI().split("/");
            groupName = parts[parts.length - 1].substring(7);
        } else {
            groupName = req.getParameter(GROUP_NAME);
        }
        List<Member> allMembers = groupService.getMembers(groupName);
        List<Member> requests = new ArrayList<>();
        List<Member> members = new ArrayList<>();
        List<Member> admins = new ArrayList<>();
        Member creator = null;
        for (Member member : allMembers) {
            if (member.getRights() == 1) {
                requests.add(member);
            } else if (member.getRights() == 2) {
                members.add(member);
            } else if (member.getRights() == 3) {
                admins.add(member);
            } else if (member.getRights() == 4) {
                creator = member;
            }
        }
        ModelAndView modelAndView = new ModelAndView("members");
        modelAndView.addObject(GROUP_NAME, groupName.replace("_", " "));
        modelAndView.addObject("requests", requests);
        modelAndView.addObject("members", members);
        modelAndView.addObject("admins", admins);
        modelAndView.addObject("creator", creator);
        modelAndView.addObject(RIGHTS, groupService.checkRights(groupName, (long) req.getSession().getAttribute("id")));
        return modelAndView;
    }

    @PostMapping(value = "/group/leave")
    public String leaveGroup(HttpServletRequest req) {
        String groupName = req.getParameter(GROUP_NAME);
        long memberId = parseLong(req.getParameter("member"));
        groupService.deleteUser(groupName, memberId);
        return REDIRECT_GROUP + groupName;
    }

    @PostMapping(value = "/group/join")
    public String joinGroup(HttpServletRequest req) {
        String groupName = req.getParameter(GROUP_NAME);
        long memberId = parseLong(req.getParameter("member"));
        groupService.join(groupName, memberId);
        return REDIRECT_GROUP + groupName;
    }


    @PostMapping(value = "/group/give")
    public String giveGroup(HttpServletRequest req) {
        long selfId = (long) req.getSession().getAttribute("id");
        String groupName = req.getParameter(GROUP_NAME);
        long memberId = parseLong(req.getParameter(MEMBER_ID));
        groupService.giveGroup(groupName, memberId, selfId);
        return MEMBERS_GROUP + groupName;
    }

    @PostMapping(value = "/member/refuse")
    public String excludeMember(HttpServletRequest req) {
        String groupName = req.getParameter(GROUP_NAME);
        long memberId = parseLong(req.getParameter(MEMBER_ID));
        groupService.deleteUser(groupName, memberId);
        return MEMBERS_GROUP + groupName;
    }

    @PostMapping(value = "/member/reduce")
    public String reduceMember(HttpServletRequest req) {
        String groupName = req.getParameter(GROUP_NAME);
        long memberId = parseLong(req.getParameter(MEMBER_ID));
        groupService.downgrade(groupName, memberId);
        return MEMBERS_GROUP + groupName;
    }

    @PostMapping(value = "/member/raise")
    public String raiseMember(HttpServletRequest req) {
        String groupName = req.getParameter(GROUP_NAME);
        long memberId = parseLong(req.getParameter(MEMBER_ID));
        groupService.approveRank(groupName, memberId);
        return MEMBERS_GROUP + groupName;
    }

    @PostMapping(value = "/member/include")
    public String includeMember(HttpServletRequest req) {
        String groupName = req.getParameter(GROUP_NAME);
        long memberId = parseLong(req.getParameter(MEMBER_ID));
        groupService.acceptRequest(groupName, memberId);
        return MEMBERS_GROUP + groupName;
    }

    @PostMapping(value = "/group/create")
    public String createGroup(HttpServletRequest req) {
        String name = req.getParameter(GROUP_NAME);
        String description = req.getParameter("description");
        long creator = parseLong(req.getParameter("creator"));
        Group group = new Group(name, description);
        if (groupService.createGroup(group, creator)) {
            return REDIRECT_GROUP + name;
        } else {
            return "redirect:/error";
        }
    }

    @GetMapping(value = "/group/delete")
    @ResponseBody
    public void deleteUser(@RequestParam(name = "name") String name) {
        groupService.deleteGroup(name);
    }

    @PostMapping(value = "/discussion/start")
    public String includeMember(HttpServletRequest req, @RequestParam(name = "name") String name,
                                @RequestParam(name = "access") String access,
                                @RequestParam(name = "status") String status,
                                @RequestParam(name = GROUP_NAME) String groupName,
                                @RequestParam(name = "message") String firstMessage) {
        Discussion discussion = new Discussion(name, parseInt(access), Boolean.parseBoolean(status));
        DiscussionMessage message = new DiscussionMessage(null, (User) req.getSession().getAttribute("user"), firstMessage);
        groupService.createDiscussion(groupService.replaceNameBase(groupName), discussion, message);
        return REDIRECT_GROUP + groupName;
    }

    @GetMapping(value = "/discussion")
    public ModelAndView getDiscussion(HttpServletRequest req, @RequestParam(name = "id") String id,
                                      @RequestParam(name = GROUP_NAME) String groupName) {
        Long discusId = parseLong(id);
        List<DiscussionMessage> messages = groupService.getDiscussionMessagePage(discusId, 1);
        Discussion discussion = groupService.getDiscussion(discusId);
        ModelAndView discusView = new ModelAndView("discus");
        int rights = groupService.checkRights(groupName, (long) req.getSession().getAttribute("id"));
        if ((discussion.getAccess() == 2 && rights < 2) || (discussion.getAccess() == 3 && rights < 3)) {
            return showErrorPage(RIGHTS);
        }
        discusView.addObject(GROUP_NAME, groupName);
        discusView.addObject("messages", messages);
        discusView.addObject(DISCUSSION, discussion);
        discusView.addObject(RIGHTS, rights);
        discusView.addObject("pages", (int) Math.ceil((double) discussion.getCount() / DISCUS_PAGE_SIZE));
        return discusView;
    }

    @GetMapping(value = "/discussion/page/get")
    @ResponseBody
    public List<DiscussionMessage> getDiscusPage(@RequestParam(name = "id") String id,
                                                 @RequestParam(name = "page") String page) {
        List<DiscussionMessage> messages = groupService.getDiscussionMessagePage(parseLong(id), parseInt(page));
        for (DiscussionMessage message : messages) {
            message.getAuthor().setPhones(null);
            message.setDiscussion(null);
        }
        return messages;
    }

    @MessageMapping("/write/{key}")
    @SendTo("/topic/{key}")
    public DiscussionMessage writeMessage(Map<String, String> xmlDiscussionMessage) {
        if (xmlDiscussionMessage.get(AUTHOR).equals("-1")) {
            User optionUser = new User();
            optionUser.setId(parseLong(xmlDiscussionMessage.get(AUTHOR)));
            DiscussionMessage optionMessage =
                    new DiscussionMessage(null, optionUser, xmlDiscussionMessage.get("text"));
            groupService.changeDiscussionOptions(parseLong(xmlDiscussionMessage.get(DISCUSSION)),
                    xmlDiscussionMessage.get("text"));
            return optionMessage;
        }
        DiscussionMessage newMessage = groupService.createMessage(parseLong(xmlDiscussionMessage.get(DISCUSSION)),
                parseLong(xmlDiscussionMessage.get(AUTHOR)), xmlDiscussionMessage.get("text"));
        newMessage.getAuthor().setPhones(null);
        newMessage.getDiscussion().setGroup(null);
        return newMessage;
    }
}