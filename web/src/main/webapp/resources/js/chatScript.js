var stompClient = null;
const BLOCK_SIZE = 750
const STRING_SIZE = 150
let massageCount = 5
let resources

function checkScroll() {
    let block = document.getElementById('chatBox');
    if (block.scrollTop === 0) {
        getPage()
    }
}

function getSystemLocale() {
    return document.getElementById("lang").value;
}

$(function () {
    let locale = getSystemLocale()
    $.ajax({
        url: "/locale/get",
        data: {
            lang: locale,
            key: "chat",
        },
        success: function (data) {
            resources = data
        }
    });
});

function connect() {
    let chat = document.getElementById("chatTable")
    scroll(chat.scrollHeight)
    let selfId = Number(document.getElementById("spectatorId").value)
    let companionId = Number(document.getElementById("companionId").value)
    let socket = new SockJS('/chat');
    let key = selfId < companionId ? selfId + "&" + companionId : companionId + "&" + selfId
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/' + key, function (event) {
            let message = JSON.parse(event.body);
            console.log(message)
            showMessageOutput(message.text, message.sender.id);
        });
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function send() {
    if (document.getElementById('message').value === "") {
        document.getElementById('message').placeholder =
            resources[document.getElementById("lang").value + '.chat.error.empty']
    } else {
        document.getElementById('message').placeholder = 'message';
        let from = document.getElementById('spectatorId').value;
        let to = document.getElementById('companionId').value;
        let key = from < to ? from + "&" + to : to + "&" + from
        let text = document.getElementById('message').value;
        document.getElementById('message').value = '';
        stompClient.send("/app/send/" + key, {},
            JSON.stringify({'sender': from, 'destination': to, 'message': text}));
    }
}

function showMessageOutput(message, from) {
    let chatTable = document.getElementById("chatTable")
    let string = document.createElement('tr')
    let messageView
    let selfId = Number(document.getElementById("spectatorId").value)
    if (selfId === Number(from)) {
        messageView = '<td></td><td class="self">' + message + '</td>'
    } else {
        messageView = '<td class="alias">' + message + '</td><td></td>'
    }
    string.innerHTML = messageView
    let scrollBorder = massageCount * STRING_SIZE
    chatTable.insertBefore(string, chatTable.lastChild.nextSibling)
    massageCount++
    let chat = document.getElementById('chatBox');
    let position = chat.scrollTop + BLOCK_SIZE - STRING_SIZE
    if (scrollBorder < position) {
        scroll(chat.scrollHeight)
    }
}

let getPage = function () {
    let to = document.getElementById('companionId').value;
    let page = document.getElementById("page").value
    $.ajax({
        url: "/chat/page/get",
        data: {
            companionId: to,
            page: ++page
        },
        success: function (data) {
            buildPrevious(data)
        }
    });
}

function buildPrevious(messages) {
    console.log(messages)
    let chatTable = document.getElementById("chatTable")
    let actualPage = document.getElementById("page")
    let page = Number(actualPage.value) + 1
    let selfId = Number(document.getElementById("spectatorId").value)
    for (message in messages) {
        let string = document.createElement('tr')
        let messageView
        let from = Number(messages[message].sender.id)
        if (from === selfId) {
            messageView = '<td></td><td class="self">' + messages[message].text + '</td>'
        } else {
            messageView = '<td class="alias">' + messages[message].text + '</td><td></td>'
        }
        string.innerHTML = messageView
        chatTable.insertBefore(string, chatTable.firstChild.nextSibling)
    }
    let chat = document.getElementById('chatBox');
    if (messages.length > 0) {
        massageCount += messages.length
        actualPage.value = page
        scroll(chat.scrollTop + BLOCK_SIZE)
    }
}

function readAll() {
    let companion = document.getElementById('companionId').value;
    $.ajax({
        url: "/chat/read",
        type: "POST",
        data: {
            companion: companion
        }
    });
}

function scroll(position) {
    let chat = document.getElementById("chatBox");
    chat.scrollTop = position;
}