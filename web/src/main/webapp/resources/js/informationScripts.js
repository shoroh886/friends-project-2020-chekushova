const MAX_FIRST_NAME_LENGTH = 15;
const MIN_FIRST_NAME_LENGTH = 4;
const MAX_SECOND_NAME_LENGTH = 15;
const MIN_SECOND_NAME_LENGTH = 4;
const MAX_LAST_NAME_LENGTH = 15;
const MIN_LAST_NAME_LENGTH = 4;
const MAX_PHONE_LENGTH = 20;
const MIN_PHONE_LENGTH = 3;
const MAX_DESCRIPTION_LENGTH = 12;
const MIN_DESCRIPTION_LENGTH = 1;
const MAX_BIRTH_LENGTH = 10;
const MIN_BIRTH_LENGTH = 10;
const MAX_HOME_ADDRESS_LENGTH = 90;
const MIN_HOME_ADDRESS_LENGTH = 1;
const MAX_WORK_ADDRESS_LENGTH = 90;
const MIN_WORK_ADDRESS_LENGTH = 1;
const MAX_EMAIL_LENGTH = 45;
const MIN_EMAIL_LENGTH = 5;
const MAX_ICQ_LENGTH = 9;
const MIN_ICQ_LENGTH = 9;
const MAX_SKYPE_LENGTH = 45;
const MIN_SKYPE_LENGTH = 1;
const MAX_ADDITIONALLY_LENGTH = 255;
const MIN_ADDITIONALLY_LENGTH = 1;
const MAX_LOGIN_LENGTH = 45;
const MIN_LOGIN_LENGTH = 4;
const MAX_PASS_LENGTH = 45;
const MIN_PASS_LENGTH = 4;
const EMAIL_PATTERN = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const requirements = [
    {name: 'firstName', min: MIN_FIRST_NAME_LENGTH, max: MAX_FIRST_NAME_LENGTH, notNull: false, sql: 'null'},
    {name: 'secondName', min: MIN_SECOND_NAME_LENGTH, max: MAX_SECOND_NAME_LENGTH, notNull: false, sql: 'null'},
    {name: 'lastName', min: MIN_LAST_NAME_LENGTH, max: MAX_LAST_NAME_LENGTH, notNull: true, sql: 'null'},
    {name: 'phone1', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false, sql: 'phone'},
    {name: 'description1', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false, sql: 'null'},
    {name: 'phone2', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false, sql: 'phone'},
    {name: 'description2', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false, sql: 'null'},
    {name: 'phone3', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false, sql: 'phone'},
    {name: 'description3', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false, sql: 'null'},
    {name: 'phone4', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false, sql: 'phone'},
    {name: 'description4', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false, sql: 'null'},
    {name: 'phone5', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false, sql: 'phone'},
    {name: 'description5', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false, sql: 'null'},
    {name: 'birth', min: MIN_BIRTH_LENGTH, max: MAX_BIRTH_LENGTH, notNull: true, sql: 'null'},
    {name: 'homeAddress', min: MIN_HOME_ADDRESS_LENGTH, max: MAX_HOME_ADDRESS_LENGTH, notNull: true, sql: 'null'},
    {name: 'workAddress', min: MIN_WORK_ADDRESS_LENGTH, max: MAX_WORK_ADDRESS_LENGTH, notNull: true, sql: 'null'},
    {name: 'email', min: MIN_EMAIL_LENGTH, max: MAX_EMAIL_LENGTH, notNull: false, sql: 'email'},
    {name: 'icq', min: MIN_ICQ_LENGTH, max: MAX_ICQ_LENGTH, notNull: true, sql: 'icq'},
    {name: 'skype', min: MIN_SKYPE_LENGTH, max: MAX_SKYPE_LENGTH, notNull: true, sql: 'skype'},
    {name: 'additionally', min: MIN_ADDITIONALLY_LENGTH, max: MAX_ADDITIONALLY_LENGTH, notNull: true, sql: 'null'},
    {name: 'login', min: MIN_LOGIN_LENGTH, max: MAX_LOGIN_LENGTH, notNull: false, sql: 'null'},
    {name: 'password', min: MIN_PASS_LENGTH, max: MAX_PASS_LENGTH, notNull: false, sql: 'null'},
    {name: 'rPass', min: MIN_PASS_LENGTH, max: MAX_PASS_LENGTH, notNull: true, sql: 'null'}
]

let resources

$(function () {
    $("#birthText").mask("9999-99-99");
    $("#icqText").mask("999999999");
});

$(function () {
    let locale = getLocale()
    $.ajax({
        url: "/locale/get",
        data: {
            lang: locale,
            key: "information",
        },
        success: function (data) {
            resources = data
        }
    });
});

function getLocale() {
    return document.getElementById("lang").value;
}

function checkField(id) {
    removeValidation(id)
    const FORM = document.getElementById('userFields');
    let result = true
    const field = FORM.querySelector('#' + id + 'Text')
    for (const requirement of requirements) {
        let error;
        if (requirement.name + 'Text' === field.id) {
            let sql = requirement.sql
            if (requirement.notNull === false && field.value.length === 0) {
                error = resources[getLocale() + '.information.error.empty']
                showValidationError(error, field)
                result = false
            } else if (requirement.min > field.value.length && field.value.length > 0) {
                error = resources[getLocale() + '.information.error.short'] + requirement.min
                showValidationError(error, field)
                result = false
            } else if (requirement.max < field.value.length) {
                error = resources[getLocale() + '.information.error.long'] + requirement.max
                showValidationError(error, field)
                result = false
            } else if (requirement.sql === "phone" && !localUniqCheck(field.value)) {
                error = resources[getLocale() + '.information.error.duplicate']
                showValidationError(error, field)
                result = false
            } else if (requirement.sql === "birth" && field.value !== '') {
                if (!dateCheck(field.value)) {
                    error = resources[getLocale() + '.registration.error.data']
                    showError(error, field)
                    result = false
                }
            } else if (requirement.sql === "email") {
                if (!validateEmail(field.value)) {
                    error = resources[getLocale() + '.information.error.email']
                    showError(error, field)
                    result = false
                }
            }
            if (sql !== 'null' && field.value.length !== 0 && field.value !== field.placeholder
                && !uniqCheck(field.value, requirement.sql)) {
                error = resources[getLocale() + '.information.error.taken']
                showValidationError(error, field)
                result = false
            }
        }
    }
    return result
}

function validateEmail(email) {
    return EMAIL_PATTERN.test(String(email).toLowerCase());
}

function dateCheck(date) {
    let year = Number(date.substr(0, 4));
    let month = Number(date.substr(5, 2));
    let day = Number(date.substr(8, 2));
    let february = year % 400 === 0 ? 29 : year % 4 === 0 ? year % 100 === 0 ? 28 : 29 : 28;
    if (month > 12 || month < 1 || year < 1900 || day < 1) {
        return false;
    } else if (month === 2 && day > february) {
        return false
    } else if ((month === 4 || month === 6 || month === 9 || month === 11) && day > 30) {
        return false
    } else if (day > 31) {
        return false
    }
    return true
}

function localUniqCheck(value) {
    let phones = document.getElementsByClassName("phoneLabel")
    for (let phone of phones) {
        if (phone.innerHTML === value) {
            return false;
        }
    }
    return true
}

function uniqCheck(field, sql) {
    let result = true;
    $.ajax({
        async: false,
        url: "/data/check",
        data: {
            value: field,
            sql: sql,
            type: "user"
        },
        success: function (data) {
            result = data
        }
    });
    return result;
}

function send() {
    const activeFields = document.querySelectorAll('.saveButton')
    if (activeFields.length > 0) {
        alert(resources[getLocale() + '.information.message.saved'])
    } else {
        const FORM = document.getElementById('userFields');
        if (confirm(resources[getLocale() + '.information.message.send'])) {
            FORM.submit()
        }
    }
}

const showValidationError = function (error, field) {
    removeOldPhoneError()
    const fieldName = field.id.toString().slice(0, -4)
    const errorField = document.createElement('div')
    if (fieldName.includes("phone")) {
        errorField.className = 'errorField phoneError'
    } else if (fieldName.includes("description")) {
        errorField.className = 'errorField phoneError'
    } else {
        errorField.className = 'errorField'
    }
    errorField.style.color = 'red'
    errorField.id = fieldName + 'Error'
    errorField.innerHTML = error
    const after = document.querySelector('#' + fieldName + 'Save')
    field.parentElement.parentNode.insertBefore(errorField, after.parentNode.lastChild.nextSibling)
}

const removeValidation = function (id) {
    let field = document.querySelector('#' + id + 'Error')
    if (field !== null) {
        field.remove()
    }
}

const showPhonesError = function (error) {
    const FORM = document.getElementById('userFields');
    const addButton = FORM.querySelector('#addButton')
    const firstPhoneField = FORM.querySelector('#ph1')
    removeOldPhoneError()
    const errorField = document.createElement('div')
    errorField.className = 'errorField'
    errorField.id = 'phoneError'
    errorField.innerHTML = error
    firstPhoneField.insertBefore(errorField, addButton.parentNode.lastChild.nextSibling)
}

const removeOldPhoneError = function () {
    const FORM = document.getElementById('userFields');
    const oldErrorField = FORM.querySelector('#phoneError')
    if (oldErrorField !== null) {
        oldErrorField.remove()
    }
}

const plus = function plus() {
    const FORM = document.getElementById('userFields');
    removeOldPhoneError()
    let phones = FORM.querySelectorAll('.ph')
    let parent = document.querySelector('.fields')
    let phoneCnt = phones.length
    let mark = document.querySelector("#ph" + phoneCnt)
    let newField =
        '<span id="description' + (phoneCnt + 1) + 'Saved" class="descriptionLabel saved">' +
        '<select id="description' + (phoneCnt + 1) + 'Text" class="select">' +
        '<option value="mobile">' + resources[getLocale() + '.information.label.phoneDescription.mobile'] + '</option>' +
        '<option value="home">' + resources[getLocale() + '.information.label.phoneDescription.home'] + '</option>' +
        '<option value="work">' + resources[getLocale() + '.information.label.phoneDescription.work'] + '</option>' +
        '<option value="alternative">' + resources[getLocale() + '.information.label.phoneDescription.alternative'] + '</option>' +
        '</select>' +
        '</span>' +
        '<span class="label">' + resources[getLocale() + '.information.label.phoneNumber'] + '</span>' +
        '<input type="hidden" id="description' + (phoneCnt + 1) + 'Hidden"  name="description' + (phoneCnt + 1) + '" ' +
        'class="descriptionHidden field" value="">' +
        '<input type="button" id="description' + (phoneCnt + 1) + 'Save" value="' + resources[getLocale() + '.information.button.save'] +
        '" class="descriptionSaveButton saveButton mediumButton mediumYellow boxButton" onclick="save(this.id)">' +
        '<span id="phone' + (phoneCnt + 1) + 'Saved" class="phoneLabel saved">' +
        '<input type="text" id="phone' + (phoneCnt + 1) + 'Text" ' +
        'placeholder="' + resources[getLocale() + '.information.placeholder.phoneNumber'] + '" class="phoneText textField">' +
        '</span>' +
        '<input type="hidden" id="phone' + (phoneCnt + 1) + 'Hidden" name="phone' + (phoneCnt + 1) + '" ' +
        'class="phoneHidden field" value="">' +
        '<input type="hidden" id="phone' + (phoneCnt + 1) + 'Id" name="phone' + (phoneCnt + 1) + 'Id" ' +
        'class="phoneId field" value="0">' +
        '<input type="button" id="phone' + (phoneCnt + 1) + 'Save" value="' + resources[getLocale() + '.information.button.save'] +
        '" class="phoneSaveButton saveButton mediumButton mediumYellow boxButton" onclick="save(this.id)">' +
        '<input type="button" id="phone' + (phoneCnt + 1) + 'Remove" ' +
        'class="removeButton smallButton phDelete" value="" onclick="minus(this.id)">'
    if (phoneCnt > 4) {
        showPhonesError(resources[getLocale() + '.information.error.maxPhones'])
    } else {
        let newTr = document.createElement('div')
        newTr.id = 'ph' + (phoneCnt + 1)
        newTr.className = 'ph fieldBox'
        newTr.innerHTML = newField;
        parent.insertBefore(newTr, mark.nextSibling)
    }
}

function minus(id) {
    const FORM = document.getElementById('userFields');
    const phCnt = FORM.querySelectorAll('.ph').length
    if (phCnt == 1) {
        showPhonesError(resources[getLocale() + '.information.error.lastPhone'])
    } else {
        const index = id.slice(5, -6)
        const removable = document.querySelector("#ph" + index)
        removable.remove()
        changeIndexes(index)
    }
}

function changeIndexes(cnt) {
    const FORM = document.getElementById('userFields');
    const ph = FORM.querySelectorAll('.ph')
    for (let i = 0; i < ph.length; i++) {
        ph[i].id = 'ph' + (i + 1);
        const phChildren = ph[i].childNodes
        for (const child of phChildren) {
            if (child.className === 'descriptionLabel saved') {
                child.id = 'description' + (i + 1) + 'Saved'
                if (child.firstChild !== 'undefined') {
                    child.firstChild.id = 'description' + (i + 1) + 'Text'
                }
            } else if (child.className === 'descriptionHidden field') {
                child.id = 'description' + (i + 1) + 'Hidden'
                child.name = 'description' + (i + 1)
            } else if (child.className === 'descriptionChangeButton mediumButton mediumBlue changeButton boxButton') {
                child.id = 'description' + (i + 1) + 'Change'
            } else if (child.className === 'descriptionSaveButton saveButton mediumButton mediumYellow boxButton') {
                child.id = 'description' + (i + 1) + 'Save'
            } else if (child.className === 'phoneLabel saved') {
                child.id = 'phone' + (i + 1) + 'Saved'
                if (child.firstChild !== 'undefined') {
                    child.firstChild.id = 'phone' + (i + 1) + 'Text'
                }
            } else if (child.className === 'phoneHidden field') {
                child.id = 'phone' + (i + 1) + 'Hidden'
                child.name = 'phone' + (i + 1)
            } else if (child.className === 'phoneChangeButton mediumButton mediumBlue changeButton boxButton') {
                child.id = 'phone' + (i + 1) + 'Change'
            } else if (child.className === 'phoneSaveButton saveButton mediumButton mediumYellow boxButton') {
                child.id = 'phone' + (i + 1) + 'Save'
            } else if (child.className === 'removeButton smallButton phDelete') {
                child.id = 'phone' + (i + 1) + 'Remove'
            } else if (child.className === 'phoneId field') {
                child.id = 'phone' + (i + 1) + 'Id'
                child.name = 'phone' + (i + 1) + 'Id'
            }
        }
    }
    if (cnt == 1) {
        const addButton = document.createElement('input')
        addButton.type = 'button'
        addButton.id = 'addButton'
        addButton.className = 'smallButton phAdd'
        addButton.setAttribute('onclick', 'plus()')
        const parent = FORM.querySelector('#ph1')
        const before = FORM.querySelector('#phone1Remove')
        parent.insertBefore(addButton, before.nextSibling)
    }
}

function change(id) {
    const FORM = document.getElementById('userFields');
    const fieldName = id.slice(0, -6)
    let fieldClass
    if (id.toString().includes('phone') || id.toString().includes('description')) {
        fieldClass = id.slice(0, -7)
    } else {
        fieldClass = id.slice(0, -6)
    }
    const savedField = document.getElementById(fieldName + 'Saved')
    if (id.toString().includes('description')) {
        savedField.innerHTML = '<select id="' + fieldName + 'Text" class="select">' +
            '<option value="mobile">' + resources[getLocale() + '.information.label.phoneDescription.mobile'] + '</option>' +
            '<option value="home">' + resources[getLocale() + '.information.label.phoneDescription.home'] + '</option>' +
            '<option value="work">' + resources[getLocale() + '.information.label.phoneDescription.work'] + '</option>' +
            '<option value="alternative">' + resources[getLocale() + '.information.label.phoneDescription.alternative'] + '</option>' +
            '</select>'
    } else {
        savedField.innerHTML = '<input class="textField" type="text" id="' + fieldName + 'Text" placeholder="' +
            savedField.innerHTML + '">'
    }
    document.getElementById(id).remove()
    const saveButton = document.createElement('input')
    saveButton.type = 'button'
    saveButton.id = fieldName + 'Save'
    saveButton.className = fieldClass + 'SaveButton saveButton mediumButton mediumYellow boxButton'
    saveButton.value = resources[getLocale() + '.information.button.save']
    saveButton.setAttribute('onclick', 'save(this.id)')
    const parent = FORM.querySelector('#' + fieldName + 'Text').parentNode.parentNode
    const before = FORM.querySelector('#' + fieldName + 'Text').parentNode
    parent.insertBefore(saveButton, before.nextSibling)
}

function save(id) {
    const fieldName = id.slice(0, -4)
    let fieldClass
    if (id.toString().includes('phone') || id.toString().includes('description')) {
        fieldClass = id.slice(0, -5)
    } else {
        fieldClass = id.slice(0, -4)
    }
    if (checkField(fieldName)) {
        const FORM = document.getElementById('userFields');
        const fieldName = id.slice(0, -4)
        const savedField = document.getElementById(fieldName + 'Saved')
        const newValue = document.getElementById(fieldName + 'Text').value
        savedField.innerHTML = newValue
        document.getElementById(id).remove()
        document.getElementById(fieldName + 'Hidden').value = newValue
        const changeButton = document.createElement('input')
        changeButton.type = 'button'
        changeButton.id = fieldName + 'Change'
        changeButton.className = fieldClass + 'ChangeButton mediumButton mediumBlue changeButton boxButton'
        changeButton.value = resources[getLocale() + '.information.button.change']
        changeButton.setAttribute('onclick', 'change(this.id)')
        const parent = FORM.querySelector('#' + fieldName + 'Saved').parentNode
        const before = FORM.querySelector('#' + fieldName + 'Saved')
        parent.insertBefore(changeButton, before.nextSibling)
        if (document.getElementById('lang').value === 'ru') {
            if (savedField.innerHTML === 'mobile') {
                savedField.innerHTML = resources[getLocale() + '.information.label.phoneDescription.mobile']
            } else if (savedField.innerHTML === 'work') {
                savedField.innerHTML = resources[getLocale() + '.information.label.phoneDescription.work']
            } else if (savedField.innerHTML === 'home') {
                savedField.innerHTML = resources[getLocale() + '.information.label.phoneDescription.home']
            } else if (savedField.innerHTML === 'alternative') {
                savedField.innerHTML = resources[getLocale() + '.information.label.phoneDescription.alternative']
            }
        }
    }
}