let resources

function getSystemLocale() {
    return document.getElementById("lang").value;
}

$(function () {
    let locale = getSystemLocale()
    $.ajax({
        url: "/locale/get",
        data: {
            lang: locale,
            key: "discussion",
        },
        success: function (data) {
            resources = data
        }
    });
});

function connect() {
    let discusId = Number(document.getElementById("discusId").value)
    let socket = new SockJS('/discus');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/' + discusId, function (event) {
            showMessageOutput(event);
        });
    });
}

function send() {
    let text = document.getElementById('message').value;
    if (text === "") {
        document.getElementById('message').placeholder =
            resources[document.getElementById("lang").value + '.discussion.error.empty']
        document.getElementById('message').style.borderColor = "red"
    } else {
        let discussionId = document.getElementById('discusId').value;
        let from = document.getElementById('spectatorId').value;
        let key = Number(document.getElementById("discusId").value);
        /*        let firstName = document.getElementById('firstName').value;
                let secondName = document.getElementById('secondName').value;*/
        document.getElementById('message').value = '';
        stompClient.send('/app/write/' + key, {},
            JSON.stringify({
                'discussion': discussionId, 'author': from, 'text': text
            }));
    }
}

function changeOption() {
    let discussionId = document.getElementById('discusId').value;
    let access = document.getElementById('accessField').value;
    let status = document.getElementById('statusField').value === "true" ? "1" : "0";
    let key = Number(document.getElementById("discusId").value);
    stompClient.send('/app/write/' + key, {},
        JSON.stringify({
            'discussion': discussionId, 'author': -1, 'text': access + status
        }));
}

function showMessageOutput(event) {
    let incomingMessage = JSON.parse(event.body);
    if (incomingMessage.author.id === -1) {
        changeDiscusView(event)
    } else {
        let cnt = (Number(document.querySelector('#cntValue').innerHTML)) + 1
        document.querySelector('#cntValue').innerHTML = cnt.toString();
        let messagesField = document.getElementById('messages');
        let messages = document.getElementsByClassName('messageBox');
        let page = Number(document.getElementById('page').value);
        if (page === 1) {
            const newMessageBox = document.createElement('div')
            newMessageBox.id = 'messageBox'
            newMessageBox.className = 'minorBox messageBox'
            let avatar = incomingMessage.author.avatar.image === null ? '<img class="avatar" src="/resources/img/default.png" alt="">' :
                '<img class="avatar" src="data:image/jpeg;base64, ' + incomingMessage.author.avatar.image + '" alt="">'
            newMessageBox.innerHTML = avatar +
                '<div class="authorInfo">' + incomingMessage.author.name + incomingMessage.author.surname +
                resources[document.getElementById("lang").value + '.discussion.message.author'] +
                ' </div> ' + incomingMessage.text
            if (messages.length === 10) {
                messages[9].remove()
                messagesField.insertBefore(newMessageBox, messagesField.firstChild)
            } else {
                messagesField.insertBefore(newMessageBox, messagesField.firstChild)
            }
        }
        buildPagination(1);
    }
}

function changeDiscusView(event) {
    let newOption = JSON.parse(event.body).text;
    let rights = Number(document.getElementById('rights').value);
    if (rights > 2) {
        let selects = document.getElementsByClassName("selectOption")
        for (let i = 0; i < selects.length; i++) {
            selects[i].remove();
        }
        let accessField = document.getElementById("accessField")
        if (newOption.charAt(0) === "1") {
            accessField.innerHTML = '<option class="selectOption" value="2">' +
                resources[document.getElementById("lang").value + '.discussion.select.private'] + '</option>' +
                '<option class="selectOption" selected value="1">' +
                resources[document.getElementById("lang").value + '.discussion.select.public'] + '</option>' +
                '<option class="selectOption" value="3">' +
                resources[document.getElementById("lang").value + '.discussion.select.secure'] + '</option>';
        } else if (newOption.charAt(0) === "2") {
            accessField.innerHTML = '<option class="selectOption" selected value="2">' +
                resources[document.getElementById("lang").value + '.discussion.select.private'] + '</option>' +
                '<option class="selectOption" value="1">' +
                resources[document.getElementById("lang").value + '.discussion.select.public'] + '</option>' +
                '<option class="selectOption" value="3">' +
                resources[document.getElementById("lang").value + '.discussion.select.secure'] + '</option>';
        } else if (newOption.charAt(0) === "3") {
            accessField.innerHTML = '<option class="selectOption" value="2">' +
                resources[document.getElementById("lang").value + '.discussion.select.private'] + '</option>' +
                '<option class="selectOption" value="1">' +
                resources[document.getElementById("lang").value + '.discussion.select.public'] + '</option>' +
                '<option class="selectOption" selected value="3">' +
                resources[document.getElementById("lang").value + '.discussion.select.secure'] + '</option>';
        }
        let statusField = document.getElementById("statusField")
        if (newOption.charAt(1) === "1") {
            statusField.innerHTML = '<option class="selectOption" selected value="true">' +
                resources[document.getElementById("lang").value + '.discussion.select.open'] + '</option>' +
                '<option class="selectOption" value="false">' +
                resources[document.getElementById("lang").value + '.discussion.select.close'] + '</option>';
        } else if (newOption.charAt(1) === "0") {
            statusField.innerHTML = '<option class="selectOption" value="true">' +
                resources[document.getElementById("lang").value + '.discussion.select.open'] + '</option>' +
                '<option class="selectOption" selected value="false">' +
                resources[document.getElementById("lang").value + '.discussion.select.close'] + '</option>';
        }
    } else {
        let accessField = document.getElementById("discusAccess")
        if (newOption.charAt(0) === "1") {
            accessField.innerHTML = '<p>' +
                resources[document.getElementById("lang").value + '.discussion.select.public'] + '</p>';
            accessField.style.color = "black"
        } else if (newOption.charAt(0) === "2") {
            accessField.innerHTML = '<p>' +
                resources[document.getElementById("lang").value + '.discussion.select.private'] + '</p>';
            accessField.style.color = "black"
        } else if (newOption.charAt(0) === "3") {
            accessField.innerHTML = '<p>' +
                resources[document.getElementById("lang").value + '.discussion.select.secure'] + '</p>';
            accessField.style.color = "red"
        }
        let statusField = document.getElementById("discusStatus")
        if (newOption.charAt(1) === "1") {
            statusField.innerHTML = '<p>' +
                resources[document.getElementById("lang").value + '.discussion.select.open'] + '</p>';
            statusField.style.color = "black"
            document.getElementById("newMessageBox").innerHTML =
                '<textarea name="message" id="message" class="field" placeholder="message" maxlength="255"></textarea>' +
                '<input type="hidden" name="discusId" value="${discus.id}">' +
                '<input id="writeButton" type="submit" class="bigButton bigYellow" value="write" onclick="send()">'
        } else if (newOption.charAt(1) === "0") {
            statusField.innerHTML = '<p>' +
                resources[document.getElementById("lang").value + '.discussion.select.close'] + '</p>';
            statusField.style.color = "red"
            document.getElementById("newMessageBox").innerHTML =
                '<p id="readOnlyMessage"> ' +
                resources[document.getElementById("lang").value + '.discussion.message.close'] + '</p>'
        }
    }
}

let getPage = function (page, discusId) {
    $.ajax({
        url: "/discussion/page/get",
        data: {
            id: discusId,
            page: page
        },
        success: function (data) {
            buildPage(data, page)
        }
    });
}

let setPage = function (discusId) {
    let page = Number(prompt('Enter the page:'));
    getPage(page, discusId)
}

let cleanPage = function () {
    document.querySelector('#messages').innerHTML = "";
    document.querySelector('#pagination').innerHTML = "";
}

let buildPage = function (data, page) {
    cleanPage()
    document.getElementById("page").value = page
    let messagesInner = "";
    for (key in data) {
        let avatar = data[key].author.avatar.image === null ? '<img class="avatar" src="/resources/img/default.png" alt="">' :
            '<img class="avatar" src="data:image/jpeg;base64, ' + data[key].author.avatar.image + '" alt="">'
        messagesInner = messagesInner +
            '<div id="messageBox" class="minorBox messageBox">' + avatar +
            '<div class="authorInfo">' + data[key].author.name + " " + data[key].author.surname +
            resources[document.getElementById("lang").value + '.discussion.message.author'] +
            '</div>' + data[key].text + '</div>'
    }
    document.querySelector('#messages').innerHTML = messagesInner;
    buildPagination(page)
}

let buildPagination = function (page) {
    let discusId = Number(document.querySelector('#discusId').value)
    let pages = Math.ceil(Number(document.querySelector('#cntValue').innerHTML) / 10)
    let pagination = '';
    let range = pages > 9 ? 7 : pages
    let insert;
    if (page > 1) {
        pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(' + (page - 1) + ', ' + discusId + ')"><< </a></div>'
    }
    if (pages > 1) {
        if (page === 1) {
            pagination = pagination + '<div class="pageBox active"><a class="active" href="#" onclick="getPage(1, ' + discusId + ')">1 </a></div>'
        } else {
            pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(1, ' + discusId + ')">1 </a></div>'
        }
    }
    if (pages > 9) {
        for (let i = 1; i <= range; i++) {
            if (page <= 5) {
                insert = i + 1;
                if (insert === page) {
                    pagination = pagination + '<div class="pageBox active"><a class="active" href="#" onclick="getPage(' + insert + ', ' + discusId + ')">' + insert + ' </a></div>'
                } else if (i === 7) {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="setPage()">... </a></div>'
                } else {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(' + insert + ', ' + discusId + ')">' + insert + ' </a></div>'
                }
            } else if (page > 4 && page < pages - 4) {
                insert = (page + (-4 + i))
                if (i === 1 || i === 7) {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="setPage()">... </a></div>'
                } else if (i === 4) {
                    pagination = pagination + '<div class="pageBox active"><a class="active" href="#" onclick="getPage(' + insert + ', ' + discusId + ')">' + insert + ' </a></div>'
                } else {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(' + insert + ', ' + discusId + ')">' + insert + ' </a></div>'
                }
            } else if (page >= pages - 4) {
                insert = pages + (-8 + i)
                if (insert === page) {
                    pagination = pagination + '<div class="pageBox active"><a class="active" href="#" onclick="getPage(' + insert + ', ' + discusId + ')">' + insert + ' </a></div>'
                } else if (i === 1) {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="setPage()">... </a></div>'
                } else {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(' + insert + ', ' + discusId + ')">' + insert + ' </a></div>'
                }
            }
        }
    } else {
        for (let i = 2; i < range; i++) {
            if (i === page) {
                pagination = pagination + '<div class="pageBox active"><a class="active" href="#" onclick="getPage(' + i + ', ' + discusId + ')">' + i + ' </a></div>'
            } else {
                pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(' + i + ', ' + discusId + ')">' + i + ' </a></div>'
            }
        }
    }
    if (pages > 1) {
        if (page === pages) {
            pagination = pagination + '<div class="pageBox active"><a class="active" href="#" onclick="getPage(' + pages + ', ' + discusId + ')">' + pages + ' </a></div>'
        } else {
            pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(' + pages + ', ' + discusId + ')">' + pages + ' </a></div>'
        }
    }
    if (page < pages) {
        pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(' + (page + 1) + ', ' + discusId + ')">>> </a></div>'
    }
    document.querySelector('#pagination').innerHTML = pagination;
}