let resources

function getSystemLocale() {
    return document.getElementById("lang").value;
}

$(function () {
    let locale = getSystemLocale()
    $.ajax({
        url: "/locale/get",
        data: {
            lang: locale,
            key: "search",
        },
        success: function (data) {
            resources = data
        }
    });
});

let getPage = function (page, type) {
    let filter = document.querySelector('#filter')
    $.ajax({
        url: "/" + type + "s/search/page",
        data: {
            filter: filter.value,
            page: page
        },
        success: function (data) {
            buildPage(data, page, type)
        }
    });
}

let rebuildPage = function (type, page) {
    let filter = document.querySelector('#filter')
    $.ajax({
        url: "/" + type + "s/search/pages",
        data: {
            filter: filter.value,
        },
        success: function (data) {
            changeType(type, data)
            getPage(page, type)
        }
    });
}

let getPages = function (type) {
    let filter = document.querySelector('#filter')
    $.ajax({
        url: "/" + type + "s/search/pages",
        data: {
            filter: filter.value,
        },
        success: function (data) {
            changeType(type, data)
            getPage(1, type)
        }
    });
}

let deleteUser = function (id) {
    let page = Number(document.getElementById('actualPage').value)
    $.ajax({
        url: "/user/delete",
        data: {
            id: id.toString(),
        },
        success: function () {
            rebuildPage("user", page)
        }
    });
}

let deleteGroup = function (name) {
    let page = Number(document.getElementById('actualPage').value)
    $.ajax({
        url: "/group/delete",
        data: {
            name: name,
        },
        success: function () {
            rebuildPage("group", page)
        }
    });
}

let changeType = function (type, pages) {
    document.querySelector('#type').value = type
    document.querySelector('#pages').value = pages
}

let cleanPage = function () {
    document.querySelector('#searchResultsBox').innerHTML = "";
    document.querySelector('#pagination').innerHTML = "";
}

let setPage = function () {
    let page = Number(prompt('' + resources[document.getElementById("lang").value + '.search.modal.page'] + ''));
    let type = document.querySelector('#type').value
    getPage(page, type)
}

let buildPage = function (data, page, type) {
    cleanPage()
    document.querySelector('#actualPage').value = page;
    let searchResultsBoxInner = '';
    let role = document.getElementById('role').value
    let deleteButton
    let right = role === 'admin'
    if (type === "user") {
        for (key in data) {
            if (right) {
                deleteButton = '<input type="button" id="deleteButton" class="mediumButton mediumRed bottomBoxButton" ' +
                    'value="' + resources[document.getElementById("lang").value + '.search.button.delete'] + '" onclick="deleteUser(' + data[key].id + ')">'
            } else {
                deleteButton = ''
            }
            let avatarBox
            if (data[key].avatar.image === null) {
                avatarBox = '<div class="avatarBox"><img class="avatar" src="/resources/img/default.png" alt=""></div>'
            } else {
                avatarBox = '<div class="avatarBox"><img class="avatar" src="data:image/jpeg;base64, ' +
                    data[key].avatar.image + '" alt=""></div>'
            }
            searchResultsBoxInner = searchResultsBoxInner +
                '<div id="resultBox" class="minorBox">' + avatarBox +
                '<div id="resultInfo">' + data[key].name + ' ' + data[key].surname + ' ' +
                data[key].patronymic + '</div>' +
                '<form action="/id' + data[key].id + '" method="get"> ' +
                '<input type="submit" id="showButton" class="mediumButton mediumBlue bottomBoxButton" ' +
                'value="' + resources[document.getElementById("lang").value + '.search.button.show'] + '"></form>' + deleteButton + '</div>'
        }
    } else {
        for (key in data) {
            if (right) {
                deleteButton = '<input type="button" id="deleteButton" class="mediumButton mediumRed bottomBoxButton" ' +
                    'value="' + resources[document.getElementById("lang").value + '.search.button.delete'] + '" onclick="deleteUser(' + data[key].id + ')">'
            } else {
                deleteButton = ''
            }
            let avatarBox
            if (data[key].avatar === null || data[key].avatar === undefined) {
                avatarBox = '<div class="avatarBox"><img class="avatar" src="/resources/img/default.png" alt=""></div>'
            } else {
                avatarBox = '<div class="avatarBox"><img class="avatar" src="data:image/jpeg;base64, ' +
                    data[key].avatar + '" alt=""></div>'
            }
            searchResultsBoxInner = searchResultsBoxInner +
                '<div id="resultBox" class="minorBox">' + avatarBox +
                '<div id="resultInfo">' + data[key].name + '</div>' +
                '<form action="/group' + data[key].name + '" method="get">' +
                '<input type="submit" id="showButton" class="mediumButton mediumBlue bottomBoxButton" ' +
                'value="' + resources[document.getElementById("lang").value + '.search.button.show'] + '"> </form>' + deleteButton + '</div>'
        }
    }
    document.querySelector('#searchResultsBox').innerHTML = searchResultsBoxInner;
    buildPagination(page, type)
}

let buildPagination = function (page, type) {
    type = "'" + type + "'"
    let newPagination = document.querySelector('#pagination');
    let pages = Number(document.querySelector('#pages').value)
    let pagination = '';
    let range = pages > 9 ? 7 : pages
    let insert;
    if (page > 1) {
        pagination = pagination + '<div class="pageBox"><a href="#" ' +
            'onclick="getPage(' + (page - 1) + ', ' + type + ')"><< </a></div>'
    }
    if (pages > 1) {
        if (page === 1) {
            pagination = pagination + '<div class="pageBox active"><a class="active" href="#" ' +
                'onclick="getPage(1, ' + type + ')">1 </a></div>'
        } else {
            pagination = pagination + '<div class="pageBox"><a href="#" onclick="getPage(1, ' + type + ')">1 </a></div>'
        }
    }
    if (pages > 9) {
        for (let i = 1; i <= range; i++) {
            if (page <= 5) {
                insert = i + 1;
                if (insert === page) {
                    pagination = pagination + '<div class="pageBox active"><a class="active" href="#" ' +
                        'onclick="getPage(' + insert + ', ' + type + ')">' + insert + ' </a></div>'
                } else if (i === 7) {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="setPage()">... </a></div>'
                } else {
                    pagination = pagination + '<div class="pageBox"><a href="#" ' +
                        'onclick="getPage(' + insert + ', ' + type + ')">' + insert + ' </a></div>'
                }
            } else if (page > 4 && page < pages - 4) {
                insert = (page + (-4 + i))
                if (i === 1 || i === 7) {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="setPage()">... </a></div>'
                } else if (i === 4) {
                    pagination = pagination + '<div class="pageBox active"><a class="active" href="#" ' +
                        'onclick="getPage(' + insert + ', ' + type + ')">' + insert + ' </a></div>'
                } else {
                    pagination = pagination + '<div class="pageBox"><a href="#" ' +
                        'onclick="getPage(' + insert + ', ' + type + ')">' + insert + ' </a></div>'
                }
            } else if (page >= pages - 4) {
                insert = pages + (-8 + i)
                if (insert === page) {
                    pagination = pagination + '<div class="pageBox active"><a class="active" href="#" ' +
                        'onclick="getPage(' + insert + ', ' + type + ')">' + insert + ' </a></div>'
                } else if (i === 1) {
                    pagination = pagination + '<div class="pageBox"><a href="#" onclick="setPage()">... </a></div>'
                } else {
                    pagination = pagination + '<div class="pageBox"><a href="#" ' +
                        'onclick="getPage(' + insert + ', ' + type + ')">' + insert + ' </a></div>'
                }
            }
        }
    } else {
        for (let i = 2; i < range; i++) {
            if (i === page) {
                pagination = pagination + '<div class="pageBox active"><a class="active" href="#" ' +
                    'onclick="getPage(' + i + ', ' + type + ')">' + i + ' </a></div>'
            } else {
                pagination = pagination + '<div class="pageBox"><a href="#" ' +
                    'onclick="getPage(' + i + ', ' + type + ')">' + i + ' </a></div>'
            }
        }
    }
    if (pages > 1) {
        if (page === pages) {
            pagination = pagination + '<div class="pageBox active"><a class="active" href="#" ' +
                'onclick="getPage(' + pages + ', ' + type + ')">' + pages + ' </a></div>'
        } else {
            pagination = pagination + '<div class="pageBox"><a href="#" ' +
                'onclick="getPage(' + pages + ', ' + type + ')">' + pages + ' </a></div>'
        }
    }
    if (page < pages) {
        pagination = pagination + '<div class="pageBox"><a href="#" ' +
            'onclick="getPage(' + (page + 1) + ', ' + type + ')">>> </a></div>'
    }
    newPagination.innerHTML = pagination;
}