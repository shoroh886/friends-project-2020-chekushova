let wallSize
let resources

function getSystemLocale() {
    return document.getElementById("lang").value;
}

$(function () {
    let locale = getSystemLocale()
    $.ajax({
        url: "/locale/get",
        data: {
            lang: locale,
            key: "registration",
        },
        success: function (data) {
            resources = data
        }
    });
});

function checkScroll() {
    let block = document.getElementById('wall');
    wallSize = block.offsetHeight
    if (Math.trunc(block.scrollTop) === block.scrollHeight - block.clientHeight) {
        getPage()
    }
}

function getPage() {
    let page = document.getElementById("wallPage").value
    let userId = document.getElementById("userPageId").value
    $.ajax({
        url: "/wall/scroll",
        data: {
            page: ++page,
            userId: userId
        },
        success: function (data) {
            buildPrevious(data)
        }
    });
}

function buildPrevious(wall) {
    let wallTable = document.getElementById("wallContent")
    let actualPage = document.getElementById("wallPage")
    let page = Number(actualPage.value) + 1
    for (element in wall) {
        let string = document.createElement('tr')
        let brickView
        let contentId = Number(wall[element].content)
        console.log(contentId)
        if (contentId === 0) {
            string.className = "textBrick"
            brickView = '<td class="textBrickTextBox" colspan="2">' + wall[element].text + '</td>'
        } else {
            string.className = "imgBrick"
            brickView = '<td class="brickImgBox"><img class="brickImg" src="/wall/content?id='
                + wall[element].content + '" alt=""></td><td class="imgBrickTextBox">' + wall[element].text + '</td>'
        }
        string.innerHTML = brickView
        wallTable.insertBefore(string, wallTable.lastChild.nextSibling)
    }
    if (wall.length > 0) {
        actualPage.value = page
    }
}

function postCheck() {
    let form = document.getElementById("wallForm")
    let textField = document.getElementById("wallTextField")
    if (textField.value === "") {
        textField.placeholder = resources[document.getElementById("lang").value + '.registration.error.empty']
        textField.style.borderColor = "red"
    } else {
        form.submit()
    }
}