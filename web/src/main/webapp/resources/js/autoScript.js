$(function () {
    $("#search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/search/auto",
                data: {
                    filter: request.term
                },
                success: function (data) {
                    response($.map(data, function (result, i) {
                        return {value: result.link, label: result.searchField + " " + result.type}
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            $(location).attr('href', ui.item.value)
        }
    });
});