let resources

function getLocale() {
    return document.getElementById("lang").value;
}

$(function () {
    let locale = getLocale()
    $.ajax({
        url: "/locale/get",
        data: {
            lang: locale,
            key: "registration",
        },
        success: function (data) {
            resources = data
        }
    });
});

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}

$(function () {
    $("#birth").mask("9999-99-99");
    $("#icq").mask("999999999");
});

const MAX_FIRST_NAME_LENGTH = 15;
const MIN_FIRST_NAME_LENGTH = 4;
const MAX_SECOND_NAME_LENGTH = 15;
const MIN_SECOND_NAME_LENGTH = 4;
const MAX_LAST_NAME_LENGTH = 15;
const MIN_LAST_NAME_LENGTH = 4;
const MAX_PHONE_LENGTH = 20;
const MIN_PHONE_LENGTH = 3;
const MAX_DESCRIPTION_LENGTH = 12;
const MIN_DESCRIPTION_LENGTH = 1;
const MAX_BIRTH_LENGTH = 10;
const MIN_BIRTH_LENGTH = 10;
const MAX_HOME_ADDRESS_LENGTH = 90;
const MIN_HOME_ADDRESS_LENGTH = 1;
const MAX_WORK_ADDRESS_LENGTH = 90;
const MIN_WORK_ADDRESS_LENGTH = 1;
const MAX_EMAIL_LENGTH = 45;
const MIN_EMAIL_LENGTH = 5;
const MAX_ICQ_LENGTH = 9;
const MIN_ICQ_LENGTH = 9;
const MAX_SKYPE_LENGTH = 45;
const MIN_SKYPE_LENGTH = 1;
const MAX_ADDITIONALLY_LENGTH = 255;
const MIN_ADDITIONALLY_LENGTH = 1;
const MAX_LOGIN_LENGTH = 45;
const MIN_LOGIN_LENGTH = 4;
const MAX_PASS_LENGTH = 45;
const MIN_PASS_LENGTH = 4;
const EMAIL_PATTERN = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const requirements = [
    {name: 'firstName', min: MIN_FIRST_NAME_LENGTH, max: MAX_FIRST_NAME_LENGTH, notNull: false},
    {name: 'secondName', min: MIN_SECOND_NAME_LENGTH, max: MAX_SECOND_NAME_LENGTH, notNull: false},
    {name: 'lastName', min: MIN_LAST_NAME_LENGTH, max: MAX_LAST_NAME_LENGTH, notNull: true},
    {name: 'phone1', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false},
    {name: 'description1', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false},
    {name: 'phone2', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false},
    {name: 'description2', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false},
    {name: 'phone3', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false},
    {name: 'description3', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false},
    {name: 'phone4', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false},
    {name: 'description4', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false},
    {name: 'phone5', min: MIN_PHONE_LENGTH, max: MAX_PHONE_LENGTH, notNull: false},
    {name: 'description5', min: MIN_DESCRIPTION_LENGTH, max: MAX_DESCRIPTION_LENGTH, notNull: false},
    {name: 'birth', min: MIN_BIRTH_LENGTH, max: MAX_BIRTH_LENGTH, notNull: true},
    {name: 'homeAddress', min: MIN_HOME_ADDRESS_LENGTH, max: MAX_HOME_ADDRESS_LENGTH, notNull: true},
    {name: 'workAddress', min: MIN_WORK_ADDRESS_LENGTH, max: MAX_WORK_ADDRESS_LENGTH, notNull: true},
    {name: 'email', min: MIN_EMAIL_LENGTH, max: MAX_EMAIL_LENGTH, notNull: false},
    {name: 'icq', min: MIN_ICQ_LENGTH, max: MAX_ICQ_LENGTH, notNull: true},
    {name: 'skype', min: MIN_SKYPE_LENGTH, max: MAX_SKYPE_LENGTH, notNull: true},
    {name: 'additionally', min: MIN_ADDITIONALLY_LENGTH, max: MAX_ADDITIONALLY_LENGTH, notNull: true},
    {name: 'login', min: MIN_LOGIN_LENGTH, max: MAX_LOGIN_LENGTH, notNull: false},
    {name: 'password', min: MIN_PASS_LENGTH, max: MAX_PASS_LENGTH, notNull: false},
    {name: 'repeatPassword', min: MIN_PASS_LENGTH, max: MAX_PASS_LENGTH, notNull: true}
]

function check() {
    hide('confirm')
    const FORM = document.getElementById('regForm');
    removeValidation()
    let result = true
    const fields = FORM.querySelectorAll('.field')
    for (const requirement of requirements) {
        let error;
        for (const field of fields) {
            if (requirement.name === field.id) {
                if (requirement.notNull === false && field.value.length === 0) {
                    error = resources[getLocale() + '.registration.error.empty']
                    showError(error, field)
                    result = false
                } else if (requirement.min > field.value.length && field.value.length > 0) {
                    error = resources[getLocale() + '.registration.error.short'] + requirement.min
                    showError(error, field)
                    result = false
                } else if (requirement.max < field.value.length) {
                    error = resources[getLocale() + '.registration.error.long'] + requirement.max
                    showError(error, field)
                    result = false
                } else if (field.id === "password") {
                    const repeatPassword = FORM.querySelector('#repeatPassword')
                    if (field.value !== repeatPassword.value) {
                        error = resources[getLocale() + '.registration.error.mismatch']
                        showError(error, field)
                        result = false
                    }
                    if (field.value.includes(' ')) {
                        error = resources[getLocale() + '.registration.error.space']
                        showError(error, field)
                        result = false
                    }
                } else if (field.id === "birth" && field.value !== '') {
                    if (!dateCheck(field.value)) {
                        error = resources[getLocale() + '.registration.error.data']
                        showError(error, field)
                        result = false
                    }
                } else if (field.id.includes("phone") && localUniqCheck(field.value)) {
                    error = resources[getLocale() + '.registration.error.duplicate']
                    showError(error, field)
                    result = false
                } else if (field.id === "email") {
                    if (!validateEmail(field.value)) {
                        error = resources[getLocale() + '.registration.error.email']
                        showError(error, field)
                        result = false
                    }
                }
            }
        }
    }
    if (result) {
        FORM.submit()
    } else {
        return false
    }
}

function validateEmail(email) {
    return EMAIL_PATTERN.test(String(email).toLowerCase());
}

function dateCheck(date) {
    let year = Number(date.substr(0, 4));
    let month = Number(date.substr(5, 2));
    let day = Number(date.substr(8, 2));
    let february = year % 400 === 0 ? 29 : year % 4 === 0 ? year % 100 === 0 ? 28 : 29 : 28;
    if (month > 12 || month < 1 || year < 1900 || day < 1) {
        return false;
    } else if (month === 2 && day > february) {
        return false
    } else if ((month === 4 || month === 6 || month === 9 || month === 11) && day > 30) {
        return false
    } else if (day > 31) {
        return false
    }
    return true
}

function localUniqCheck(value) {
    let phones = document.getElementsByClassName("phone")
    let matchCnt = 0;
    for (let phone of phones) {
        if (phone.value === value) {
            matchCnt++
        }
    }
    return matchCnt > 1
}

let showError = function (error, field) {
    let errorField = document.getElementById(field.id + 'Error')
    errorField.innerHTML = error
}

let removeValidation = function () {
    let errors = document.getElementById('regForm').querySelectorAll('.error')
    for (let i = 0; i < errors.length; i++) {
        errors[i].innerHTML = ''
    }
}

const plus = function plus() {
    const FORM = document.getElementById('regForm');
    removeValidation()
    let phones = FORM.querySelectorAll('.ph')
    let parent = document.querySelector('.fields')
    let phoneField = FORM.querySelector('#phone5')
    let phoneCnt = phones.length
    let mark = document.querySelector("#ph" + phoneCnt)
    let newField =
        '<input type="text" name="phones[' + (phoneCnt) + '].number" id="phone' + (phoneCnt + 1) + '" class="phone field" ' +
        'placeholder="' + resources[getLocale() + '.registration.placeholder.phoneNumber'] + '" maxlength="45"> ' +
        '<div id="phone' + (phoneCnt + 1) + 'Error" class="error phoneEr"></div>' +
        '<select id="description' + (phoneCnt + 1) + '" class="desc field" name="phones[' + (phoneCnt) + '].description">' +
        '<option value="mobile">' + resources[getLocale() + '.registration.select.mobile'] + '</option>' +
        '<option value="home">' + resources[getLocale() + '.registration.select.home'] + '</option>' +
        '<option value="work">' + resources[getLocale() + '.registration.select.work'] + '</option>' +
        '<option value="alternative">' + resources[getLocale() + '.registration.select.alternative'] + '</option>' +
        '</select>' +
        '<div id="description' + (phoneCnt + 1) + 'Error" class="error descEr"></div>' +
        '<input type="button" id="ph' + (phoneCnt + 1) + 'Delete" class="phDelete smallButton" onclick=minus(this.id)>'
    if (phoneCnt > 4) {
        let error = resources[getLocale() + '.registration.error.maxPhones']
        showError(error, phoneField)
    } else {
        let newTr = document.createElement('span')
        newTr.id = 'ph' + (phoneCnt + 1)
        newTr.className = 'ph'
        newTr.innerHTML = newField;
        parent.insertBefore(newTr, mark.nextSibling)
        moveElementsDown()
    }
}

function moveElementsDown() {
    let elements = document.getElementsByClassName('afterPh');
    for (let i = 0; i < elements.length; i++) {
        let top = Number(getComputedStyle(elements[i]).top.replace("px", ''))
        elements[i].style.top = top + 70 + 'px'
    }
    let box = document.getElementById('fields');
    let height = Number(getComputedStyle(box).height.replace("px", ''))
    box.style.height = height + 70 + 'px'
}

function moveElementsUp() {
    let elements = document.getElementsByClassName('afterPh');
    for (let i = 0; i < elements.length; i++) {
        let top = Number(getComputedStyle(elements[i]).top.replace("px", ''))
        elements[i].style.top = top - 70 + 'px'
    }
    let box = document.getElementById('fields');
    let height = Number(getComputedStyle(box).height.replace("px", ''))
    box.style.height = height - 70 + 'px'
}

function minus(id) {
    const FORM = document.getElementById('regForm');
    removeValidation()
    let phCnt = FORM.querySelectorAll('.ph').length
    if (phCnt === 1) {
        let field = FORM.querySelector('#phone1')
        let error = resources[getLocale() + '.registration.error.lastPhone']
        showError(error, field)
    } else {
        let removable = id.slice(2, -6)
        let mark = document.querySelector("#ph" + removable)
        mark.remove()
        changeIndexes(removable)
        moveElementsUp()
    }
}

function changeIndexes(cnt) {
    const FORM = document.getElementById('regForm');
    let ph = FORM.querySelectorAll('.ph')
    let desc = FORM.querySelectorAll('.desc')
    let phone = FORM.querySelectorAll('.phone')
    let phDel = FORM.querySelectorAll('.phDelete')
    let eDesc = FORM.querySelectorAll('.descEr')
    let ePhone = FORM.querySelectorAll('.phoneEr')
    for (let i = 0; i < ph.length; i++) {
        ph[i].id = 'ph' + (i + 1);
        desc[i].id = 'description' + (i + 1)
        if (typeof desc[i] != 'undefined') {
            desc[i].id = 'description' + (i + 1)
            desc[i].name = 'phones[' + i + '].description'
        }
        if (typeof phone[i] != 'undefined') {
            phone[i].id = 'phone' + (i + 1);
            phone[i].name = 'phones[' + i + '].number';
        }
        if (typeof ePhone[i] != 'undefined') {
            ePhone[i].id = 'phone' + (i + 1) + 'Error';
            eDesc[i].id = 'description' + (i + 1) + 'Error';
        }
        phDel[i].id = 'ph' + (i + 1) + 'Delete';
    }
    if (cnt == 1) {
        let addButton = document.createElement('input')
        addButton.type = 'button'
        addButton.id = 'phoneAdd'
        addButton.className = 'phAdd smallButton'
        addButton.setAttribute('onclick', 'plus()')
        let parent = FORM.querySelector('#ph1')
        let before = FORM.querySelector('#ph1Delete')
        parent.insertBefore(addButton, before)
    }
}

function show(type) {
    let elements = document.getElementsByClassName(type);
    for (let i = 0; i < elements.length; i++) {
        elements[i].style.visibility = 'visible'
    }
}

function hide(type) {
    let elements = document.getElementsByClassName(type);
    for (let i = 0; i < elements.length; i++) {
        elements[i].style.visibility = 'hidden'
    }
}