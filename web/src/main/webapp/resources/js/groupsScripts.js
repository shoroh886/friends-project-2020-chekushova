const GROUP_NAME_MAX_LENGTH = 80
const GROUP_NAME_MIN_LENGTH = 8
const GROUP_NAME_PATTERN = /^[A-Za-z0-9 ]+$/

let resources

function getSystemLocale() {
    return document.getElementById("lang").value;
}

$(function () {
    let locale = getSystemLocale()
    $.ajax({
        url: "/locale/get",
        data: {
            lang: locale,
            key: "group",
        },
        success: function (data) {
            resources = data
        }
    });
});

function show() {
    let elements = document.getElementsByClassName("modal");
    for (let i = 0; i < elements.length; i++) {
        elements[i].style.visibility = 'visible'
    }
}

function hide() {
    let elements = document.getElementsByClassName("modal");
    for (let i = 0; i < elements.length; i++) {
        elements[i].style.visibility = 'hidden'
    }
}

function uniqCheck() {
    let value = document.getElementById("groupName").value
    let result = false;
    $.ajax({
        url: "/data/check",
        async: false,
        data: {
            value: value,
            sql: "mgroup",
            type: "group"
        },
        success: function (data) {
            result = data
        }
    });
    return result
}

function createGroup() {
    if (validateGroup()) {
        let form = document.getElementById("createGroupForm")
        form.submit();
    }
}

function createDiscus() {
    if (validateDiscus()) {
        let form = document.getElementById("newDiscusForm")
        form.submit();
    }
}

function validateGroup() {
    let groupNameField = document.getElementById("groupName")
    let errorField = document.getElementById("errorField")
    if (groupNameField.value === "") {
        errorField.innerHTML = resources[document.getElementById("lang").value + '.groups.error.empty']
        groupNameField.style.borderColor = "red"
        return false
    } else if (groupNameField.value.length < GROUP_NAME_MIN_LENGTH) {
        errorField.innerHTML = resources[document.getElementById("lang").value + '.groups.error.short'] + GROUP_NAME_MIN_LENGTH
        groupNameField.style.borderColor = "red"
        return false
    } else if (groupNameField.value.length > GROUP_NAME_MAX_LENGTH) {
        errorField.innerHTML = resources[document.getElementById("lang").value + '.groups.error.long'] + GROUP_NAME_MAX_LENGTH
        groupNameField.style.borderColor = "red"
        return false
    } else if (!patternCheck(groupNameField.value)) {
        errorField.innerHTML = resources[document.getElementById("lang").value + '.groups.error.incorrect']
        groupNameField.style.borderColor = "red"
        return false
    } else if (!uniqCheck()) {
        errorField.innerHTML = resources[document.getElementById("lang").value + '.groups.error.taken']
        groupNameField.style.borderColor = "red"
        return false
    } else {
        return true
    }
}

function patternCheck(string) {
    return GROUP_NAME_PATTERN.test(string)
}

function validateDiscus() {
    let result = true
    let themeField = document.getElementById("theme")
    let messageField = document.getElementById("message")
    if (themeField.value === "") {
        themeField.placeholder = resources[document.getElementById("lang").value + '.groups.error.empty']
        themeField.style.borderColor = "red"
        result = false
    }
    if (messageField.value === "") {
        messageField.placeholder = resources[document.getElementById("lang").value + '.groups.error.empty']
        messageField.style.borderColor = "red"
        result = false
    }
    return result
}