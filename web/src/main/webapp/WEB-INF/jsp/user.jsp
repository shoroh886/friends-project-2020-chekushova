<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="name" value="${requestScope.user.name}"/>
<c:set var="secondName" value="${requestScope.user.surname}"/>
<c:set var="phone" value="${requestScope.user.phones}"/>
<c:set var="email" value="${requestScope.user.email}"/>
<c:set var="userPageId" value="${requestScope.user.id}"/>
<c:set var="spectator" value="${sessionScope.user}"/>
<c:set var="status" value="${requestScope.status}"/>
<c:set var="unread" value="${requestScope.unread}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <script type="text/javascript" src="../../resources/js/userScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/userPageStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form>
        <button id="language<spring:message code="general.locale"/>" class="localeButton" name="language"
                value="<spring:message code="general.translate"/>" onchange="submit()"></button>
    </form>
</div>
<div class="box">
    <input type="hidden" id="userPageId" value="${userPageId}">
    <div class="leftBoxTop">
        <div class="avatarBox">
            <c:choose>
                <c:when test="${requestScope.user.avatar.image == null}">
                    <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
                </c:when>
                <c:otherwise>
                    <img class="avatar" src="data:image/jpeg;base64, ${requestScope.user.avatar.base64Avatar}" alt="">
                </c:otherwise>
            </c:choose>
        </div>
        <div id="userData">
            <p id="name"><spring:message code="user.data.firstName"/>${name}</p>
            <p id="sName"><spring:message code="user.data.secondName"/>${secondName}</p>
            <p id="phone"><spring:message code="user.data.phone"/>${phone.get(0).number}</p>
            <p id="mail"><spring:message code="user.data.email"/>${email}</p>
        </div>
    </div>
    <div class="leftBoxBot">
        <c:if test="${userPageId == spectator.id}">
            <form action="${pageContext.request.contextPath}/user/info/get" method="get">
                <input id="fullInfo" type="submit" class="mediumButton mediumBlue leftBoxButton"
                       value="<spring:message code="user.button.info"/>">
            </form>
            <form action="${pageContext.request.contextPath}/user/json/get" method="get">
                <input id="downloadUser" type="submit" class="mediumButton mediumYellow leftBoxButton"
                       value="<spring:message code="user.button.download"/>">
            </form>
        </c:if>
        <c:choose>
            <c:when test="${userPageId != sessionScope.id}">
                <form action="${pageContext.request.contextPath}/chat" method="get">
                    <input type="hidden" name="companionId" value="${userPageId}">
                    <input id="chatButton" type="submit" class="mediumButton mediumBlue leftBoxButton"
                           value="<spring:message code="user.button.chat"/>">
                </form>
            </c:when>
            <c:when test="${userPageId == sessionScope.id}">
                <form action="${pageContext.request.contextPath}/chats" method="get">
                    <input id="chats" type="submit" class="mediumButton mediumBlue leftBoxButton"
                           value="<spring:message code="user.button.chatList"/>${unread}"/>
                </form>
            </c:when>
        </c:choose>
        <c:if test="${userPageId != sessionScope.id}">
            <c:choose>
                <c:when test="${status == null}">
                    <br>
                </c:when>
                <c:when test="${status == 0}">
                    <form action="${pageContext.request.contextPath}/friend/add" method="post">
                        <input type="hidden" name="friendId" value="${userPageId}">
                        <input type="hidden" name="selfId" value="${sessionScope.id}">
                        <input id="addFriendButton" class="mediumButton mediumYellow leftBoxButton" type="submit"
                               value="<spring:message code="user.button.addFriend"/>">
                    </form>
                </c:when>
                <c:when test="${status == 1}">
                    <p id="requestMessage"><spring:message code="user.message.sent"/></p>
                    <form action="${pageContext.request.contextPath}/friend/remove" method="post">
                        <input type="hidden" name="friendId" value="${userPageId}">
                        <input type="hidden" name="selfId" value="${sessionScope.id}">
                        <input id="cancelFriendButton" class="mediumButton mediumRed leftBoxButton" type="submit"
                               value="<spring:message code="user.button.cancel"/>">
                    </form>
                </c:when>
                <c:when test="${status == 2}">
                    <form action="${pageContext.request.contextPath}/friend/accept" method="post">
                        <input type="hidden" name="friendId" value="${userPageId}">
                        <input type="hidden" name="selfId" value="${sessionScope.id}">
                        <input id="acceptRequestButton" class="mediumButton mediumYellow leftBoxButton" type="submit"
                               value="<spring:message code="user.button.accept"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/friend/reject" method="post">
                        <input type="hidden" name="friendId" value="${userPageId}">
                        <input type="hidden" name="selfId" value="${sessionScope.id}">
                        <input id="rejectRequestButton" class="mediumButton mediumRed leftBoxButton" type="submit"
                               value="<spring:message code="user.button.reject"/>">
                    </form>
                </c:when>
                <c:when test="${status == 3}">
                    <p id="subscribeMessage"><spring:message code="user.message.subscribed"/></p>
                    <form action="${pageContext.request.contextPath}/friend/remove" method="post">
                        <input type="hidden" name="friendId" value="${userPageId}">
                        <input type="hidden" name="selfId" value="${sessionScope.id}">
                        <input id="stopSubscribeButton" type="submit" class="mediumButton mediumRed leftBoxButton"
                               value="<spring:message code="user.button.stop"/>">
                    </form>
                </c:when>
                <c:when test="${status == 4}">
                    <p id="subscriberMessage"><spring:message code="user.message.subscriber"/></p>
                    <form action="${pageContext.request.contextPath}/friend/accept" method="post">
                        <input type="hidden" name="friendId" value="${userPageId}">
                        <input type="hidden" name="selfId" value="${sessionScope.id}">
                        <input id="addFriendSecondButton" class="mediumButton mediumYellow leftBoxButton" type="submit"
                               value="<spring:message code="user.button.accept"/>">
                    </form>
                </c:when>
                <c:when test="${status == 5}">
                    <p id="friendMessage"><spring:message code="user.message.friend"/></p>
                    <form action="${pageContext.request.contextPath}/friend/remove" method="post">
                        <input type="hidden" name="friendId" value="${userPageId}">
                        <input type="hidden" name="selfId" value="${sessionScope.id}">
                        <input id="removeFriendSecondButton" class="mediumButton mediumRed leftBoxButton" type="submit"
                               value="<spring:message code="user.button.remove"/>">
                    </form>
                </c:when>
            </c:choose>
        </c:if>
        <form action="${pageContext.request.contextPath}/friends" method="get">
            <input type="hidden" name="userPageId" value="${userPageId}">
            <input id="friends" class="mediumButton mediumBlue leftBoxButton" type="submit"
                   value="<spring:message code="user.button.friends"/>">
        </form>
        <form action="${pageContext.request.contextPath}/groups" method="get">
            <input type="hidden" name="userPageId" value="${userPageId}">
            <input id="groups" class="mediumButton mediumBlue leftBoxButton" type="submit"
                   value="<spring:message code="user.button.groups"/>">
        </form>
    </div>
    <div class="rightBox">
        <input type="hidden" id="wallPage" name="wallPage" value="0">
        <c:choose>
            <c:when test="${userPageId == sessionScope.id}">
                <div class="newBrick">
                    <form id="wallForm" action="${pageContext.request.contextPath}/wall/post" method="post"
                          enctype="multipart/form-data">
                        <input id="file" class="newBrickFile" type="file" name="file" accept=".jpg, .jpeg, .png">
                        <label for="file" id="fileLabel"></label>
                        <textarea id="wallTextField" class="newBrickText" name="text" maxlength="255"></textarea>
                        <input id="postButton" type="button" class="bigButton bigYellow"
                               value="<spring:message code="user.button.post"/>" onclick="postCheck()">
                    </form>
                </div>
            </c:when>
        </c:choose>
        <div id="wall" class="wall" onscroll="checkScroll()">
            <table id="wallContent" class="wallContent">
                <c:if test="${requestScope.wall.size() > 0}">
                    <c:forEach var="wall" items="${requestScope.wall}">
                        <c:choose>
                            <c:when test="${wall.content != null}">
                                <tr class="imgBrick">
                                    <td class="brickImgBox">
                                        <img class="brickImg"
                                             src="${pageContext.request.contextPath}/wall/content?id=${wall.content}"
                                             alt="">
                                    </td>
                                    <td class="imgBrickTextBox">
                                        <p>${wall.text}</p>
                                    </td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr class="textBrick">
                                    <td class="textBrickTextBox" colspan="2">
                                        <p>${wall.text}</p>
                                    </td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:if>
            </table>
        </div>
    </div>
</div>
<div id="hiddenFields">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
</body>
</html>
