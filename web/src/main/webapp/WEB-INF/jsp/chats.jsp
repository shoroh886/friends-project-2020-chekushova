<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="spectator" value="${sessionScope.user}"/>
<c:set var="chats" value="${requestScope.chats}"/>
<html>
<head>
    <title>ChatList</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/chatsListStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form>
        <button id="language<spring:message code="general.locale"/>" class="localeButton" name="language"
                value="<spring:message code="general.translate"/>" onchange="submit()"></button>
    </form>
</div>
<div id="chatsBox" class="majorBox">
    <p><spring:message code="chats.message.chats"/></p>
    <c:forEach var="chat" items="${requestScope.chats}">
        <div class="minorBox userBox">
            <div class="avatarBox">
                <c:if test="${chat.avatar == null}">
                    <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
                </c:if>
                <c:if test="${chat.avatar != null}">
                    <img class="avatar" src="data:image/jpeg;base64, ${chat.base64Avatar}" alt="">
                </c:if>
            </div>
            <div class="userInfo">
                    ${chat.name}
                    ${chat.surname}
            </div>
            <form action="${pageContext.request.contextPath}/id${chat.sender}" method="get">
                <input type="submit" class="mediumButton mediumBlue showButton"
                       value="<spring:message code="chats.button.show"/>">
            </form>
            <form action="${pageContext.request.contextPath}/chat" method="get">
                <input type="hidden" name="companionId" value="${chat.sender}">
                <input type="submit" class="mediumButton mediumYellow openChatButton"
                       value="<spring:message code="chats.button.chat"/>${chat.newMessageCount}">
            </form>
        </div>
    </c:forEach>
</div>
</body>
</html>