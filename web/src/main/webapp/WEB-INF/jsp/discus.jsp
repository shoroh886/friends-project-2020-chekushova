<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="discussion" value="${discussion}"/>
<c:set var="rights" value="${rights}"/>
<c:set var="messages" value="${messages}"/>
<c:set var="self" value="${sessionScope.user.id}"/>
<c:set var="spectator" value="${sessionScope.user}"/>
<c:set var="noOfPages" value="${pages}"/>
<c:set var="url" value="${pageContext.request.contextPath}/discussion${pageContext.request.queryString}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.4.0/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/discusStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body onload="connect()">
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form action="${pageContext.request.contextPath}/discussion">
        <input type="hidden" name="id" value="${pageContext.request.getParameter("id")}">
        <input type="hidden" name="groupName" value="${pageContext.request.getParameter("groupName")}">
        <input type="hidden" name="language" value="<spring:message code="general.translate"/>">
        <input type="submit" id="language<spring:message code="general.locale"/>" class="localeButton" value="">
    </form>
</div>
<div id="box">
    <div id="groupLeftBox" class="majorBox">
        <input type="hidden" id="spectatorId" value="${spectator.id}">
        <div id="discusName">
            ${discussion.name}
        </div>
        <div id="discusCnt">
            <spring:message code="discussion.label.count"/><p id="cntValue">${discussion.count}</p>
        </div>
        <div id="discusMembers">
            <spring:message code="discussion.label.members"/><p id="memberValue">${discussion.members}</p>
        </div>
        <div id="discusStatusBox">
            <p id="statusHint" class="hint"><spring:message code="discussion.label.status"/></p>
            <c:choose>
                <c:when test="${rights > 2}">
                    <select id="statusField" class="field" name="status">
                        <c:choose>
                            <c:when test="${discussion.status}">
                                <option class="selectOption" selected value="true">
                                    <spring:message code="discussion.select.open"/>
                                </option>
                                <option class="selectOption" value="false">
                                    <spring:message code="discussion.select.close"/>
                                </option>
                            </c:when>
                            <c:when test="${!discussion.status}">
                                <option class="selectOption" value="true">
                                    <spring:message code="discussion.select.open"/>
                                </option>
                                <option class="selectOption" selected value="false">
                                    <spring:message code="discussion.select.close"/>
                                </option>
                            </c:when>
                        </c:choose>
                    </select>
                </c:when>
                <c:otherwise>
                    <div id="discusStatus">
                        <c:if test="${discussion.status}">
                            <p><spring:message code="discussion.select.open"/></p>
                        </c:if>
                        <c:if test="${!discussion.status}">
                            <p id="status" style="color: red"><spring:message code="discussion.select.close"/></p>
                        </c:if>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <div id="discusAccessBox">
            <p id="accessHint" class="hint"><spring:message code="discussion.label.access"/></p>
            <c:choose>
                <c:when test="${rights > 2}">
                    <select id="accessField" class="field" name="access">
                        <c:choose>
                            <c:when test="${discussion.access == 1}">
                                <option class="selectOption" value="2">
                                    <spring:message code="discussion.select.private"/>
                                </option>
                                <option class="selectOption" selected value="1">
                                    <spring:message code="discussion.select.public"/>
                                </option>
                                <option class="selectOption" value="3">
                                    <spring:message code="discussion.select.secure"/>
                                </option>
                            </c:when>
                            <c:when test="${discussion.access == 2}">
                                <option class="selectOption" selected value="2">
                                    <spring:message code="discussion.select.private"/>
                                </option>
                                <option class="selectOption" value="1">
                                    <spring:message code="discussion.select.public"/>
                                </option>
                                <option class="selectOption" value="3">
                                    <spring:message code="discussion.select.secure"/>
                                </option>
                            </c:when>
                            <c:when test="${discussion.access == 3}">
                                <option class="selectOption" value="2">
                                    <spring:message code="discussion.select.private"/>
                                </option>
                                <option class="selectOption" value="1">
                                    <spring:message code="discussion.select.public"/>
                                </option>
                                <option class="selectOption" selected value="3">
                                    <spring:message code="discussion.select.secure"/>
                                </option>
                            </c:when>
                        </c:choose>
                    </select>
                </c:when>
                <c:otherwise>
                    <div id="discusAccess">
                        <c:if test="${discussion.access == 1}">
                            <p><spring:message code="discussion.select.public"/></p>
                        </c:if>
                        <c:if test="${discussion.access == 2}">
                            <p><spring:message code="discussion.select.private"/></p>
                        </c:if>
                        <c:if test="${discussion.access == 3}">
                            <p style="color: red"><spring:message code="discussion.select.secure"/></p>
                        </c:if>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <c:if test="${rights > 2}">
            <button type="button" id="applyButton" class="bigButton bigRed" onclick="changeOption()">
                <spring:message code="discussion.button.apply"/>
            </button>
        </c:if>
    </div>
    <div id="groupRightBox">
        <div id="messagesBox" class="majorBox">
            <div id="newMessageBox" class="minorBox">
                <c:choose>
                    <c:when test="${discussion.status == true || rights > 2}">
                    <textarea name="message" id="message" class="field"
                              placeholder="<spring:message code="discussion.placeholder.message"/>"
                              maxlength="255"></textarea>
                        <input type="hidden" name="discusId" value="${discussion.id}">
                        <input id="writeButton" type="submit" class="bigButton bigYellow"
                               value="<spring:message code="discussion.button.write"/>"
                               onclick="send()">
                    </c:when>
                    <c:otherwise>
                        <p id="readOnlyMessage"><spring:message code="discussion.message.close"/></p>
                    </c:otherwise>
                </c:choose>
            </div>
            <div id="pagination">
                <c:if test="${noOfPages > 1}">
                    <c:forEach begin="1" end="${noOfPages > 9 ? 9 : noOfPages}" var="i">
                        <c:choose>
                            <c:when test="${noOfPages <= 9}">
                                <c:choose>
                                    <c:when test="${i == 1}">
                                        <div class="pageBox active">
                                            <a class="active" href="#"
                                               onclick="getPage(${i}, ${discussion.id})">${i}</a>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="pageBox">
                                            <a href="#" onclick="getPage(${i}, ${discussion.id})">${i}</a>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${i == 1}">
                                        <div class="pageBox">
                                            <a href="#" class="active"
                                               onclick="getPage(${i}, ${discussion.id})">${i}</a>
                                        </div>
                                    </c:when>
                                    <c:when test="${i == 8}">
                                        <div class="pageBox">
                                            <a href="#" onclick="setPage()">...</a>
                                        </div>
                                    </c:when>
                                    <c:when test="${i == 9}">
                                        <div class="pageBox">
                                            <a href="#"
                                               onclick="getPage(${noOfPages}, ${discussion.id})">${noOfPages}</a>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="pageBox">
                                            <a href="#" onclick="getPage(${i}, ${discussion.id})">${i}</a>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <c:if test="${noOfPages > 1}">
                        <div class="pageBox">
                            <a href="#" class="page" onclick="getPage(2, ${discussion.id})">>></a>
                        </div>
                    </c:if>
                </c:if>
            </div>
            <div id="messages">
                <c:forEach var="message" items="${messages}">
                    <div id="messageBox" class="minorBox messageBox">
                        <c:if test="${message.author.avatar.image == null}">
                            <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                                 alt="">
                        </c:if>
                        <c:if test="${message.author.avatar.image != null}">
                            <img class="avatar" src="data:image/jpeg;base64, ${message.author.avatar.base64Avatar}"
                                 alt="">
                        </c:if>
                        <div class="authorInfo">
                                ${message.author.name} ${message.author.surname} <spring:message
                                code="discussion.message.author"/>
                        </div>
                            ${message.text}
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<div id="hiddenFields">
    <input type="hidden" id="page" value="1">
    <input type="hidden" id="firstName" value="${spectator.name}">
    <input type="hidden" id="secondName" value="${spectator.surname}">
    <input type="hidden" id="rights" value="${rights}">
    <input type="hidden" id="discusId" value="${discussion.id}">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
<script src="../../resources/js/discusScript.js"></script>
</body>
</html>