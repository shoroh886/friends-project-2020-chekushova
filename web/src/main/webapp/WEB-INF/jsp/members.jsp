<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="spectator" value="${sessionScope.user}"/>
<c:set var="name" value="${requestScope.groupName}"/>
<c:set var="requests" value="${requestScope.requests}"/>
<c:set var="members" value="${requestScope.members}"/>
<c:set var="admins" value="${requestScope.admins}"/>
<c:set var="creator" value="${requestScope.creator}"/>
<c:set var="rights" value="${requestScope.rights}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/membersStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form>
        <button id="language<spring:message code="general.locale"/>" class="localeButton" name="language"
                value="<spring:message code="general.translate"/>" onchange="submit()"></button>
    </form>
</div>
<div class="box">
    <div id="creatorBox" class="majorBox roleBox">
        <p><spring:message code="members.message.creator"/></p>
        <div class="memberBox minorBox">
            <div class="avatarBox">
                <c:if test="${creator.avatar == null}">
                    <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
                </c:if>
                <c:if test="${creator.avatar != null}">
                    <img class="avatar" src="data:image/jpeg;base64, ${creator.avatar}" alt="">
                </c:if>
            </div>
            <div class="userInfo">
                ${creator.name}
                ${creator.surname}
            </div>
            <form action="${pageContext.request.contextPath}/id${creator.account}">
                <input type="submit" class="mediumButton mediumBlue showButton"
                       value="<spring:message code="members.button.show"/>">
            </form>
        </div>
    </div>
    <c:if test="${rights > 2}">
        <div id="requestBox" class="majorBox roleBox">
            <p><spring:message code="members.message.request"/></p>
            <c:forEach var="request" items="${requests}">
                <div class="memberBox minorBox">
                    <div class="avatarBox">
                        <c:if test="${request.avatar == null}">
                            <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                                 alt="">
                        </c:if>
                        <c:if test="${request.avatar != null}">
                            <img class="avatar" src="data:image/jpeg;base64, ${request.avatar}" alt="">
                        </c:if>
                    </div>
                    <div class="userInfo">
                            ${request.name}
                            ${request.surname}
                    </div>
                    <form action="${pageContext.request.contextPath}/id${request.account}">
                        <input type="submit" class="mediumButton mediumBlue showButton"
                               value="<spring:message code="members.button.show"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/member/include" method="post">
                        <input type="hidden" name="memberId" value="${request.account}">
                        <input type="hidden" name="groupName" value="${name}">
                        <input type="submit" class="mediumButton mediumYellow includeButton"
                               value="<spring:message code="members.button.include"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/member/refuse" method="post">
                        <input type="hidden" name="memberId" value="${request.account}">
                        <input type="hidden" name="groupName" value="${name}">
                        <input type="submit" class="mediumButton mediumRed refuseButton"
                               value="<spring:message code="members.button.refuse"/>">
                    </form>
                </div>
            </c:forEach>
        </div>
    </c:if>
    <c:if test="${rights > 1}">
        <div id="adminBox" class="majorBox roleBox">
            <p><spring:message code="members.message.admin"/></p>
            <c:forEach var="admin" items="${admins}">
                <div class="memberBox minorBox">
                    <div class="avatarBox">
                        <c:if test="${admin.avatar == null}">
                            <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                                 alt="">
                        </c:if>
                        <c:if test="${admin.avatar != null}">
                            <img class="avatar" src="data:image/jpeg;base64, ${admin.avatar}" alt="">
                        </c:if>
                    </div>
                    <div class="userInfo">
                            ${admin.name}
                            ${admin.surname}
                    </div>
                    <form action="${pageContext.request.contextPath}/id${admin.account}">
                        <input type="submit" class="mediumButton mediumBlue showButton"
                               value="<spring:message code="members.button.show"/>">
                    </form>
                    <c:if test="${rights == 4}">
                        <form action="${pageContext.request.contextPath}/group/give" method="post">
                            <input type="hidden" name="memberId" value="${admin.account}">
                            <input type="hidden" name="groupName" value="${name}">
                            <input type="submit" class="mediumButton mediumRed giveButton"
                                   value="<spring:message code="members.button.give"/>">
                        </form>
                        <form action="${pageContext.request.contextPath}/member/reduce" method="post">
                            <input type="hidden" name="memberId" value="${admin.account}">
                            <input type="hidden" name="groupName" value="${name}">
                            <input type="submit" class="mediumButton mediumRed reduceButton"
                                   value="<spring:message code="members.button.low"/>">
                        </form>
                        <form action="${pageContext.request.contextPath}/member/refuse" method="post">
                            <input type="hidden" name="memberId" value="${admin.account}">
                            <input type="hidden" name="groupName" value="${name}">
                            <input type="submit" class="mediumButton mediumRed removeButton"
                                   value="<spring:message code="members.button.remove"/>">
                        </form>
                    </c:if>
                </div>
            </c:forEach>
        </div>
        <div id="memberBox" class="majorBox roleBox">
            <p><spring:message code="members.message.member"/></p>
            <c:forEach var="member" items="${members}">
                <div class="memberBox minorBox">
                    <div class="avatarBox">
                        <c:if test="${member.avatar == null}">
                            <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                                 alt="">
                        </c:if>
                        <c:if test="${member.avatar != null}">
                            <img class="avatar" src="data:image/jpeg;base64, ${member.avatar}" alt="">
                        </c:if>
                    </div>
                    <div class="userInfo">
                            ${member.name}
                            ${member.surname}
                    </div>
                    <form action="${pageContext.request.contextPath}/id${member.account}">
                        <input type="submit" class="mediumButton mediumBlue showButton"
                               value="<spring:message code="members.button.show"/>">
                    </form>
                    <c:if test="${rights > 2}">
                        <form action="${pageContext.request.contextPath}/member/raise" method="post">
                            <input type="hidden" name="memberId" value="${member.account}">
                            <input type="hidden" name="groupName" value="${name}">
                            <input type="submit" class="mediumButton mediumYellow raiseButton"
                                   value="<spring:message code="members.button.up"/>">
                        </form>
                        <form action="${pageContext.request.contextPath}/member/refuse" method="post">
                            <input type="hidden" name="memberId" value="${member.account}">
                            <input type="hidden" name="groupName" value="${name}">
                            <input type="submit" class="mediumButton mediumRed removeButton"
                                   value="<spring:message code="members.button.remove"/>">
                        </form>
                    </c:if>
                </div>
            </c:forEach>
        </div>
    </c:if>
</div>
<div id="hiddenFields">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
</body>
</html>