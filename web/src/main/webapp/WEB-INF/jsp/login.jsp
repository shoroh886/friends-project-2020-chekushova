<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>login</title>
    <link href="${pageContext.request.contextPath}/resources/css/loginPageStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="box">
    <div class="welcome">
        <div class="logoText"><spring:message code="login.welcome"/></div>
        <img class="friends" src="${pageContext.request.contextPath}/resources/img/friends.png" alt="logo">
    </div>
    <div id="logBox">
        <div class="error">
            <c:if test="${requestScope.message != null}">${requestScope.message}</c:if>
        </div>
        <form action="${pageContext.request.contextPath}/login">
            <input type="hidden" name="language" value="<spring:message code="general.translate"/>">
            <input type="submit" id="language<spring:message code="general.locale"/>" class="localeButton" value="">
        </form>
        <form action="${pageContext.request.contextPath}/logging" method="post">
            <input type="text" name="login" id="login" placeholder="<spring:message code="login.placeholder.login"/>"
                   maxlength="45">
            <input type="password" name="password" id="password"
                   placeholder="<spring:message code="login.placeholder.password"/>" maxlength="45">
            <input type="checkbox" id="remember-me" name="remember-me"/>
            <label id="rmLabel" for="remember-me"></label>
            <p id="rmHint"><spring:message code="login.label.remember"/></p>
            <img id="hint" src="${pageContext.request.contextPath}/resources/img/RememberMe%20hint.png" alt="">
            <input type="submit" id="enter" value="<spring:message code="login.button.enter"/>">
        </form>
        <form action="${pageContext.request.contextPath}/registration" method="get">
            <input type="submit" id="join" value="<spring:message code="login.button.signIn"/>">
        </form>
    </div>
</div>
</body>
</html>