<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="spectator" value="${sessionScope.user}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/groupsStyle.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../resources/js/groupsScripts.js"></script>
</head>
<body>
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form action="${pageContext.request.contextPath}/groups">
        <input type="hidden" name="userPageId" value="${pageContext.request.getParameter("userPageId")}">
        <input type="hidden" name="language" value="<spring:message code="general.translate"/>">
        <input type="submit" id="language<spring:message code="general.locale"/>" class="localeButton" value="">
    </form>
</div>
<div id="box">
    <div id="groupsBox" class="majorBox">
        <p id="pageName"><spring:message code="groups.message.groups"/></p>
        <button type="button" id="createButton" class="bigButton bigYellow"
                onclick="show()"><spring:message code="groups.button.create"/></button>
        <c:forEach var="group" items="${requestScope.groups}">
            <div class="minorBox groupBox">
                <div class="avatarBox">
                    <c:if test="${group.avatar == null}">
                        <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                             alt="">
                    </c:if>
                    <c:if test="${group.avatar != null}">
                        <img class="avatar" src="${pageContext.request.contextPath}/wall/content?id=${group.avatar}"
                             alt="">
                    </c:if>
                </div>
                <div class="groupName">
                        ${group.name}
                </div>
                <div class="groupDescription">
                        ${group.description}
                </div>
                <form action="${pageContext.request.contextPath}/group${group.name}">
                    <input type="submit" class="mediumButton mediumBlue showButton"
                           value="<spring:message code="groups.button.show"/>">
                </form>
                    <%--                <form action="${pageContext.request.contextPath}/leave" method="post">
                                        <input type="hidden" name="groupName" value="${group.name}">
                                        <input type="hidden" name="member" value="${spectator.id}">
                                        <input type="submit" class="mediumButton mediumRed leaveButton"
                                               value="<spring:message code="groups.button.leave"/>">
                                    </form>--%>
            </div>
        </c:forEach>
    </div>
    <div id="createGroupBox" class="majorBox modal">
        <form id="createGroupForm" action="${pageContext.request.contextPath}/group/create" method="post">
            <input type="text" name="groupName" id="groupName"
                   placeholder="<spring:message code="groups.modal.placeholder.groupName"/>" maxlength="90">
            <p id="errorField"></p>
            <textarea name="description" id="description" maxlength="255"
                      placeholder="<spring:message code="groups.modal.placeholder.description"/>"></textarea>
            <input type="hidden" name="creator" value="${spectator.id}">
            <button type="button" id="cancelButton" class="bigButton bigRed" onclick="hide()">
                <spring:message code="groups.modal.button.cancel"/></button>
            <button type="button" id="saveButton" class="bigButton bigYellow" onclick="createGroup()">
                <spring:message code="groups.modal.button.create"/></button>
        </form>
    </div>
</div>
<div id="background" class="modal"></div>
<div id="hiddenFields">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
    <input type="hidden" id="error" value="${requestScope.error}">
</div>
</body>
</html>
