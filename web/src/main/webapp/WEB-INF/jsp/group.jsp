<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="group" value="${requestScope.group}"/>
<c:set var="rights" value="${requestScope.rights}"/>
<c:set var="self" value="${sessionScope.user.id}"/>
<c:set var="spectator" value="${sessionScope.user}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/groupStyle.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../resources/js/groupsScripts.js"></script>
</head>
<body>
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form>
        <button id="language<spring:message code="general.locale"/>" class="localeButton" name="language"
                value="<spring:message code="general.translate"/>" onchange="submit()"></button>
    </form>
</div>
<div id="box">
    <div id="groupLeftBox" class="majorBox">
        <div class="avatarBox">
            <c:if test="${group.avatar == null}">
                <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
            </c:if>
            <c:if test="${group.avatar != null}">
                <img class="avatar" src="${pageContext.request.contextPath}/wall/content?id=${group.avatar}" alt="">
            </c:if>
        </div>
        <div id="groupName">
            ${group.name}
        </div>
        <div id="groupDescription">
            ${group.description}
        </div>
        <c:choose>
            <c:when test="${rights == 0}">
                <form action="${pageContext.request.contextPath}/group/join" method="post">
                    <input type="hidden" name="member" value="${self}">
                    <input type="hidden" name="groupName" value="${group.name}">
                    <input type="submit" id="joinButton" class="mediumButton mediumYellow"
                           value="<spring:message code="group.button.join"/>">
                </form>
            </c:when>
            <c:when test="${rights == 1}">
                <p id="requestHint"><spring:message code="group.message.request"/></p>
            </c:when>
            <c:when test="${rights == 2 || rights == 3}">
                <form action="${pageContext.request.contextPath}/group/leave" method="post">
                    <input type="hidden" name="member" value="${self}">
                    <input type="hidden" name="groupName" value="${group.name}">
                    <input type="submit" id="leaveButton" class="mediumButton mediumRed"
                           value="<spring:message code="group.button.leave"/>">
                </form>
            </c:when>
        </c:choose>
        <form action="${pageContext.request.contextPath}/members${group.name}" method="get">
            <input id="membersButton" type="submit" class="mediumButton mediumBlue"
                   value="<spring:message code="group.button.members"/>">
        </form>
    </div>
    <div id="groupRightBox" class="majorBox">
        <c:if test="${rights > 1}">
            <div id="newDiscusBox" class="minorBox">
                <form id="newDiscusForm" action="${pageContext.request.contextPath}/discussion/start" method="post">
                    <input type="text" name="name" id="theme" class="field"
                           placeholder="<spring:message code="group.placeholder.theme"/>" maxlength="55">
                    <textarea name="message" id="message" class="field"
                              placeholder="<spring:message code="group.placeholder.message"/>"
                              maxlength="255"></textarea>
                    <select id="status" class="field" name="status">
                        <option value="true"><spring:message code="group.select.open"/></option>
                        <c:if test="${rights > 2}">
                            <option value="false"><spring:message code="group.select.close"/></option>
                        </c:if>
                    </select>
                    <label id="statusHint" class="hint" for="access"><spring:message code="group.label.status"/></label>
                    <select id="access" class="field" name="access">
                        <option value="2"><spring:message code="group.select.private"/></option>
                        <c:if test="${rights > 2}">
                            <option value="1"><spring:message code="group.select.public"/></option>
                            <option value="3"><spring:message code="group.select.secure"/></option>
                        </c:if>
                    </select>
                    <label id="accessHint" class="hint" for="access"><spring:message code="group.label.access"/></label>
                    <input type="hidden" name="groupName" value="${group.name}">
                    <button id="startButton" type="button" class="bigButton bigYellow"
                            onclick="createDiscus()"><spring:message code="group.button.start"/></button>
                </form>
            </div>
        </c:if>
        <div id="discusesTable">
            <c:forEach var="discussion" items="${requestScope.discuses}">
                <div id="discusBox" class="minorBox linkBox">
                    <div class="discusName">
                            ${discussion.name}
                    </div>
                    <div class="discusMessages">
                        <spring:message code="group.label.count"/> ${discussion.count}
                    </div>
                    <div class="discusMembers">
                        <spring:message code="group.label.members"/> ${discussion.members}
                    </div>
                    <div class="discusStatus">
                        <c:if test="${discussion.status}">
                            <p class="option"><spring:message code="group.select.open"/></p>
                        </c:if>
                        <c:if test="${!discussion.status}">
                            <p style="color: red" class="option"><spring:message code="group.select.close"/></p>
                        </c:if>
                    </div>
                    <div class="discusAccess">
                        <c:if test="${discussion.access == 1}">
                            <p class="option"><spring:message code="group.select.public"/></p>
                        </c:if>
                        <c:if test="${discussion.access == 2}">
                            <p class="option"><spring:message code="group.select.private"/></p>
                        </c:if>
                        <c:if test="${discussion.access == 3}">
                            <p style="color: red" class="option"><spring:message code="group.select.secure"/></p>
                        </c:if>
                    </div>
                    <a class="link" href="discussion?id=${discussion.id}&groupName=${group.name}"></a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<div id="hiddenFields">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
</body>
</html>
