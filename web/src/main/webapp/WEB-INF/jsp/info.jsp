<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="spectator" value="${sessionScope.user}"/>
<c:set var="status" value="${requestScope.status}"/>
<c:set var="user" value="${requestScope.user}"/>
<c:set var="phone" value="${requestScope.user.phones}"/>
<c:choose>
    <c:when test="${phone.get(0).description == 'mobile'}">
        <spring:message code="information.label.phoneDescription.mobile" var="description1"/>
    </c:when>
    <c:when test="${phone.get(0).description == 'work'}">
        <spring:message code="information.label.phoneDescription.work" var="description1"/>
    </c:when>
    <c:when test="${phone.get(0).description == 'home'}">
        <spring:message code="information.label.phoneDescription.home" var="description1"/>
    </c:when>
    <c:when test="${phone.get(0).description == 'alternative'}">
        <spring:message code="information.label.phoneDescription.alternative" var="description1"/>
    </c:when>
</c:choose>
<c:if test="${phone.size() > 1}">
    <c:choose>
        <c:when test="${phone.get(1).description == 'mobile'}">
            <spring:message code="information.label.phoneDescription.mobile" var="description2"/>
        </c:when>
        <c:when test="${phone.get(1).description == 'work'}">
            <spring:message code="information.label.phoneDescription.work" var="description2"/>
        </c:when>
        <c:when test="${phone.get(1).description == 'home'}">
            <spring:message code="information.label.phoneDescription.home" var="description2"/>
        </c:when>
        <c:when test="${phone.get(1).description == 'alternative'}">
            <spring:message code="information.label.phoneDescription.alternative" var="description2"/>
        </c:when>
    </c:choose>
</c:if>
<c:if test="${phone.size() > 2}">
    <c:choose>
        <c:when test="${phone.get(2).description == 'mobile'}">
            <spring:message code="information.label.phoneDescription.mobile" var="description3"/>
        </c:when>
        <c:when test="${phone.get(2).description == 'work'}">
            <spring:message code="information.label.phoneDescription.work" var="description3"/>
        </c:when>
        <c:when test="${phone.get(2).description == 'home'}">
            <spring:message code="information.label.phoneDescription.home" var="description3"/>
        </c:when>
        <c:when test="${phone.get(2).description == 'alternative'}">
            <spring:message code="information.label.phoneDescription.alternative" var="description3"/>
        </c:when>
    </c:choose>
</c:if>
<<c:if test="${phone.size() > 3}">
    <c:choose>
        <c:when test="${phone.get(3).description == 'mobile'}">
            <spring:message code="information.label.phoneDescription.mobile" var="description4"/>
        </c:when>
        <c:when test="${phone.get(3).description == 'work'}">
            <spring:message code="information.label.phoneDescription.work" var="description4"/>
        </c:when>
        <c:when test="${phone.get(3).description == 'home'}">
            <spring:message code="information.label.phoneDescription.home" var="description4"/>
        </c:when>
        <c:when test="${phone.get(3).description == 'alternative'}">
            <spring:message code="information.label.phoneDescription.alternative" var="description4"/>
        </c:when>
    </c:choose>
</c:if>
<c:if test="${phone.size() > 4}">
    <c:choose>
        <c:when test="${phone.get(4).description == 'mobile'}">
            <spring:message code="information.label.phoneDescription.mobile" var="description5"/>
        </c:when>
        <c:when test="${phone.get(4).description == 'work'}">
            <spring:message code="information.label.phoneDescription.work" var="description5"/>
        </c:when>
        <c:when test="${phone.get(4).description == 'home'}">
            <spring:message code="information.label.phoneDescription.home" var="description5"/>
        </c:when>
        <c:when test="${phone.get(4).description == 'alternative'}">
            <spring:message code="information.label.phoneDescription.alternative" var="description5"/>
        </c:when>
    </c:choose>
</c:if>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/infoPageStyle.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../../resources/js/informationScripts.js"></script>
</head>
<body>
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form>
        <button id="language<spring:message code="general.locale"/>" class="localeButton" name="language"
                value="<spring:message code="general.translate"/>" onchange="submit()"></button>
    </form>
</div>
<div class="box majorBox">
    <div class="avatarBox minorBox">
        <c:choose>
            <c:when test="${requestScope.user.avatar.image == null}">
                <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
            </c:when>
            <c:otherwise>
                <img class="avatar" src="data:image/jpeg;base64, ${requestScope.user.avatar.base64Avatar}" alt="">
            </c:otherwise>
        </c:choose>
        <form action="${pageContext.request.contextPath}/user/avatar/set" method="post"
              enctype="multipart/form-data">
            <input id="file" type="file" name="file" accept=".jpg, .jpeg, .png">
            <label id="fileLabel" for="file"></label>
            <input id="saveAvatarButton" type="submit" class="boxButton mediumButton mediumYellow"
                   value="<spring:message code="information.button.save"/>">
        </form>
    </div>
    <form id="userFields" action="${pageContext.request.contextPath}/user/info/change" method="post">
        <input type="hidden" id="selfId" name="id" value="${user.id}">
        <input type="hidden" id="avatar" name="avatar" value="${user.avatar}">
        <div class="fields">
            <div class="fieldBox">
                <span id="fName" class="label"><spring:message code="information.label.firstName"/></span>
                <span id="firstNameSaved" class="saved">${user.name}</span>
                <input type="hidden" id="firstNameHidden" name="firstName" class="field" value="${user.name}">
                <input type="button" id="firstNameChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="sName" class="label"><spring:message code="information.label.secondName"/></span>
                <span id="secondNameSaved" class="saved">${user.surname}</span>
                <input type="hidden" id="secondNameHidden" name="secondName" class="field" value="${user.surname}">
                <input type="button" id="secondNameChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="lName" class="label"><spring:message code="information.label.lastName"/></span>
                <span id="lastNameSaved" class="saved">${user.patronymic}</span>
                <input type="hidden" id="lastNameHidden" name="lastName" class="field" value="${user.patronymic}">
                <input type="button" id="lastNameChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div id="ph1" class="ph fieldBox">
                <span id="description1Saved" class="descriptionLabel saved">${description1} </span>
                <span class="label"><spring:message code="information.label.phoneNumber"/></span>
                <input type="hidden" id="description1Hidden" name="description1" class="descriptionHidden field"
                       value="${phone.get(0).description}">
                <input type="button" id="description1Change"
                       class="descriptionChangeButton mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                <span id="phone1Saved" class="phoneLabel saved">${phone.get(0).number}</span>
                <input type="hidden" id="phone1Hidden" name="phone1" class="phoneHidden field"
                       value="${phone.get(0).number}">
                <input type="hidden" id="phone1Id" name="phone1Id" class="phoneId field" value="${phone.get(0).id}">
                <input type="button" id="phone1Change"
                       class="phoneChangeButton mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                <input type="button" id="phone1Remove" class="removeButton smallButton phDelete" value=""
                       onclick="minus(this.id)">
                <input type="button" id="addButton" class="smallButton phAdd" value="" onclick="plus()">
            </div>
            <c:if test="${phone.size() > 1}">
                <div id="ph2" class="ph fieldBox">
                    <span id="description2Saved" class="descriptionLabel saved">${description2} </span>
                    <span class="label"><spring:message code="information.label.phoneNumber"/></span>
                    <input type="hidden" id="description2Hidden" name="description2" class="descriptionHidden field"
                           value="${phone.get(1).description}">
                    <input type="button" id="description2Change"
                           class="descriptionChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <span id="phone2Saved" class="phoneLabel saved">${phone.get(1).number}</span>
                    <input type="hidden" id="phone2Hidden" name="phone2" class="phoneHidden field"
                           value="${phone.get(1).number}">
                    <input type="hidden" id="phone2Id" name="phone2Id" class="phoneId field" value="${phone.get(1).id}">
                    <input type="button" id="phone2Change"
                           class="phoneChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <input type="button" id="phone2Remove" class="removeButton smallButton phDelete" value=""
                           onclick="minus(this.id)">
                </div>
            </c:if>
            <c:if test="${phone.size() > 2}">
                <div id="ph3" class="ph fieldBox">
                    <span id="description3Saved" class="descriptionLabel saved">${description3} </span>
                    <span class="label"><spring:message code="information.label.phoneNumber"/></span>
                    <input type="hidden" id="description3Hidden" name="description3" class="descriptionHidden field"
                           value="${phone.get(2).description}">
                    <input type="button" id="description3Change"
                           class="descriptionChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <span id="phone3Saved" class="phoneLabel saved">${phone.get(2).number}</span>
                    <input type="hidden" id="phone3Hidden" name="phone3" class="phoneHidden field"
                           value="${phone.get(2).number}">
                    <input type="hidden" id="phone3Id" name="phone3Id" class="phoneId field" value="${phone.get(2).id}">
                    <input type="button" id="phone3Change"
                           class="phoneChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <input type="button" id="phone3Remove" class="removeButton smallButton phDelete" value=""
                           onclick="minus(this.id)">
                </div>
            </c:if>
            <c:if test="${phone.size() > 3}">
                <div id="ph4" class="ph fieldBox">
                    <span id="description4Saved" class="descriptionLabel saved">${description4} </span>
                    <span class="label"><spring:message code="information.label.phoneNumber"/></span>
                    <input type="hidden" id="description4Hidden" name="description4" class="descriptionHidden field"
                           value="${phone.get(3).description}">
                    <input type="button" id="description4Change"
                           class="descriptionChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <span id="phone4Saved" class="phoneLabel saved">${phone.get(3).number}</span>
                    <input type="hidden" id="phone4Hidden" name="phone4" class="phoneHidden field"
                           value="${phone.get(3).number}">
                    <input type="hidden" id="phone4Id" name="phone4Id" class="phoneId field" value="${phone.get(3).id}">
                    <input type="button" id="phone4Change"
                           class="phoneChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <input type="button" id="phone4Remove" class="removeButton smallButton phDelete" value=""
                           onclick="minus(this.id)">
                </div>
            </c:if>
            <c:if test="${phone.size() > 4}">
                <div id="ph5" class="ph fieldBox">
                    <span id="description5Saved" class="descriptionLabel saved">${description5} </span>
                    <span class="label"><spring:message code="information.label.phoneNumber"/></span>
                    <input type="hidden" id="description5Hidden" name="description5" class="descriptionHidden field"
                           value="${phone.get(4).description}">
                    <input type="button" id="description5Change"
                           class="descriptionChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <span id="phone5Saved" class="phoneLabel saved">${phone.get(4).number}</span>
                    <input type="hidden" id="phone5Hidden" name="phone5" class="phoneHidden field"
                           value="${phone.get(4).number}">
                    <input type="hidden" id="phone5Id" name="phone5Id" class="phoneId field" value="${phone.get(4).id}">
                    <input type="button" id="phone5Change"
                           class="phoneChangeButton mediumButton mediumBlue changeButton boxButton"
                           value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
                    <input type="button" id="phone5Remove" class="removeButton smallButton phDelete" value=""
                           onclick="minus(this.id)">
                </div>
            </c:if>
            <div class="fieldBox">
                <span id="birth" class="label"><spring:message code="information.label.birth"/></span>
                <span id="birthSaved" class="saved">${user.birth}</span>
                <input type="hidden" id="birthHidden" name="birth" class="field" value="${user.birth}">
                <input type="button" id="birthChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="hAddress" class="label"><spring:message code="information.label.home"/></span>
                <span id="homeAddressSaved" class="saved">${user.home}</span>
                <input type="hidden" id="homeAddressHidden" name="homeAddress" class="field"
                       value="${user.home}">
                <input type="button" id="homeAddressChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="wAddress" class="label"><spring:message code="information.label.work"/></span>
                <span id="workAddressSaved" class="saved">${user.work}</span>
                <input type="hidden" id="workAddressHidden" name="workAddress" class="field"
                       value="${user.work}">
                <input type="button" id="workAddressChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="email" class="label"><spring:message code="information.label.email"/></span>
                <span id="emailSaved" class="saved">${user.email}</span>
                <input type="hidden" id="emailHidden" name="email" class="field" value="${user.email}">
                <input type="button" id="emailChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="icq" class="label"><spring:message code="information.label.icq"/></span>
                <span id="icqSaved" class="saved">${user.icq}</span>
                <input type="hidden" id="icqHidden" name="icq" class="field" value="${user.icq}">
                <input type="button" id="icqChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="skype" class="label"><spring:message code="information.label.skype"/></span>
                <span id="skypeSaved" class="saved">${user.skype}</span>
                <input type="hidden" id="skypeHidden" name="skype" class="field" value="${user.skype}">
                <input type="button" id="skypeChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <div class="fieldBox">
                <span id="additionally" class="label"><spring:message code="information.label.additionally"/></span>
                <span id="additionallySaved" class="saved">${user.additionally}</span>
                <input type="hidden" id="additionallyHidden" name="additionally" class="field"
                       value="${user.additionally}">
                <input type="button" id="additionallyChange" class="mediumButton mediumBlue changeButton boxButton"
                       value="<spring:message code="information.button.change"/>" onclick="change(this.id)">
            </div>
            <input type="button" id="saveButton" class="bigButton bigYellow"
                   value="<spring:message code="information.button.send"/>" onclick="send()">
        </div>
    </form>
</div>
<div id="hiddenFields">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
</body>
</html>

