<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="spectator" value="${sessionScope.user}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/searchScripts.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/searchStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body id="body">
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form action="${pageContext.request.contextPath}/user/search">
        <input type="hidden" name="filter" value="${pageContext.request.getParameter("filter")}">
        <input type="hidden" name="page" value="${pageContext.request.getParameter("page")}">
        <input type="hidden" name="language" value="<spring:message code="general.translate"/>">
        <input type="submit" id="language<spring:message code="general.locale"/>" class="localeButton" value="">
    </form>
</div>
<div id="bottomBox" class="majorBox">
    <div id="buttonBox">
        <input type="button" id="usersButton" class="mediumButton mediumYellow bottomBoxButton"
               value="<spring:message code="search.button.users"/>" onclick="getPages('user')">
        <input type="button" id="groupsButton" class="mediumButton mediumYellow bottomBoxButton"
               value="<spring:message code="search.button.groups"/>" onclick="getPages('group')">
    </div>
    <div id="searchResultsBox">
        <c:forEach var="result" items="${results}">
            <div id="resultBox" class="minorBox">
                <div class="avatarBox">
                    <c:if test="${result.avatar.image == null}">
                        <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                             alt="">
                    </c:if>
                    <c:if test="${result.avatar.image != null}">
                        <img class="avatar" src="data:image/jpeg;base64, ${result.avatar.base64Avatar}" alt="">
                    </c:if>
                </div>
                <div id="resultInfo">
                        ${result.name} ${result.surname} ${result.patronymic}
                </div>
                <form action="${pageContext.request.contextPath}/id${result.id}" method="get">
                    <input type="submit" id="showButton" class="mediumButton mediumBlue bottomBoxButton"
                           value="<spring:message code="search.button.show"/>">
                </form>
                <c:if test="${sessionScope.role.equals('admin')}">
                    <input type="button" id="deleteButton" class="mediumButton mediumRed bottomBoxButton"
                           value="<spring:message code="search.button.delete"/>"
                           onclick="deleteUser(${result.id})">
                </c:if>
            </div>
        </c:forEach>
    </div>
    <div id="pagination">
        <c:forEach begin="1" end="${noOfPages > 9 ? 9 : noOfPages}" var="i">
            <c:choose>
                <c:when test="${noOfPages <= 9}">
                    <c:choose>
                        <c:when test="${i == 1}">
                            <div class="pageBox active">
                                <a class="active" href="#" onclick="getPage(${i}, 'user')">${i}</a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="pageBox">
                                <a href="#" onclick="getPage(${i}, 'user')">${i}</a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${i == 1}">
                            <div class="pageBox">
                                <a href="#" class="active" onclick="getPage(${i}, 'user')">${i}</a>
                            </div>
                        </c:when>
                        <c:when test="${i == 8}">
                            <div class="pageBox">
                                <a href="#" onclick="setPage()">...</a>
                            </div>
                        </c:when>
                        <c:when test="${i == 9}">
                            <div class="pageBox">
                                <a href="#" onclick="getPage(${noOfPages}, 'user')">${noOfPages}</a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="pageBox">
                                <a href="#" onclick="getPage(${i}, 'user')">${i}</a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${noOfPages > 1}">
            <div class="pageBox">
                <a href="#" class="page" onclick="getPage(2, 'user')">>></a>
            </div>
        </c:if>
    </div>
</div>
<div id="hiddenFields">
    <input type="hidden" id="actualPage" value="1">
    <input type="hidden" id="filter" value="${filter}">
    <input type="hidden" id="pages" value="${noOfPages}">
    <input type="hidden" id="type" value="user">
    <input type="hidden" id="role" value="${sessionScope.role}">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
</body>
</html>
