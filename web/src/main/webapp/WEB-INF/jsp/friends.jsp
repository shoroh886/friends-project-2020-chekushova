<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="userPageId" value="${requestScope.userPageId}"/>
<c:set var="spectator" value="${sessionScope.user}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/friendListStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form action="${pageContext.request.contextPath}/friends">
        <input type="hidden" name="userPageId" value="${pageContext.request.getParameter("userPageId")}">
        <input type="hidden" name="language" value="<spring:message code="general.translate"/>">
        <input type="submit" id="language<spring:message code="general.locale"/>" class="localeButton" value="">
    </form>
</div>
<div id="box">
    <c:if test="${userPageId == spectator.id}">
        <div id="requestBox" class="majorBox">
            <p><spring:message code="friends.message.request"/></p>
            <c:forEach var="request" items="${requestScope.requests}">
                <div class="minorBox userBox">
                    <div class="avatarBox">
                        <c:if test="${request.avatar.image == null}">
                            <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                                 alt="">
                        </c:if>
                        <c:if test="${request.avatar.image != null}">
                            <img class="avatar" src="data:image/jpeg;base64, ${request.avatar.base64Avatar}" alt="">
                        </c:if>
                    </div>
                    <div class="userInfo">
                            ${request.name}
                            ${request.surname}
                    </div>
                    <form action="${pageContext.request.contextPath}/id${request.id}">
                        <input type="submit" class="mediumButton mediumBlue showButton"
                               value="<spring:message code="friends.button.show"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/friend/accept" method="post">
                        <input type="hidden" name="friendId" value="${request.id}">
                        <input type="hidden" name="selfId" value="${spectator.id}">
                        <input type="hidden" name="userPageId" value="${spectator.id}">
                        <input type="submit" class="mediumButton mediumYellow requestAcceptButton"
                               value="<spring:message code="friends.button.accept"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/friend/reject" method="post">
                        <input type="hidden" name="friendId" value="${request.id}">
                        <input type="hidden" name="selfId" value="${spectator.id}">
                        <input type="hidden" name="userPageId" value="${spectator.id}">
                        <input type="submit" class="mediumButton mediumRed requestRejectButton"
                               value="<spring:message code="friends.button.reject"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/chat" method="get">
                        <input type="hidden" name="companionId" value="${request.id}">
                        <input type="submit" class="mediumButton mediumBlue sendMessageButton"
                               value="<spring:message code="friends.button.chat"/>">
                    </form>
                </div>
            </c:forEach>
        </div>
    </c:if>
    <div id="friendsBox" class="majorBox">
        <p><spring:message code="friends.message.friends"/></p>
        <c:forEach var="friend" items="${requestScope.friends}">
            <div class="minorBox userBox">
                <div class="avatarBox">
                    <c:if test="${friend.avatar.image == null}">
                        <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
                    </c:if>
                    <c:if test="${friend.avatar.image != null}">
                        <img class="avatar" src="data:image/jpeg;base64, ${friend.avatar.base64Avatar}" alt="">
                    </c:if>
                </div>
                <div class="userInfo">
                        ${friend.name}
                        ${friend.surname}
                </div>
                <form action="${pageContext.request.contextPath}/id${friend.id}">
                    <input type="submit" class="mediumButton mediumBlue showButton"
                           value="<spring:message code="friends.button.show"/>">
                </form>
                <c:if test="${userPageId == spectator.id}">
                    <form action="${pageContext.request.contextPath}/friend/remove" method="post">
                        <input type="hidden" name="friendId" value="${friend.id}">
                        <input type="hidden" name="selfId" value="${spectator.id}">
                        <input type="hidden" name="userPageId" value="${spectator.id}">
                        <input type="submit" class="mediumButton mediumRed removeFriendButton"
                               value="<spring:message code="friends.button.remove"/>">
                    </form>
                </c:if>
                <form action="${pageContext.request.contextPath}/chat" method="get">
                    <input type="hidden" name="companionId" value="${friend.id}">
                    <input type="submit" class="mediumButton mediumBlue sendMessageButton"
                           value="<spring:message code="friends.button.chat"/>">
                </form>
            </div>
        </c:forEach>
    </div>
    <c:if test="${userPageId == spectator.id}">
        <div id="subscribersBox" class="majorBox">
            <p><spring:message code="friends.message.subscribers"/></p>
            <c:forEach var="subscriber" items="${requestScope.subscribers}">
                <div class="minorBox userBox">
                    <div class="avatarBox">
                        <c:if test="${subscriber.avatar.image == null}">
                            <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                                 alt="">
                        </c:if>
                        <c:if test="${subscriber.avatar.image != null}">
                            <img class="avatar" src="data:image/jpeg;base64, ${subscriber.avatar.base64Avatar}" alt="">
                        </c:if>
                    </div>
                    <div class="userInfo">
                            ${subscriber.name}
                            ${subscriber.surname}
                    </div>
                    <form action="${pageContext.request.contextPath}/id${subscriber.id}">
                        <input type="submit" class="mediumButton mediumBlue showButton"
                               value="<spring:message code="friends.button.show"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/friend/accept" method="post">
                        <input type="hidden" name="friendId" value="${subscriber.id}">
                        <input type="hidden" name="selfId" value="${spectator.id}">
                        <input type="hidden" name="userPageId" value="${spectator.id}">
                        <input type="submit" class="mediumButton mediumYellow addFriendButton"
                               value="<spring:message code="friends.button.addFriend"/>">
                    </form>
                    <form action="${pageContext.request.contextPath}/chat" method="get">
                        <input type="hidden" name="companionId" value="${subscriber.id}">
                        <input type="submit" class="mediumButton mediumBlue sendMessageButton"
                               value="<spring:message code="friends.button.chat"/>">
                    </form>
                </div>
            </c:forEach>
        </div>
        <div id="subscriptionsBox" class="majorBox">
            <p><spring:message code="friends.message.subscription"/></p>
            <c:forEach var="subscription" items="${requestScope.subscriptions}">
                <div class="minorBox userBox">
                    <div class="avatarBox">
                        <c:if test="${subscription.avatar.image == null}">
                            <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png"
                                 alt="">
                        </c:if>
                        <c:if test="${subscription.avatar.image != null}">
                            <img class="avatar" src="data:image/jpeg;base64, ${subscription.avatar.base64Avatar}"
                                 alt="">
                        </c:if>
                    </div>
                    <div class="userInfo">
                            ${subscription.name}
                            ${subscription.surname}
                    </div>
                    <form action="${pageContext.request.contextPath}/id${subscription.id}">
                        <input type="submit" class="mediumButton mediumBlue showButton"
                               value="<spring:message code="friends.button.show"/>">
                    </form>
                    <c:if test="${userPageId == spectator.id}">
                        <form action="${pageContext.request.contextPath}/friend/remove" method="post">
                            <input type="hidden" name="friendId" value="${subscription.id}">
                            <input type="hidden" name="selfId" value="${spectator.id}">
                            <input type="hidden" name="userPageId" value="${spectator.id}">
                            <input type="hidden" name="from" value="friendlist">
                            <input type="submit" class="mediumButton mediumRed stopSubscribeButton"
                                   value="<spring:message code="friends.button.stop"/>">
                        </form>
                    </c:if>
                    <form action="${pageContext.request.contextPath}/chat" method="get">
                        <input type="hidden" name="companionId" value="${subscription.id}">
                        <input type="submit" class="mediumButton mediumBlue sendMessageButton"
                               value="<spring:message code="friends.button.chat"/>">
                    </form>
                </div>
            </c:forEach>
        </div>
    </c:if>
</div>
</body>
</html>