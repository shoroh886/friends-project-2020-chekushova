<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="spectator" value="${sessionScope.user}"/>
<c:set var="companion" value="${requestScope.companion}"/>
<c:set var="self" value="${sessionScope.user.id}"/>
<html>
<head>
    <title>Title</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.4.0/sockjs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
    <script type="text/javascript" src="../../resources/js/autoScript.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="${pageContext.request.contextPath}/resources/css/generalStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/buttonsStyle.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/chatStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body onload="connect()" onclick="readAll()">
<div id="upperBox" class="majorBox">
    <div id="myInfo" class="myInfo">
        ${spectator.name} ${spectator.surname}
    </div>
    <form action="${pageContext.request.contextPath}/id${spectator.id}" method="get">
        <input id="myPageButton" class="mediumButton mediumBlue upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.my"/>">
    </form>
    <div id="searchField" class="searchField">
        <form action="${pageContext.request.contextPath}/search" method="get">
            <div class="ui-widget">
                <label id="searchLabel" for="search"><spring:message code="general.upperBox.search"/></label>
                <input id="search" name="filter">
                <input type="hidden" name="page" value="1">
                <input id="searchButton" class="mediumButton mediumYellow upperBoxButton" type="submit"
                       value="<spring:message code="general.upperBox.find"/>">
                <p id="searchHint" class="hint"></p>
            </div>
        </form>
    </div>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input id="logoutButton" class="mediumButton mediumRed upperBoxButton" type="submit"
               value="<spring:message code="general.upperBox.logout"/>">
    </form>
    <form action="${pageContext.request.contextPath}/chat">
        <input type="hidden" name="companionId" value="${pageContext.request.getParameter("companionId")}">
        <input type="hidden" name="language" value="<spring:message code="general.translate"/>">
        <input type="submit" id="language<spring:message code="general.locale"/>" class="localeButton" value="">
    </form>
</div>
<input type="hidden" id="spectatorId" value="${spectator.id}">
<input type="hidden" id="companionId" value="${companion.id}">
<div id="mainBox">
    <div id="companionBox" class="majorBox userBox">
        <div class="avatarBox">
            <c:choose>
                <c:when test="${companion.avatar.image == null}">
                    <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
                </c:when>
                <c:otherwise>
                    <img class="avatar" src="data:image/jpeg;base64, ${companion.avatar.base64Avatar}" alt="">
                </c:otherwise>
            </c:choose>
        </div>
        <div class="userData">
            <p>${companion.name} ${companion.surname}</p>
        </div>
    </div>
    <div id="spectatorBox" class="majorBox userBox">
        <div class="avatarBox">
            <c:choose>
                <c:when test="${spectator.avatar.image == null}">
                    <img class="avatar" src="${pageContext.request.contextPath}/resources/img/default.png" alt="">
                </c:when>
                <c:otherwise>
                    <img class="avatar" src="data:image/jpeg;base64, ${spectator.avatar.base64Avatar}" alt="">
                </c:otherwise>
            </c:choose>
        </div>
        <div class="userData">
            <p>${spectator.name} ${spectator.surname}</p>
        </div>
    </div>
    <div id="chatBox" class="majorBox" onscroll="checkScroll()">
        <input type="hidden" id="page" value="1">
        <table id="chatTable">
            <tr>
                <c:if test="${messages.size() > 4}">
                    <c:choose>
                        <c:when test="${messages.get(4).sender.id == companion.id}">
                            <td class="alias">${messages.get(4).text}</td>
                            <td></td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            <td class="self">${messages.get(4).text}</td>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </tr>
            <tr>
                <c:if test="${messages.size() > 3}">
                    <c:choose>
                        <c:when test="${messages.get(3).sender.id == companion.id}">
                            <td class="alias">${messages.get(3).text}</td>
                            <td></td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            <td class="self">${messages.get(3).text}</td>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </tr>
            <tr>
                <c:if test="${messages.size() > 2}">
                    <c:choose>
                        <c:when test="${messages.get(2).sender.id == companion.id}">
                            <td class="alias">${messages.get(2).text}</td>
                            <td></td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            <td class="self">${messages.get(2).text}</td>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </tr>
            <tr>
                <c:if test="${messages.size() > 1}">
                    <c:choose>
                        <c:when test="${messages.get(1).sender.id == companion.id}">
                            <td class="alias">${messages.get(1).text}</td>
                            <td></td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            <td class="self">${messages.get(1).text}</td>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </tr>
            <tr>
                <c:if test="${messages.size() > 0}">
                    <c:choose>
                        <c:when test="${messages.get(0).sender.id == companion.id}">
                            <td class="alias">${messages.get(0).text}</td>
                            <td></td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                            <td class="self">${messages.get(0).text}</td>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </tr>
        </table>
    </div>
    <div id="enterBox" class="majorBox">
        <textarea id="message" placeholder="<spring:message code="chat.placeholder.message"/>"
                  maxlength="255"></textarea>
        <input type="submit" id="sendButton" class="bigButton bigYellow" onclick="send()"
               value="<spring:message code="chat.button.send"/>">
    </div>
</div>
<div id="hiddenFields">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
<script src="../../resources/js/chatScript.js"></script>
</body>
</html>