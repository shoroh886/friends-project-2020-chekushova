<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cf" uri="http://www.springframework.org/tags/form" %>
<c:set var="loginPlaceholder"><spring:message code="registration.placeholder.login"/></c:set>
<c:set var="passwordPlaceholder"><spring:message code="registration.placeholder.password"/></c:set>
<c:set var="repeatPlaceholder"><spring:message code="registration.placeholder.repeat"/></c:set>
<c:set var="namePlaceholder"><spring:message code="registration.placeholder.firstName"/></c:set>
<c:set var="surnamePlaceholder"><spring:message code="registration.placeholder.secondName"/></c:set>
<c:set var="patronymicPlaceholder"><spring:message code="registration.placeholder.lastName"/></c:set>
<c:set var="phonePlaceholder"><spring:message code="registration.placeholder.phoneNumber"/></c:set>
<c:set var="birthPlaceholder"><spring:message code="registration.placeholder.birth"/></c:set>
<c:set var="homePlaceholder"><spring:message code="registration.placeholder.home"/></c:set>
<c:set var="workPlaceholder"><spring:message code="registration.placeholder.work"/></c:set>
<c:set var="emailPlaceholder"><spring:message code="registration.placeholder.email"/></c:set>
<c:set var="icqPlaceholder"><spring:message code="registration.placeholder.icq"/></c:set>
<c:set var="skypePlaceholder"><spring:message code="registration.placeholder.skype"/></c:set>
<c:set var="additionalyPlaceholder"><spring:message code="registration.placeholder.additionally"/></c:set>
<html>
<head>
    <title>registration</title>
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script type="text/javascript" src="../../resources/js/regScripts.js"></script>
    <link href="${pageContext.request.contextPath}/resources/css/regPageStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="box">
    <sf:form id="regForm" action="${pageContext.request.contextPath}/register" method="post"
             modelAttribute="registerUser">
        <div id="fields" class="fields">
            <div id="pageWelcome"><spring:message code="registration.welcome"/></div>
            <div id="pageMessage"><spring:message code="registration.message"/></div>
            <img id="arrow" src="${pageContext.request.contextPath}/resources/img/arrow-grey.png" alt="">
            <button type="button" id="jsonButton" class="button" onclick="show('modal')">.json</button>
            <span>
                <sf:input path="login" type="text" name="login" id="login" class="field"
                          placeholder="${loginPlaceholder}" maxlength="45"/>
                <div id="loginError" class="error">
                    <sf:errors path="login"/>
                </div>
            </span>
            <span>
                <sf:input path="password" type="password" name="password" id="password" class="field"
                          placeholder="${passwordPlaceholder}" maxlength="45"/>
                <div id="passwordError" class="error">
                    <sf:errors path="password"/>
                </div>
            </span>
            <span>
                <sf:input path="repeatPassword" type="password" name="repeatPassword" id="repeatPassword" class="field"
                          placeholder="${repeatPlaceholder}" maxlength="45"/>
            </span>
            <div id="repeatPasswordError" class="error">
                <sf:errors path="repeatPassword"/>
            </div>
            <span>
                <sf:input path="name" type="text" name="firstName" id="firstName" class="field"
                          placeholder="${namePlaceholder}" maxlength="45"/>
                <div id="firstNameError" class="error">
                    <sf:errors path="name"/>
                </div>
            </span>
            <span>
                <sf:input path="surname" type="text" name="secondName" id="secondName" class="field"
                          placeholder="${surnamePlaceholder}" maxlength="45"/>
                <div id="secondNameError" class="error">
                    <sf:errors path="surname"/>
                </div>
            </span>
            <span>
                <sf:input path="patronymic" type="text" name="lastName" id="lastName" class="field"
                          placeholder="${patronymicPlaceholder}" maxlength="45"/>
                <div id="lastNameError" class="error">
                    <cf:errors path="patronymic"/>
                </div>
            </span>
            <span id="ph1" class="ph">
            <sf:input path="phones[0].number" type="text" name="phone1" id="phone1" class="phone field"
                      placeholder="${phonePlaceholder}" maxlength="45"/>
                <div id="phone1Error" class="error phoneEr">
                    <cf:errors path="phones[0].number"/>
                </div>
                <sf:select path="phones[0].description" id="description1" class="desc field" name="description1">
                    <option value="mobile">
                        <spring:message code="registration.placeholder.phoneDescription.mobile"/>
                    </option>
                    <option value="home">
                        <spring:message code="registration.placeholder.phoneDescription.home"/>
                    </option>
                    <option value="work">
                        <spring:message code="registration.placeholder.phoneDescription.work"/>
                    </option>
                    <option value="alternative">
                        <spring:message code="registration.placeholder.phoneDescription.alternative"/>
                    </option>
                </sf:select>
                <div id="description1Error" class="error descEr"></div>
                <input type="button" id="ph1Delete" class="phDelete smallButton" onclick=minus(this.id)>
                <input type="button" id="phoneAdd" class="phAdd smallButton" onclick=plus()>
            </span>
            <span>
                <sf:input path="birth" type="text" name="birth" id="birth" class="field afterPh"
                          placeholder="${birthPlaceholder}" maxlength="45"/>
                <div id="birthError" class="error afterPh">
                    <sf:errors path="birth"/>
                </div>
            </span>
            <span>
                <sf:input path="home" type="text" name="homeAddress" id="homeAddress" class="field afterPh"
                          placeholder="${homePlaceholder}" maxlength="90"/>
                <div id="homeAddressError" class="error afterPh">
                    <sf:errors path="home"/>
                </div>
            </span>
            <span>
                <sf:input path="work" type="text" name="workAddress" id="workAddress" class="field afterPh"
                          placeholder="${workPlaceholder}" maxlength="90"/>
                <div id="workAddressError" class="error afterPh">
                        <sf:errors path="work"/>
                </div>
            </span>
            <span>
                <sf:input path="email" type="text" name="email" id="email" class="field afterPh"
                          placeholder="${emailPlaceholder}" maxlength="45"/>
                <div id="emailError" class="error afterPh">
                        <sf:errors path="email"/>
                </div>
            </span>
            <span>
                <sf:input path="icq" type="text" name="icq" id="icq" class="field afterPh"
                          placeholder="${icqPlaceholder}" maxlength="11"/>
                <div id="icqError" class="error afterPh">
                    <sf:errors path="icq"/>
                </div>
            </span>
            <span>
                <sf:input path="skype" type="text" name="skype" id="skype" class="field afterPh"
                          placeholder="${skypePlaceholder}" maxlength="45"/>
                <div id="skypeError" class="error afterPh">
                    <sf:errors path="skype"/>
                </div>
            </span>
            <span>
                <sf:textarea path="additionally" name="additionally" id="additionally" class="field afterPh"
                             placeholder="${additionalyPlaceholder}" maxlength="255" wrap="soft"></sf:textarea>
                <div id="additionallyError" class="error afterPh">
                    <sf:errors path="additionally"/>
                </div>
            </span>
            <div id="confirm" class="confirm">
                <div id="confirmMessage"><spring:message code="registration.modal.confirm.message"/></div>
                <button id="Yes" type="button" class="button" onclick="check()"><spring:message
                        code="registration.modal.confirm.yes"/></button>
                <button id="No" type="button" class="button" onclick="hide('confirm')"><spring:message
                        code="registration.modal.confirm.no"/></button>
            </div>
            <input type="button" id="enter" class="afterPh button"
                   value="<spring:message code="registration.button.enter"/>" onclick="show('confirm')">
        </div>
    </sf:form>
    <div id="jsonBox" class="modal">
        <form action="${pageContext.request.contextPath}/user/json/register" method="post"
              enctype="multipart/form-data">
            <input id="file" type="file" value="" name="file">
            <label id="fileLabel" for="file"></label>
            <input id="enterJson" type="submit" class="button"
                   value="<spring:message code="registration.button.enter"/>" onclick="hide('modal')">
        </form>
        <button type="button" id="hideButton" class="button" onclick="hide('modal')">
            <spring:message code="registration.modal.json.hide"/>
        </button>
    </div>
</div>
<div id="background" class="modal confirm"></div>
<div id="hiddenFields">
    <input type="hidden" id="lang" value="<spring:message code="general.locale"/>">
</div>
</body>
</html>